<?php

namespace AppSupply\FeedmakerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AppSupplyFeedmakerBundle:Default:index.html.twig', array('name' => $name));
    }
}
