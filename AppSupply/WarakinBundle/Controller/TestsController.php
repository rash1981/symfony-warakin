<?php

namespace AppSupply\WarakinBundle\Controller;

use AppSupply\WarakinBundle\Controller\WarakinController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;

use AppSupply\WarakinBundle\Entity\Album;
use AppSupply\WarakinBundle\Entity\Tags;

class TestsController extends WarakinController
{
    public function styleTrackComponentAction()
    {
		//var_dump($popTags[0]->getBpm());
        return $this->render(
        	'AppSupplyWarakinBundle:Tests:style-track-component.test.html.twig'
  			// array(
			// 	'home'=>array(
			// 		"artists" => array(
			// 			"New" => $newArtists,
			// 			"Pop" => $popArtists,
			// 			"Random" => array(),
			// 		),
			// 		"albums" => array(
			// 			"New" => $newAlbums,
			// 			"Pop" => $popAlbums,
			// 			"Random" => array(),
			// 		),
			// 		"tracks" => array(
			// 			"New" => $newTags,
			// 			"Pop" => $popTags,
			// 			"Random" => array(),
			// 			"Serato" => $seratoTags,
			// 			"newFavorites" => $newFavoriteTags
			// 		),
			// 		"genregroups" => $genreGroups
			// 	),
			// 	'value'=>new Tags()
			// )
        );
    }
}
