<?php

namespace AppSupply\WarakinBundle\Controller;

use AppSupply\WarakinBundle\Controller\WarakinController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;

use AppSupply\WarakinBundle\Entity\Album;
use AppSupply\WarakinBundle\Entity\Tags;
use AppSupply\WarakinBundle\Entity\lastFmTag;

class ServiceWorkerController extends WarakinController
{
    public function serviceWorkerAction()
    {
		// $optionalParams = array('some'=>'param');
        // $renderedView = $this->renderView('AppSupplyWarakinBundle:Default:serviceWorker.js.twig');
        // $response = new Response($renderedView, $optionalParams);
        // $response->setStatusCode(Response::HTTP_OK);
        // $response->headers->set('Content-Type','text/javascript');
        return $this->render('AppSupplyWarakinBundle:Default:serviceWorker.js.twig');
	}
}
