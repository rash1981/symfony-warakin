<?php

namespace AppSupply\WarakinBundle\Controller;

use AppSupply\WarakinBundle\Controller\WarakinController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Finder\Finder;
use Doctrine\ORM\EntityManager;
use \GetId3\GetId3Core as GetId3;

/*
	Utility controller contains a number of functions

	Library Scanner

	The 
*/

class PlexSynchronisationController extends WarakinController
{

	public function importPlaylistsAction(){
		return $this->getJsonResponse(
			$this->importPlaylists()
		);
	}

    private function importPlaylists(){
	    ini_set('memory_limit', '512M');
        $em = $this->getDoctrine()->getManager();
        $plexToken = '2sF9pXBsRChDSbFxhaus';
        $plexPlaylistsXml = new JsonSerializer(file_get_contents('https://192.168.1.74:32400/playlists/all?type=15&X-Plex-Token='.$plexToken));
        $plexPlaylistJSON = $plexPlaylistsXml->jsonSerialize()['MediaContainer']; 
        
        //var_dump($plexPlaylistJSON);
        $i=0;
        foreach($plexPlaylistJSON as $index => $playlist){

            $timelimit = ini_get('max_execution_time');
		    set_time_limit($timelimit + 3000);
            
            $plexPlaylistContentXml = new JsonSerializer(file_get_contents('https://192.168.1.74:32400/playlists/'.$playlist['ratingKey'].'/items?X-Plex-Token='.$plexToken));
            //$plexPlaylistContentXml = new JsonSerializer(file_get_contents('https://192.168.1.74:32400/playlists/60317/items?X-Plex-Token=NpxfCpgpsDzvdxzqZzDo'));
            $plexPlaylistContent = $plexPlaylistContentXml->jsonSerialize();
            $playlistAttr = (array)$playlist;
            
            $playlistObj = $em
                            ->getRepository('AppSupplyWarakinBundle:Playlists')
                            ->findOneByName($playlistAttr['@attributes']['title']);

            //var_dump($playlistAttr['@attributes']['smart']);   
            
            if($playlistAttr['@attributes']['smart']!=1){             
                if(!$playlistObj){
                    //var_dump($plexPlaylistContent);
                    echo '<b>Playlist '.$playlistAttr['@attributes']['title'].' does not exist, it will be created.</b><br/>'."\n";
                    $playlistObj = new \AppSupply\WarakinBundle\Entity\Playlists();
                    $playlistObj->setName($playlistAttr['@attributes']['title']);
                }else{
                    echo '<b>Playlist '.$playlistAttr['@attributes']['title'].' exists, loading it from db.</b><br/>'."\n";
                }

                echo  count($plexPlaylistContent['MediaContainer']).' !=  '.count($playlistObj->getTags())."<br/>\n";
                
                if( count($plexPlaylistContent['MediaContainer']) != count($playlistObj->getTags())){
                    
                    foreach($plexPlaylistContent['MediaContainer'] as $index => $track){

                        if(property_exists($track, 'Media')){
                            //var_dump($track->Media->Part);
                            $mediaPart = (array)$track->Media->Part;

                            $tag = $em
                                    ->getRepository('AppSupplyWarakinBundle:Tags')
                                    ->findOneByUrl( iconv(mb_detect_encoding ('.'.$mediaPart['@attributes']['file'], "auto"), 'US-ASCII//TRANSLIT', $mediaPart['@attributes']['file'] ));
                            
                            if(!$tag){
                                $tag = $em
                                    ->getRepository('AppSupplyWarakinBundle:Tags')
                                    ->findOneByUrl( iconv(mb_detect_encoding ('.'.$mediaPart['@attributes']['file'], "auto"),  'US-ASCII//TRANSLIT', '.'.$mediaPart['@attributes']['file'] ));
                            }
                            if(!$tag){
                                $tag = $em
                                    ->getRepository('AppSupplyWarakinBundle:Tags')
                                    ->findOneByUrl('.'.$mediaPart['@attributes']['file']);
                            }
                            if(!$tag){
                                $tag = $em
                                    ->getRepository('AppSupplyWarakinBundle:Tags')
                                    ->findOneByUrl($mediaPart['@attributes']['file']);
                            }
                            if($tag){
                                $tagInPlaylist = false;
                                $currentTags = $playlistObj->getTags();
                                foreach($currentTags as  $tagIndex => $currentTag){
                                    //echo 'Matching '.$currentTag->getUrl()." with playlist entry ".$tag->getUrl()."<br/>\n";
                                    if($currentTag->getUrl() == $tag->getUrl()){
                                        //echo 'found track '.$tag->getUrl()." in playlist <br/>\n";
                                        $tagInPlaylist = true;
                                    }
                                }
                                if(!$tagInPlaylist){
                                    //var_dump($tag->getUrl());
                                    echo ('Playlist '.$playlistAttr['@attributes']['title'].' adding track <i>'.$mediaPart['@attributes']['file']."</i><br/>\n");
                                    $playlistObj->addTag($tag);
                                }else{
                                    //echo ('Playlist '.$playlistAttr['@attributes']['title'].' already includes track <i>'.$mediaPart['@attributes']['file']."</i><br/>\n");
                                }
                            }else{
                                echo 'Tag not found for: '.$mediaPart['@attributes']['file']."<br/>\n";
                            }
                        }

                    }

                    $em->persist($playlistObj);
                    $em->flush();
                }
            }
            /*$i++;
            if($i > 10){
                die();
            } */          
        } 

        //var_dump($plexPlaylistContent);   
    }
}

class JsonSerializer extends \SimpleXmlElement implements \JsonSerializable
{
    /**
     * SimpleXMLElement JSON serialization
     *
     * @return null|string
     *
     * @link http://php.net/JsonSerializable.jsonSerialize
     * @see JsonSerializable::jsonSerialize
     */
    function jsonSerialize()
    {
        if (count($this)) {
            // serialize children if there are children
            $i = 0;
            foreach ($this as $tag => $child) {
                // child is a single-named element -or- child are multiple elements with the same name - needs array
                if (count($child) > 1) {
                    $child = [($child->children()->getName()) => iterator_to_array($child, false)];
                }
                $array[$tag.$i] = $child;
                $i++;
            }
        } else {
            // serialize attributes and text for a leaf-elements
            foreach ($this->attributes() as $name => $value) {
                $array["_$name"] = (string) $value;
            }
            $array["__text"] = (string) $this;
        }

        if ($this->xpath('/*') == array($this)) {
            // the root element needs to be named
            $array = [$this->getName() => $array];
        }

        return $array;
    }
}
