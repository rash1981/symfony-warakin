<?php

namespace AppSupply\WarakinBundle\Controller;

use AppSupply\WarakinBundle\Controller\WarakinController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Doctrine\ORM\EntityManager; 
use \SpotifyWebAPI\SpotifyWebAPI;
use AppSupply\WarakinBundle\Entity\Artist;


class DataController extends WarakinController
{
    
    public function datadumpAction()
    {
        $output = '';
        $data = mysql_query('SELECT concat(\'update tags set genre=\', concat(concat(genre, \' where url ="\'), concat(url, \'"\'))) FROM `tags`');
        while($dataRow = mysql_fetch_array($data)){
            $output .= $dataRow[0]."\n";
        }
        return new Response($output);
    }

    /*
     *    Artist data retrieval functions
     */

    public function artistsAction()
    {    
        $dataService = $this->get('warakin.service.data');
        return $this->getJsonResponse($dataService->getArtists());
    }



    public function artistAction($artistName)
    {    
        $dataService = $this->get('warakin.service.data');

        return $this->getJsonResponse($dataService->getArtist($artistName));
    }


    public function artistAlbumsAction($artistName, $albumName)
    {    
        $dataService = $this->get('warakin.service.data');
        
        return $this->getJsonResponse(
                $dataService->artistAlbums($artistName, $albumName)
            );
    }


    /*
     *    Artist data retrieveal functions
     */

    public function artistQueryAction($query)
    {    
        $dataService = $this->get('warakin.service.data');

        return $this->getJsonResponse(
            $dataService->getArtistQuery($query)
        );
    }


    /*
     *    Genre data retrieveal functions
     */

    public function genresAction()
    {   
        $genres = $this->get('doctrine')->getManager()->createQueryBuilder()
            ->select(
                array(
                    'g.id as id',
                    'g.name as name',
                    'count(t) as tagsCount',
                    'concat(\'data/genres/\', g.id) as dataUrl'
                )
            )
			->from('AppSupplyWarakinBundle:Genre', 'g')
	        ->leftJoin('g.tags','t')
		   	->having('tagsCount > 10')
            ->groupBy('g.id')
   			->orderBy('g.name', 'ASC')
		   	->getQuery()
		   	->getResult();

        return $this->getJsonResponse(
            array('genres'=>$genres)
        );
    }


    public function genreAction($genreName)
    {
        set_time_limit(0);
        $em = $this->get('doctrine')->getManager();

        if(is_numeric($genreName))
        {
            $genre = $em
                        ->getRepository('AppSupplyWarakinBundle:Genre')   
                           ->findOneById($genreName);
            
        }
        else
        {
            $genre = $em
                        ->getRepository('AppSupplyWarakinBundle:Genre')   
                           ->findOneByName($genreName);
        }


        $genreAlbumsDoctrine = $em->createQueryBuilder()
            ->select(
                array(
                    'distinct a.id',
                    'a.name',
                    'count(t.id) as trackCount',
                    'y.name as year',
                    'ar.id as artistId',
                    'ar.name as artistName',
                )
            )
			->from('AppSupplyWarakinBundle:Genre', 'g')
	        ->innerJoin('g.tags','t')
            ->innerJoin('t.album','a')
	        ->innerJoin('t.artist','ar')
	        ->innerJoin('t.year','y')
            ->where('g.id = :genreId')
            ->setParameter(':genreId', $genre->getId())
            ->groupBy('a.id')
            ->having('count(t) > 1')
            ->orderBy('a.name', 'ASC')
            //->setMaxResults(1)
		   	->getQuery()
            ->setHydrationMode(\Doctrine\ORM\AbstractQuery::HYDRATE_SINGLE_SCALAR)
            ->getResult();
        
        $genreAlbums = [];

        foreach($genreAlbumsDoctrine as $index=>$album){
            if($album){
                $artist = $this->get('doctrine')->getManager()
                ->getRepository('AppSupplyWarakinBundle:Artist')
                ->findOneById($album['artistId']);
                
                $album['artist'] = $artist;
                $genreAlbums[] = $album;
            }
        }

        //$genreAlbums = $ids;

        $genreArtistsDoctrine = $this->get('doctrine')->getManager()->createQueryBuilder()
            ->select(
                array(
                    'distinct a.id',
                    'a.name'
                    // 'a.name as name',
                    // 'count(t) as tagsCount',
                    // 'concat(\'data/genres/\', g.id) as dataUrl'
                )
            )
			->from('AppSupplyWarakinBundle:Genre', 'g')
	        ->innerJoin('g.tags','t')
            ->innerJoin('t.artist','a')
            ->where('g.id = :genreId')
            ->setParameter(':genreId', $genre->getId())
		   	//->having('tagsCount > 5')
            ->groupBy('a.id')
   			->orderBy('a.name', 'ASC')
		   	->getQuery()
            ->setHydrationMode(\Doctrine\ORM\AbstractQuery::HYDRATE_SINGLE_SCALAR)
            ->getResult();

        $genreArtists = array();

        
        foreach($genreArtistsDoctrine as $index=>$artist){
            if($artist){
                $genreArtists[] = $artist;
            }
        }

        // $genreTagCount = $this->get('doctrine')->getManager()->createQueryBuilder()
        //     ->select(
        //         array(
        //             'count(t) as tagsCount'
        //         )
        //     )
		// 	->from('AppSupplyWarakinBundle:Genre', 'g')
	    //     ->leftJoin('g.tags','t')
        //     ->where('g.id = :genreId')
        //     ->setParameter(':genreId', $genre->getId())
        //     ->groupBy('g.id')
		//    	->getQuery()
        //     ->getResult();

        return $this->getJsonResponse(
            array(
                'id'=>$genre->getId(),
                'name'=>$genre->getName(),
                //  'tagcount'=>$genreTagCount[0]['tagsCount'],
                'artists'=>$genreArtists,
                'albums'=>$genreAlbums,
            )
        );
    }


    public function genreArtistsAction($genreName)
    {     
        $em = $this->get('doctrine')->getManager();

        $genre = $em
                    ->getRepository('AppSupplyWarakinBundle:Genre')             
                    ->findOneByName($genreName);

        $genreArtists = array();
        foreach($genre->getArtists() as $index=>$artist){
            $genreArtists[]=$artist->getShort();
        }

        return $this->getJsonReponse(
            array('artists'=>$genreArtists)
        );
    }


    public function genreFavoriteArtistsAction($genreName)
    {     
        $em = $this->get('doctrine')->getManager();
        //$em->getRepository('...')->find($id);

        $genre = $em
                    ->getRepository('AppSupplyWarakinBundle:Genre')
                    ->findOneByName($genreName);

        $genreArtists = array();
        foreach($genre->getArtists() as $index=>$artist){
            $genreArtists[]=$artist->getShort();
        }

        return $this->getJsonResponse(
            array('artists'=>$genreArtists)
        );
    }

    public function genreAlbumsAction($genreName)
    {     
        $em = $this->get('doctrine')->getManager();

        $genre = $em
                    ->getRepository('AppSupplyWarakinBundle:Genre')             
                    ->findOneByName($genreName);

        $genreAlbums = array();
        foreach($genre->getAlbums() as $index=>$album){
            $genreAlbums[]=$album->getShort();
        }

        return $this->getJsonResponse(
            array('albums'=>$genreAlbums)
        );
    }

    public function genreFavoriteTracksAction($genreName)
    {     
        $em = $this->get('doctrine')->getManager();
        //$em->getRepository('...')->find($id);
        $genre = $em
                    ->getRepository('AppSupplyWarakinBundle:Genre')
                    ->findOneByName($genreName);

        $genreTracks = $em
                    ->getRepository('AppSupplyWarakinBundle:Statistics')
                    ->findBy(array('rating'=>'10'), array('rating'=>'ASC'));


        return $this->getJsonResponse(array('Tracks'=>$genreTracks));
    }



    /*
     *    Track data retrieveal functions
     */

    public function getTrackAction($trackId)
    {     
        $em = $this->get('doctrine')->getManager();
        //$em->getRepository('...')->find($id);

        $track =  $em
                    ->getRepository('AppSupplyWarakinBundle:Tags')             
                    ->findOneById($trackId);

        return $this->getJsonResponse(
            $track
        );
    }

    public function  getRelatedTracksAction($trackId)
    {
        $em = $this->get('doctrine')->getManager();
        //$em->getRepository('...')->find($id);

        $track =  $em
                    ->getRepository('AppSupplyWarakinBundle:Tags')             
                    ->findOneById($trackId);

        return $this->getJsonResponse(
            $track->getRelatedLastFmTracks()
        );
    }
    /**
     *    Utility Functions
     */

    public function getAlbumCover($artistName, $albumName, $album=false){

        $dataService = $this->get('warakin.service.data');
        return $dataService->getAlbumCover($artistName, $albumName, $album);
    }



    /*
     *    Track data retrieveal functions
     */

    public function albumAction($albumId){

        ini_set('memory_limit', '2048M');
        
        $em = $this->get('doctrine')->getManager();
        //$em->getRepository('...')->find($id);

        $album =  $em
                    ->getRepository('AppSupplyWarakinBundle:Album')             
                    ->findOneById($albumId);

        $artist  = $album->getArtist();

        return $this->getJsonResponse(
            array(
                //'url'=>$album->getUrl(),
                'title'=>$album->getname(),
                'artistName'=>$artist->getName(),
                'tracks'=>$album->getTags(),
                'cover'=>'data/albums/'.$album->getId().'/cover',
                'year'=>$album->getYear(),
                'stattest'=>'test'
            )
        );
    }


    /*
     *    Track data retrieveal functions
     */

    public function searchAction($searchStr){
        $dataService = $this->get('warakin.service.data');

        if(strlen($searchStr) > 2){
            return $this->getJsonResponse($dataService->searchLibrary($searchStr));
        }else{
            return $this->getJsonResponse(array());
        }
    }


    public function getNewArtistsAction(){

        $artistClient = $this->get('binary_thinking_lastfm.client.artist');
        $em = $this->get('doctrine')->getManager();

        $query = $em->createQuery(
                'SELECT 
                    a, 
                     COUNT(t.url) AS trackCount
                 FROM AppSupplyWarakinBundle:Artist a 
                     JOIN a.tags t 
                     JOIN t.statistics s 
                 GROUP BY a.id
                 HAVING trackCount > 4
                 ORDER BY s.createdate desc')->setMaxResults(16);
        $newArtistsResult = $query->getResult();

        $newArtists = array();
        foreach($newArtistsResult as $index=>$artist){
            $newArtist=$artist[0]->getShort();

            try {
                $artistLastfm = $artistClient->getInfo($artist[0]->getName());
            } catch (\Exception $e) {
                $artistLastfm = null;
            }

            $defaultImages = array('extralarge'=>'defaultimg');

            $newArtist['images'] = null;

            if($artistLastfm){
                $images = $artistLastfm->getImages();
                if($images){
                    $newArtist['images'] = $images;
                }
            }
            $newArtists[] = $newArtist;
        }

        return $newArtists;
    }

    /*
     *	Playlist actions
     */

    public function generatePlaylistIndexAction(){
		
        $dataService = $this->get('warakin.service.data');
        $genres = $dataService->getGenres();

        usort($genres, function($a, $b) {
		    return $b['favoritesCount'] - $a['favoritesCount'];
		});
       //213 var_dump($genres);

        return $this->render(
        	'AppSupplyWarakinBundle:Data:playlists.html.twig',
  			array(
  				"genres" => $genres
  			)
        );
    }

    /*
     *  Download playlists zipfile
     */

    public function generatePlaylistZipAction(){
        
        $dataService = $this->get('warakin.service.data');
        $genres = $dataService->getGenres();

        usort($genres, function($a, $b) {
            return $b['favoritesCount'] - $a['favoritesCount'];
        });

        $zip = new \ZipArchive();
        $zipName = 'Playlist-'.time().".zip";
        $zip->open($zipName,  \ZipArchive::CREATE);

        foreach($genres as $index=>$genre){
            //$args = "%7B'genres':[".$genre['id']."],'length':12000,'min_rating':7,%20'max_rating':10,%20'song_length':12%7D";
            $args = "{'genres':[".$genre['id']."],'length':12000,'min_rating':7,'max_rating':10,'song_length':12}";
            $m3uStr = $genrePlaylists[] = $this->generatePlaylistM3UStringAction($args);
            if($m3uStr != "#EXTM3U\n"){
                $zip->addFromString($genre['name'].'.m3u',  $m3uStr); 
            }
        }

        $zip->close();

        $response = new Response();

        header('Content-Type', 'application/zip');
        header('Content-disposition: attachment; filename="' . $zipName . '"');
        header('Content-Length: ' . filesize($zipName));
        ob_clean();
        flush();
        readfile($zipName);
        return $response;
    }

    public function generatePlaylistAction($args){
        $dataService = $this->get('warakin.service.data');
        
        return $this->getJsonResponse($dataService->generatePlaylist($args));
    }

    public function generatePlaylistM3UStringAction($args){

        $dataService = $this->get('warakin.service.data');
        $playlist = $dataService->generatePlaylist($args);
        $m3uStub =  "#EXTM3U\n";
       
        foreach($playlist as $index=>$item){
            //var_dump($item);
            $m3uStub .= "#EXTINF:".$item['length'].",[".$item['rating']."]".$item['artist']." - ".$item['title']."\n";
            $m3uStub .= str_replace('./media', '/media', $item['url'])."\n";
        }

        return $m3uStub;
    }

    public function generatePlaylistM3UAction($args){

        $m3uStub = $this->generatePlaylistM3UStringAction($args);

        $response = new Response();
        $response->headers->set('Content-Type', "text/m3u");
        //$response->headers->set('Content-Disposition', 'attachment; filename="'.$fileName.'"');
        $response->headers->set('Pragma', "no-cache");
        $response->headers->set('Expires', "0");
        $response->headers->set('Content-Transfer-Encoding', "binary");
        //$response->prepare();
        $response->sendHeaders();
        $response->setContent($m3uStub);
        //$response->sendContent();
        
        return $response;
    }

    public function logTrackPlayAction($trackId){
        $em = $this->get('doctrine')->getManager();
        $track = $em
                ->getRepository('AppSupplyWarakinBundle:Tags')             
                ->findOneById($trackId);
                
        // file_put_contents(
        //     "/sites/warakin.utf8/web/test/file-statistics/test-".date('Ymd_H.i.s').".".microtime()."-".$fileId.".log", 
        //     'Opend file ['.$tag->getUrl().'] at '.date('d-m-y h:i:s.u') 
        // );

        $result = false;

        if($track){
            $playEvent = new \AppSupply\WarakinBundle\Entity\PlayEvent();
            $playEvent->setTrack($track);
            $playEvent->setType("play");
            $em->persist($playEvent);
            $em->flush();
            $result = true;
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($result));
        return $response;
    }


    /*
	 *		File Streaming functions
     */

    public function streamFileByIdAction($trackId){
        $em = $this->get('doctrine')->getManager();
        $tag = $em
                ->getRepository('AppSupplyWarakinBundle:Tags')             
                ->findOneById($trackId);
                
        // file_put_contents(
        //     "/sites/warakin.utf8/web/test/file-statistics/test-".date('Ymd_H.i.s').".".microtime()."-".$fileId.".log", 
        //     'Opend file ['.$tag->getUrl().'] at '.date('d-m-y h:i:s.u') 
        // );

        if($tag){
            //$stats = $tag->getStatistics();
            
            if(!$tag->getStatistics()){
                $stats = new \AppSupply\WarakinBundle\Entity\Statistics();
                //$stats->setPlaycounter(1);
                $tag->setStatistics($stats);
                $stats->setUrl($tag->getUrl());

                try{
                    $em->persist($stats);
                } catch (\Doctrine\DBAL\DBALException $e) {
                    file_put_contents(
                        "/sites/warakin.utf8/web/test/file-statistics/test-".date('Ymd_H.i.s').".".microtime()."-".$trackId.".error", 
                        'Opend file ['.$tag->getUrl().'] at '.date('d-m-y H:i:s.u') ."\n".
                        'Caught exception: '.  $e->getMessage(). "\n"
                    );
                }
            }

            $playEvent = new \AppSupply\WarakinBundle\Entity\PlayEvent();
            $playEvent->setTrack($tag);
            $playEvent->setType("stream");
            $em->persist($playEvent);
        }
 
        try{
            $em->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            file_put_contents(
                "/sites/warakin.utf8/web/test/file-statistics/test-".date('Ymd_H.i.s').".".microtime()."-".$trackId.".error", 
                'Opend file ['.$tag->getUrl().'] at '.date('d-m-y h:i:s.u') ."\n".
                'Caught exception: '. $e->getMessage(). "\n"
            );
        }

        $format = pathinfo($tag->getUrl(), PATHINFO_EXTENSION);

        // return new \Symfony\Component\HttpFoundation\StreamedResponse(
        //     function () use ($file) {
        //         readfile($file);
        //     }, 200, array('Content-Type' => 'audio/' . $format)
        // );
        $response = new BinaryFileResponse($tag->getUrl());
        return $response;
    }

    public function setTrackRatingAction($trackId, $rating){
        $em = $this->get('doctrine')->getManager();

        $rating = ceil(floatval($rating)*2);
        //var_dump($url);
        //var_dump($track);
        var_dump($rating);
        

        $track =  $em
                    ->getRepository('AppSupplyWarakinBundle:Tags')             
                    ->findOneById($trackId);

        if(!$track){
            $url = str_replace('//', '/','./'.$url);
            $track =  $em
                    ->getRepository('AppSupplyWarakinBundle:Tags')             
                    ->findOneById($trackId);

        }
        //var_dump($url);
        //var_dump($track);

        $stats = $track->getStatistics();

        if($stats){
            //set stats
            $json = $stats->setRating($rating);
            $em->persist($stats);
            $em->flush();
        } else {
            // create stat and set rating
            $stats = new \AppSupply\WarakinBundle\Entity\Statistics();
            $stats->setRating($rating); 
            $json = $track->setStatistics($stats);

            $em->persist($track);
            $em->flush();
        }
        //var_dump($track);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($json));
        return $response;
    }

    public function setRatingAction($trackId, $rating){
        $em = $this->get('doctrine')->getManager();

        $rating = ceil(floatval($rating)*2);
        //var_dump($url);
        //var_dump($track);
        var_dump($rating);
        

        $track =  $em
                    ->getRepository('AppSupplyWarakinBundle:Tags')             
                    ->findOneById($trackId);

        if(!$track){
            $url = str_replace('//', '/','./'.$url);
            $track =  $em
                    ->getRepository('AppSupplyWarakinBundle:Tags')             
                    ->findOneById($trackId);

        }
        //var_dump($url);
        //var_dump($track);

        $stats = $track->getStatistics();

        if($stats){
            //set stats
            $json = $stats->setRating($rating);
            $em->persist($stats);
            $em->flush();
        } else {
            // create stat and set rating
            $stats = new \AppSupply\WarakinBundle\Entity\Statistics();
            $stats->setRating($rating); 
            $json = $track->setStatistics($stats);

            $em->persist($track);
            $em->flush();
        }
        //var_dump($track);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($json));
        return $response;
    }

    public function setGenreAction($trackId, $genreId){
        $em = $this->get('doctrine')->getManager();        

        $track =  $em
                    ->getRepository('AppSupplyWarakinBundle:Tags')             
                    ->findOneById($trackId);
        //var_dump($url);
        //var_dump($track);

        $genre = $em
                    ->getRepository('AppSupplyWarakinBundle:Genre')             
                    ->findOneById($genreId);

        if($genre){
            
            //set genre
            $json = $track->setGenre($genre);
            $em->persist($track);
            $em->flush();
            $json = [
                "status"=> true,
                "message"=>'Track '.$track->getTitle().' ('.$track->getId().' )genre set to '.$genre->getname()
            ];

        } else {
            // create stat and set genre
            $json = [
                "status"=> false,
                "message"=>'genre does not exist'
            ];
        }

        //var_dump($track);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode($json));
        return $response;

    }

    public function streamAlbumCoverFileAction($albumId){
        $dataService = $this->get('warakin.service.data');
        $em = $this->get('doctrine')->getManager();

        $album =  $em
                    ->getRepository('AppSupplyWarakinBundle:Album')             
                    ->findOneById($albumId);

        $cover = false;
        foreach ($album->getTags() as $index=>$track) {
            if($track->getArtist()){
                $coverTmp = $dataService->getAlbumCover($track->getArtist()->getName(), $album->getName(), $album, $track);
            }else{
                $coverTmp = $dataService->getAlbumCover('', $album->getName(), $album, $track);
            }
            if($coverTmp){$cover = $coverTmp;}
        }
        $response = new BinaryFileResponse($cover);
        return $response;
    }

    public function streamArtistCoverFileAction($artistId){
        $dataService = $this->get('warakin.service.data');
        $em = $this->get('doctrine')->getManager();

        $artist =  $em
                    ->getRepository('AppSupplyWarakinBundle:Artist')             
                    ->findOneById($artistId);

        $cover = false; //TODO: set to default warakin image
        
        // check for fetched file, if not see if fetchable
        $cover = '/sites/warakin.utf8/app/data/artist-covers/large/'.$artist->getId().'.png';

        if(!file_exists($cover)){
            $artistClient = $this->get('binary_thinking_lastfm.client.artist');

            try {
               $artistLastfm = $artistClient->getInfo($artist->getName());
            } catch (\Exception $e) {
                $artistLastfm = null;
            }

            $newArtist['images'] = null;

            if($artistLastfm){
                $images = $artistLastfm->getImages();
                if($images){
                    $image =  array_pop($images);

                    $string = file_get_contents($image);

                    if($string === FALSE) {
                        echo "Could not read the file.";
                    } else {
                        file_put_contents($cover, $string);
                    }
                
                }
            }

        }  
        
       
        
        
        $response = new BinaryFileResponse($cover);
        return $response;
    }



    public function playlistsAction()
    {    
        $dataService = $this->get('warakin.service.data');
        $playlists = $this->getJsonResponse($dataService->getPlaylists());
        return $playlists;
    }

    public function playlistsCreateAction()
    {    
        $result = false;
        if(array_key_exists('playlistName', $_GET)){

            // result should be none null & non false by returning result of doctrine insertion  
            $result = true; 
        }
        
        return $this->getJsonResponse($result);
    }



    public function playlistAction($playlistId)
    {    
        $dataService = $this->get('warakin.service.data');
        $playlist = $dataService->getPlaylist($playlistId);
        return $this->getJsonResponse(array(
            'id'=>$playlistId,
            'name'=>$playlist->getName(),
            'playlist_tracks'=>$playlist->getPlaylistTracks(),
            'type'=>$playlist->getType()
        ));
    }



    public function playlistM3uAction($playlistId)
    {    
        $dataService = $this->get('warakin.service.data');

        $playlist = $dataService->getPlaylist($playlistId);
        $tracks = $playlist->getPlaylistTracks();
        
       
        $m3uStub =  "#EXTM3U\n";
        foreach($tracks as $index=>$track){
            //var_dump($item);
            $item = $track->getTrack();
            if($item->getArtist()){
                $artist_name = $item->getArtist()->getName();
            }else{
                $artist_name = '';
            }
            $stats = $item->getStatistics();
            $rating = 0;
            if($stats){
                $rating = $stats->getRating();
            }
            $m3uStub .= "#EXTINF:".$item->getLength().",[".$rating."] ".$artist_name." - ".$item->getTitle()."\n";
            $m3uStub .= str_replace('./media', '/media', $item->getUrl())."\n";
        }
       

        $response = new Response();
        header('Content-Type', 'text/plain');
        header('Content-disposition: attachment; filename="' . $playlist->getName().'.m3u"');
        header('Content-Length: ' . strlen($m3uStub));
        echo $m3uStub;
        return $response;
    }



    public function playlistZipAction($playlistId)
    {    
        $dataService = $this->get('warakin.service.data');

        $playlist = $dataService->getPlaylist($playlistId);
        $tracks = $playlist->getPlaylistTracks();

        $zip = new \ZipArchive();
        $zipName = 'Playlist-'.$playlist->getName().".zip";
        $zip->open($zipName,  \ZipArchive::CREATE);
        
        
        foreach($tracks as $index=>$track){
            //var_dump($track);
            $timelimit = ini_get('max_execution_time');
            set_time_limit($timelimit + 30);
            $zip->addFile(str_replace('./media', '/media', $track->getTrack()->getUrl())); 
        }

        $zip->close();

        $response = new Response();

        header('Content-Type', 'application/zip');
        header('Content-disposition: attachment; filename="' . $zipName . '"');
        header('Content-Length: ' . filesize($zipName));
        ob_clean();
        flush();
        readfile($zipName);
        return $response;
    }



    public function playlistAddTrackAction($playlistId, $trackId)
    {    
        $em = $this->get('doctrine')->getManager();
        $playlist = $em
            ->getRepository('AppSupplyWarakinBundle:Playlists')   
            ->findOneById($playlistId);

        $track = $em
            ->getRepository('AppSupplyWarakinBundle:Tags')             
            ->findOneById($trackId);

        $playlistTrack = new \AppSupply\WarakinBundle\Entity\PlaylistTrack();
        $playlistTrack->setTrack($track);
        $playlistTrack->setOrder(count($playlist->getPlaylistTracks($track)));
        
        $playlist->addPlaylistTrack($playlistTrack);

        $em->persist($track);
        $em->flush();
        
        return $this->getJsonResponse(array('success'=>true));
    }
    
    public function updatePlaylistAction($playlistId, $args)
    {    
        $em = $this->get('doctrine')->getManager();
        $playlist = $em
            ->getRepository('AppSupplyWarakinBundle:Playlists')   
            ->findOneById($playlistId);

        $oldTracks = $playlist->getPlaylistTracks();
        
        foreach( $oldTracks as $key  => $val ){

            $playlist->removePlaylistTrack($val);

        }

        $em->persist($playlist);
        $em->flush();
        
        $args = json_decode($args);
        foreach($args as $key=>$trackId){
            $this->playlistAddTrackAction($playlistId, $trackId);
        }

        return $this->getJsonResponse(array('success'=>true, 'playlist'=>$playlist));
    }
    
    public function updateQueuePlaylistAction($args)
    {    
        $em = $this->get('doctrine')->getManager();
        $playlist = $em
            ->getRepository('AppSupplyWarakinBundle:Playlists')   
            ->findOneByName('LiveQueue');

        $oldTracks = $playlist->getPlaylistTracks();
        
        foreach( $oldTracks as $key  => $val ){

            $playlist->removePlaylistTrack($val);

        }

        $em->persist($playlist);
        $em->flush();
        
        $args = json_decode($args);
        foreach($args as $key=>$trackId){
            $this->playlistAddTrackAction($playlist->getId(), $trackId);
        }

        return $this->getJsonResponse(array('success'=>true, 'playlist'=>$playlist));
    }

    public function artistSinglesAction($artistId)
    {       
        $em = $this->get('doctrine')->getManager();
        $tags = $em
            ->getRepository('AppSupplyWarakinBundle:Tags')
            ->findBy(
                array(
                    'album'=>null,
                    'artist'=>$artistId
                ), 
                array('title'=>'ASC')
        );

        $singlesAlbum = array(
            'title'=>'Singles',
            'artistName'=>'artist',
            'cover'=>'./bundles/appsupplywarakin/img/defaultAlbum.png',
            'tracks'=>$tags,
            'year'=>null
        );

        return $this->getJsonResponse($singlesAlbum);
    }

    public function tagsAction($typeId=null){
        set_time_limit(0);
        $dataService = $this->get('warakin.service.data');
        return $this->getJsonResponse($dataService->getTags($typeId));
    }

    public function tagAction($tagId){
        $dataService = $this->get('warakin.service.data');
        return $this->getJsonResponse($dataService->getTag($tagId));
    }

}
