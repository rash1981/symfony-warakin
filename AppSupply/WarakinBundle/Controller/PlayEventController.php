<?php

namespace AppSupply\WarakinBundle\Controller;

use AppSupply\WarakinBundle\Controller\WarakinController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Doctrine\ORM\EntityManager; 
use \SpotifyWebAPI\SpotifyWebAPI;
use AppSupply\WarakinBundle\Entity\Artist;


class PlayEventController extends WarakinController
{


    /*
     *    PlayEvent data retrieveal functions
     */

    public function playEventsAction()
    {   
        $playEvents = $this->get('doctrine')->getManager()->createQueryBuilder()
            ->select(
                array(
                    'pe.id as id',
                    'pe.type as type',
                    't.id as track_id',
                    'pe.created as created',
                    'a.name',
                    't.title',
                    
                )
            )
			->from('AppSupplyWarakinBundle:PlayEvent', 'pe')
	        ->leftJoin('pe.track','t')
	        ->leftJoin('t.artist','a')
		   	//->having('tagsCount > 10')
            ->groupBy('pe.id')
               ->orderBy('pe.created', 'DESC')
               ->setMaxResults(600)
		   	->getQuery()
		   	->getResult();

        return $this->getJsonResponse(
            array('playEvents'=>$playEvents)
        );
    }


    public function playEventAction($playEventName)
    {     
        set_time_limit(0);
        $em = $this->get('doctrine')->getManager();

        if(is_numeric($playEventName))
        {
            $playEvent = $em
                        ->getRepository('AppSupplyWarakinBundle:PlayEvent')   
                           ->findOneById($playEventName);
            
        }
        else
        {
            $playEvent = $em
                        ->getRepository('AppSupplyWarakinBundle:PlayEvent')   
                           ->findOneByName($playEventName);
        }


        $playEventAlbumsDoctrine = $em->createQueryBuilder()
            ->select(
                array(
                    'distinct a.id',
                    'a.name',
                    'count(t.id) as trackCount',
                    'y.name as year',
                    'ar.id as artistId',
                    'ar.name as artistName',
                )
            )
			->from('AppSupplyWarakinBundle:PlayEvent', 'g')
	        ->innerJoin('g.tags','t')
            ->innerJoin('t.album','a')
	        ->innerJoin('t.artist','ar')
	        ->innerJoin('t.year','y')
            ->where('g.id = :playEventId')
            ->setParameter(':playEventId', $playEvent->getId())
            ->groupBy('a.id')
            ->having('count(t) > 1')
            ->orderBy('a.name', 'ASC')
            //->setMaxResults(1)
		   	->getQuery()
            ->setHydrationMode(\Doctrine\ORM\AbstractQuery::HYDRATE_SINGLE_SCALAR)
            ->getResult();
        
        $playEventAlbums = [];

        foreach($playEventAlbumsDoctrine as $index=>$album){
            if($album){
                $playEventAlbums[] = $album;
            }
        }

        //$playEventAlbums = $ids;

        $playEventArtistsDoctrine = $this->get('doctrine')->getManager()->createQueryBuilder()
            ->select(
                array(
                    'distinct a.id',
                    'a.name'
                    // 'a.name as name',
                    // 'count(t) as tagsCount',
                    // 'concat(\'data/playEvents/\', g.id) as dataUrl'
                )
            )
			->from('AppSupplyWarakinBundle:PlayEvent', 'g')
	        ->innerJoin('g.tags','t')
            ->innerJoin('t.artist','a')
            ->where('g.id = :playEventId')
            ->setParameter(':playEventId', $playEvent->getId())
		   	//->having('tagsCount > 5')
            ->groupBy('a.id')
   			->orderBy('a.name', 'ASC')
		   	->getQuery()
            ->setHydrationMode(\Doctrine\ORM\AbstractQuery::HYDRATE_SINGLE_SCALAR)
            ->getResult();

        $playEventArtists = array();

        
        foreach($playEventArtistsDoctrine as $index=>$artist){
            if($artist){
                $playEventArtists[] = $artist;
            }
        }

        // $playEventTagCount = $this->get('doctrine')->getManager()->createQueryBuilder()
        //     ->select(
        //         array(
        //             'count(t) as tagsCount'
        //         )
        //     )
		// 	->from('AppSupplyWarakinBundle:PlayEvent', 'g')
	    //     ->leftJoin('g.tags','t')
        //     ->where('g.id = :playEventId')
        //     ->setParameter(':playEventId', $playEvent->getId())
        //     ->groupBy('g.id')
		//    	->getQuery()
        //     ->getResult();

        return $this->getJsonResponse(
            array(
                'id'=>$playEvent->getId(),
                'name'=>$playEvent->getName(),
                //  'tagcount'=>$playEventTagCount[0]['tagsCount'],
                'artists'=>$playEventArtists,
                'albums'=>$playEventAlbums,
            )
        );
    }


    public function playEventArtistsAction($playEventName)
    {     
        $em = $this->get('doctrine')->getManager();

        $playEvent = $em
                    ->getRepository('AppSupplyWarakinBundle:PlayEvent')             
                    ->findOneByName($playEventName);

        $playEventArtists = array();
        foreach($playEvent->getArtists() as $index=>$artist){
            $playEventArtists[]=$artist->getShort();
        }

        return $this->getJsonReponse(
            array('artists'=>$playEventArtists)
        );
    }


    public function playEventFavoriteArtistsAction($playEventName)
    {     
        $em = $this->get('doctrine')->getManager();
        //$em->getRepository('...')->find($id);

        $playEvent = $em
                    ->getRepository('AppSupplyWarakinBundle:PlayEvent')
                    ->findOneByName($playEventName);

        $playEventArtists = array();
        foreach($playEvent->getArtists() as $index=>$artist){
            $playEventArtists[]=$artist->getShort();
        }

        return $this->getJsonResponse(
            array('artists'=>$playEventArtists)
        );
    }

    public function playEventAlbumsAction($playEventName)
    {     
        $em = $this->get('doctrine')->getManager();

        $playEvent = $em
                    ->getRepository('AppSupplyWarakinBundle:PlayEvent')             
                    ->findOneByName($playEventName);

        $playEventAlbums = array();
        foreach($playEvent->getAlbums() as $index=>$album){
            $playEventAlbums[]=$album->getShort();
        }

        return $this->getJsonResponse(
            array('albums'=>$playEventAlbums)
        );
    }

    public function playEventFavoriteTracksAction($playEventName)
    {     
        $em = $this->get('doctrine')->getManager();
        //$em->getRepository('...')->find($id);
        $playEvent = $em
                    ->getRepository('AppSupplyWarakinBundle:PlayEvent')
                    ->findOneByName($playEventName);

        $playEventTracks = $em
                    ->getRepository('AppSupplyWarakinBundle:Statistics')
                    ->findBy(array('rating'=>'10'), array('rating'=>'ASC'));


        return $this->getJsonResponse(array('Tracks'=>$playEventTracks));
    }



    /*
     *    Track data retrieveal functions
     */

    public function getTrackAction($trackId)
    {     
        $em = $this->get('doctrine')->getManager();
        //$em->getRepository('...')->find($id);

        $track =  $em
                    ->getRepository('AppSupplyWarakinBundle:Tags')             
                    ->findOneById($trackId);

        //$artist  = $track->getArtist();

        return $this->getJsonResponse(
            $track
        );
    }

}
