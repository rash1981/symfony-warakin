<?php

namespace AppSupply\WarakinBundle\Controller;

use AppSupply\WarakinBundle\Controller\WarakinController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Doctrine\ORM\EntityManager;
use \SpotifyWebAPI\SpotifyWebAPI;

/**
 *  Generates "next tracks" through association mapping between tracks and artists
 *  utilizing data from social media and track audio properties like BPM, chord keys, mood etc.
 * 
 *  
 * 
 */

class PlayNextController extends WarakinController  
{

    private $currentArtists;
    private $currentPlaylist;


    public function generateNextTrackAction($args, $trackCount=0, $currentRelatedTracks=array()){
        
        $currentPlayList = $this->generateNextTrack($args, $trackCount, $currentRelatedTracks);

        return $this->getJsonResponse($currentPlayList);

    }

    public function generateNextTrackJsonAction($args, $trackCount=0, $currentRelatedTracks=array()){
         
        $currentPlayList = $this->generateNextTrack($args, $trackCount, $currentRelatedTracks);
        $playlist = array(); 
        foreach($currentPlayList as  $index => $value){
            //var_dump($value);
            if($index != 0){
                $playlist[] = $value['track']; 
            }
        }
        return $this->getJsonResponse($playlist);
        
    }

    public function generateNextTrackM3uAction($args, $trackCount=0, $currentRelatedTracks=array()){
        
        $locale = $this->get('session')->getLocale();
        
        $response = new Response();
        $response->headers->set('Content-Type', 'text/plain');

        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'filename.txt'
        );
        $response->sendHeaders();

        return $this->render($view, array(), $response);


        $currentPlayList = $this->generateNextTrack($args, $trackCount, $currentRelatedTracks);

        echo $this->generatePlaylistM3UStringAction($currentPlayList);
        
        die();

    }

    public function generateNextTrackM3uZipAction($args, $trackCount=0, $currentRelatedTracks=array()){
        
        $currentPlayList = $this->generateNextTrack($args, $trackCount, $currentRelatedTracks);

        return $this->generatePlaylistM3UStringAction($currentPlayList);

    }

    /**
     *  The actual magic generating the tracks
     */


    public function generateNextTrack($args, $trackCount=1, $currentRelatedTracks=array(), $tracksToExclude=array()){

		$this->synchService = $this->get('warakin.service.synch');
        
        $listLength = $trackCount + 1;

        // Set up environmental vars

        $this->em = $this->get('doctrine')->getManager();

        $clientId = 'b1292efee02548e1889889416961d000';
        $clientSecret = '6bff08ff88864219bd98f0edb783e07e';
        $sesh = new \SpotifyWebAPI\Session($clientId, $clientSecret);
        //$this->spotifyApi = new \SpotifyWebAPI\SpotifyWebAPI($request);

        $fact = new \Nomad145\SpotifyBundle\Factory\SpotifyApiFactory($sesh);
        $this->spotifyApi = $fact->createSpotifyApi();
        
        //$this->trackClient = $this->get('binary_thinking_lastfm.client.track');
        $this->clientFactory = $this->get('binary_thinking_lastfm.client_factory');

        $this->trackClient = $this->clientFactory->getClient('track', '686f2a0a882274135e2fcb131ff1fe60', "d9e1ad43533ffcb93c291dea4f10e104");
        $this->artistClient = $this->clientFactory->getClient('artist', '686f2a0a882274135e2fcb131ff1fe60', "d9e1ad43533ffcb93c291dea4f10e104");
        
        //echo '## '.'Track id: '.$args."<br/>\n";
        set_time_limit(180);

        $this->currentTrack = $this->em
                    ->getRepository('AppSupplyWarakinBundle:Tags')             
                    ->findOneById($args);

        //var_dump ($this->currentTrack);
        //echo '## '.'Warakin track: '.$this->currentTrack->getArtist()->getName().' - '.$this->currentTrack->getTitle()."<br/>\n";

        $currentPlayList = array();
        $currentPlayList[] = array(
            'track' => $this->currentTrack,
            'suggestedTracks' => array()
        );

        $x = 0;
        while(count($currentPlayList)<$listLength+1){
            
            $index = count($currentPlayList)-1;
            
            //echo ($index).":".$currentPlayList[$index]['track']->getTitle()."<br/>\n";

            $this->currentTrack = $this->em
                    ->getRepository('AppSupplyWarakinBundle:Tags')             
                    ->findOneById($currentPlayList[$index]['track']->getId());

            $suggestedTracks = $this->generateNextTrackLoop($this->currentTrack);

            //echo 'count($suggestedTracks): '.count($suggestedTracks)."<br/>";

            if(count($suggestedTracks)<3){
                $regress = 1;
                if(count($currentPlayList)>1){
                    while(count($suggestedTracks)<5 ){
                        
                    // echo '## '.'found no associations'."<br/>";

                    $suggestedTracks = $this->generateNextTrackLoop($currentPlayList[$index-$regress]['suggestedTracks'][floor( rand(1,count($currentPlayList[$index-$regress]['suggestedTracks']) ) )-1]);
                    $regress++;
                    }
                }
            }
            
            //echo 'suggesteTracks: ' ;
            //var_dump($suggestedTracks);

            $currentPlayList[$index]['suggestedTracks'] = $suggestedTracks;

            $currentTrack = array(
                'track' => $suggestedTracks[floor( rand(1,count($suggestedTracks) ) )-1], 
                'suggestedTracks' => array()
            );
            if($currentTrack['track']){
                $currentPlayList = $this->addToTracklist($currentPlayList, $currentTrack);

                $x++;
                if($x>$listLength){
                    //echo('failed loop<br/><br/><br/>');
                }
            }else{
                die('{"error":"super rare track bro"}');
                //echo '{"error":"super rare track bro"}';
                //return array();
            }
        }
        //echo 'Did '.$x.' iterations'."<br/>\n";
        //foreach($currentPlayList as $i=>$t){
            //echo ($i+1).'. '.$t['track']->getArtist()->getName().' - '.$t['track']->getTitle()."<br/>\n";
        //}

        return $currentPlayList;
    }

    /**
     *  Fetch all track associations on Spotify
     *  match them to warakin db
     *  @return matching track array
     */

    private function generateNextTrackLoop(\AppSupply\WarakinBundle\Entity\Tags $seedTrack){//$args, $trackCount=0, $currentRelatedTracks=array()


        $relatedTracks = array();

        // get Spotify associations

        //$relatedTracks = array_merge($relatedTracks, $this->fetchSpotifyRelatedTracks($currentSpotifyTrack, $seedTrack));
        $relatedTracks = array_merge($relatedTracks, $this->getSpotifyArtistRelatedTracks($seedTrack));

        
        // get LastFm associations

        $relatedTracks = array_merge( $relatedTracks, $this->getLastFmTrackRelatedTracks($seedTrack) );
        $relatedTracks = array_merge( $relatedTracks, $this->getLastFmArtistRelatedTracks($seedTrack) );

    
        // Filter related by Genre; disabled cause it makes stuff too rigid

    /*
        if(isset($relatedTracks) && $seedTrack->getGenre()){
            //echo 'relatedTracksCount: '.count($relatedTracks)."\n";
            
            // filter genre ?
            $this->currentTrackGenreId = $seedTrack->getGenre()->getId();

            $relatedTracksFilteredByGenre = array(); 

            foreach($relatedTracks as $index=>$relatedTrack){
                if($relatedTrack->getGenre()){
                    $relatedTrackGenreId = $relatedTrack->getGenre()->getId();
                    //echo $relatedTrackGenreId.' == '.$this->currentTrackGenreId."\n";
                    //echo $relatedTrack->getArtist()->getName().' - '.$relatedTrack->getTitle()."\n";
                    //echo $this->currentTrack->getGenre()->getName().' == '.$relatedTrack->getGenre()->getName()."\n";

                    if($relatedTrackGenreId == $this->currentTrackGenreId){
                        //echo $relatedTrack->getGenre()->getName();
                        $relatedTracksFilteredByGenre[] = $relatedTrack;
                    }
                }
            }
            if(count($relatedTracksFilteredByGenre) > 0){
                $relatedTracks = $relatedTracksFilteredByGenre;
            }
        }else if(isset($currentRelatedTracks)){
            $relatedTracks =  $currentRelatedTracks;
            array_shift($relatedTracks);
        }
    */


        // do fancy spotify matching on notekey and intensity etc
 
        if(count($relatedTracks) > 0){
            // $nextTrack = $relatedTracks[ floor( rand(1,count($relatedTracks) ) )-1 ];
            // //echo 'next track:'.$nextTrack->getArtist()->getName().' - '.$nextTrack->getTitle()."<br/>\n";   
            // $trackCount++;
            // echo 'trackCount: '.$trackCount."<br/>\n";
            // if($trackCount < 60){
            //     $this->generateNextTrack($nextTrack->getId(), $trackCount, $relatedTracks);
            // }
            
            //echo 'geerateNExtLoop: relatedTracks:';
            //var_dump($relatedTracks);

            return $relatedTracks;
        }else{
            return false;
        }
    }


    private function getLastFmTrackRelatedTracks(\AppSupply\WarakinBundle\Entity\Tags $seedTrack){

        return $seedTrack->getRelatedLastFmTracks();

    }


    private function getLastFmArtistRelatedTracks(\AppSupply\WarakinBundle\Entity\Tags $seedTrack){

        return $this->getRandomTracksForArtist(
            $seedTrack->getArtist()->getRelatedLastFmArtists()
        );

    }

    private function getSpotifyArtistRelatedTracks(\AppSupply\WarakinBundle\Entity\Tags $seedTrack){

        return $this->getRandomTracksForArtist(
            $seedTrack->getArtist()->getRelatedSpotifyArtists()
        );

    }

    private function getRandomTracksForArtist($artistList){

        $randomTracks = array();

        foreach($artistList as $artist){
            
            //echo 'Related Last.fm Artist: '.$relatedArtistLastFm->getName()."<br/>\n";
            
            $randomTracks[] = $this->getRandomTrackForArtist($artist);

        }

        return $randomTracks;
    }

    private function getRandomTrackForArtist($artist){
        $temp_tracks = $artist->getTags();
        //var_dump(gettype($temp_tracks));
        if($temp_tracks && $temp_tracks){
            if(count($temp_tracks)>0){
                if(count($temp_tracks)==1){
                    return $temp_tracks[0];
                }else{
                    //echo 'Last fm track found in db:'.$temp_tracks[0]->getArtist()->getName().' - '.$temp_tracks[0]->getTitle()."<br/>\n";
                    // foreach($temp_tracks as $temp_track){
                    //     $relatedTracks[] = $temp_track;
                    // }
                    return $temp_tracks->get(array_rand($temp_tracks->toArray()));

                    //echo 'Last fm track found in db:'.$temp_tracks[0]->getArtist()->getName().' - '.$temp_tracks[0]->getTitle()."<br/>\n";
                }
            }
        }
    }


    private function addToTracklist($trackList, $newTrack){
        $found = false;
        foreach($trackList as $track){
            if($track['track']->getId() === $newTrack['track']->getId()){
                $found = true;
            }
        }

        if(!$found){
            $trackList[] = $newTrack;
        }

        return $trackList;
    }



    private function mergeTracklists($existingTrackList, $newTracklist){
       
        foreach($newTracklist as $track){

           $existingTrackList = $this->addToTracklist($existingTrackList, $track);

        }

        echo $this->generatePlaylistM3UStringAction( $existingTrackList );
    }



    public function generatePlaylistM3UStringAction($playlist){

        // $dataService = $this->get('warakin.service.data');
        // $playlist = $dataService->generatePlaylist($args);
        $m3uStub =  "#EXTM3U\n";

        //var_dump($playlist);
       
        foreach($playlist as $index=>$item){
            //var_dump($item);
            if($item['track']){
                //echo $item['track']->getArtist()->getName()." - ".$item['track']->getTitle();
                $m3uStub .= "#EXTINF:".$item['track']->getLength().",".$item['track']->getArtist()->getName()." - ".$item['track']->getTitle()."\n";
                $m3uStub .= str_replace('./media', '/media', $item['track']->getUrl())."\n";
            }
        }

        return $m3uStub;

    }



    /*
     *  Download playlists zipfile
     */

    public function generatePlaylistZipAction(){
        
        $dataService = $this->get('warakin.service.data');
        $genres = $dataService->getGenres();

        usort($genres, function($a, $b) {
            return $b['favoritesCount'] - $a['favoritesCount'];
        });
        // var_dump($genres);

        /*return $this->render(
            'AppSupplyWarakinBundle:Data:playlists.html.twig',
            array(
                "genres" => $genres
            )
        );*/

        $zip = new \ZipArchive();
        $zipName = 'Association-Playlist-'.time().".zip";
        $zip->open($zipName,  \ZipArchive::CREATE);

        foreach($genres as $index=>$genre){
            //$args = "%7B'genres':[".$genre['id']."],'length':12000,'min_rating':7,%20'max_rating':10,%20'song_length':12%7D";
            $args = "{'genres':[".$genre['id']."],'length':12000,'min_rating':7,'max_rating':10,'song_length':12}";
            $m3uStr = $genrePlaylists[] = $this->generatePlaylistM3UStringAction($args);
            if($m3uStr != "#EXTM3U\n"){
                $zip->addFromString($genre['name'].'.m3u',  $m3uStr); 
            }
        }

        $zip->close();

        $response = new Response();
        // $response->setContent(readfile("../web/".$zipName));
        // $response->headers->set('Content-Type', 'application/zip');
        // $response->header('Content-disposition: attachment; filename=../web/"'.$zipName.'"');
        // $response->header('Content-Length: ' . filesize("../web/" . $zipName));
        // $response->readfile("../web/" . $zipName);
        header('Content-Type', 'application/zip');
        header('Content-disposition: attachment; filename="' . $zipName . '"');
        header('Content-Length: ' . filesize($zipName));
        ob_clean();
        flush();
        readfile($zipName);
        return $response;
    }


    public function dirtyFetchAllSpotifyAudioFeaturesAction(){
        // fetch track from db
        $this->em = $this->get('doctrine')->getManager();

		$this->synchService = $this->get('warakin.service.synch');

        if(array_key_exists('offset', $_GET)){
            $offset = $_GET['offset'];
        }else{
            $offset = 30470;//14894+51+39;//15247 +708 + 75 + 507 +608 + 500 + 100;
        }
 
        $limit = 10;
        set_time_limit($limit*1.5);
        
        $qb = $this->em->getRepository('AppSupplyWarakinBundle:Tags')->createQueryBuilder('t');
        $tracksQ = $qb
                    ->leftJoin('t.spotifyAudioFeatures', 's')
                    ->where('t.title not like :title')
                    ->andWhere('s.id IS NULL')
                    ->andWhere('t.title not like :file_mp3')
                    ->andWhere('t.title not like :file_flac')
                    ->andWhere('t.title not like :file_mp4')
                    ->setParameter('title', '%')
                    ->setParameter('file_mp3', '%.mp3')
                    ->setParameter('file_flac', '%.flac')
                    ->setParameter('file_mp4', '%.m4a')
                    ->setFirstResult( $offset ) 
                    ->setMaxResults( $limit )
                    ->getQuery();
        
        //echo $tracksQ->getSQL();die();
        
        $tracks = $tracksQ->getResult();

        if(count($tracks)<1){die('no more to do');}
        
        $clientId = 'b1292efee02548e1889889416961d000';
        $clientSecret = '6bff08ff88864219bd98f0edb783e07e';
        $sesh = new \SpotifyWebAPI\Session($clientId, $clientSecret);
        //$this->spotifyApi = new \SpotifyWebAPI\SpotifyWebAPI($request);

        $fact = new \Nomad145\SpotifyBundle\Factory\SpotifyApiFactory($sesh);
        $this->spotifyApi = $fact->createSpotifyApi();

        $found =0;
        $nofound = 0;

        foreach($tracks as $index=>$track){
            if($track->getArtist()){
                $artist = $track->getArtist()->getName();
            } else {   
                $artist = 'no artist name ';
            }
            $stub = ' : '.$artist.' - '.$track->getTitle();
            if(!$track->getSpotifyAudioFeatures()){
                //see if spotify track can be found

                try {
                    $spotifyTrack = $this->synchService->fetchCurrentSpotifyTrack($track);
                } catch (\SpotifyWebAPI\SpotifyWebAPIException $e) {
                    echo ($index-$found).'<i style="color:red">'.$stub.' error: '.$e->getMessage().'</a>';
                }   

                // if spotify track was found, fetch it's audio features
                
                sleep(5);

                if(count($spotifyTrack->tracks->items)>0){
                    $this->fetchSpotifyAudioFeatures($spotifyTrack->tracks->items[0], $track);
                    sleep(5);
                    $found += 1;
                    echo $found.'<i style="color:darkgreen;">'.$stub.' found</i>';
                }else{
                    echo ($index-$found).'<i style="color:orange;">'.$stub.' was not found on spotify</i>'; 
                }

            }else{
                    echo ($index-$found).'<i style="color:orange;">'.$stub.' already in db</i>';
            }
            $nofound = $index - $found;
            echo "<br/>\n";
        }
        echo 'Found: '.$found."<br/>\n";
        echo 'Offset: '.($offset+$nofound)."<br/>\n";

        echo "<script>window['location'].href='?offset=".($offset+$nofound)."'</script>";

        die('done fetching');


        //return $this->getJsonResponse($track->getSpotifyAudioFeatures());
    }

    public function fetchSpotifyAudioFeaturesAction($trackId){
        // fetch track from db
        $this->em = $this->get('doctrine')->getManager();
        $track = $this->em
                    ->getRepository('AppSupplyWarakinBundle:Tags')             
                    ->findOneById($trackId);

        $this->synchService = $this->get('warakin.service.synch');

        if(!$track->getSpotifyAudioFeatures()){
            $clientId = 'b1292efee02548e1889889416961d000';
            $clientSecret = '6bff08ff88864219bd98f0edb783e07e';
            $sesh = new \SpotifyWebAPI\Session($clientId, $clientSecret);
            //$this->spotifyApi = new \SpotifyWebAPI\SpotifyWebAPI($request);

            $fact = new \Nomad145\SpotifyBundle\Factory\SpotifyApiFactory($sesh);
            $this->spotifyApi = $fact->createSpotifyApi();

            //see if spotify track can be found
            $spotifyTrack = $this->synchService->fetchCurrentSpotifyTrack($track);

            // if spotify track was found, fetch it's audio features 
            if($spotifyTrack){
                if(count($spotifyTrack->tracks->items) > 0){
                    $this->fetchSpotifyAudioFeatures($spotifyTrack->tracks->items[0], $track);
                }
            }
        }
        return $this->getJsonResponse($track->getSpotifyAudioFeatures());
    }

    

    private function fetchSpotifyAudioFeatures($currentSpotifyTrack, $warakinTrack){

        $spotifyAudioFeatures = $this->spotifyApi->getAudioFeatures(array($currentSpotifyTrack->id));

        $spotifyAudioFeaturesObj =  new \AppSupply\WarakinBundle\Entity\SpotifyAudioFeatures();
        $spotifyAudioFeatures = $spotifyAudioFeatures->{'audio_features'}[0];
        $spotifyAudioFeaturesObj->setSpotifyTrackId($spotifyAudioFeatures->id);
        $spotifyAudioFeaturesObj->setDanceability($spotifyAudioFeatures->danceability);
        $spotifyAudioFeaturesObj->setEnergy($spotifyAudioFeatures->energy);
        $spotifyAudioFeaturesObj->setMusicKey($spotifyAudioFeatures->key);
        $spotifyAudioFeaturesObj->setModality($spotifyAudioFeatures->mode);
        $spotifyAudioFeaturesObj->setSpeechiness($spotifyAudioFeatures->speechiness);
        $spotifyAudioFeaturesObj->setAcousticness($spotifyAudioFeatures->acousticness);
        $spotifyAudioFeaturesObj->setInstrumentalness($spotifyAudioFeatures->instrumentalness);
        $spotifyAudioFeaturesObj->setLiveness($spotifyAudioFeatures->liveness);
        $spotifyAudioFeaturesObj->setLoudness($spotifyAudioFeatures->loudness);
        $spotifyAudioFeaturesObj->setValence($spotifyAudioFeatures->valence);
        $spotifyAudioFeaturesObj->setTempo($spotifyAudioFeatures->tempo);
        $spotifyAudioFeaturesObj->setContentType($spotifyAudioFeatures->type);
        $spotifyAudioFeaturesObj->setUri($spotifyAudioFeatures->uri);
        $spotifyAudioFeaturesObj->setTrackHref($spotifyAudioFeatures->track_href);
        $spotifyAudioFeaturesObj->setAnalysisUrl($spotifyAudioFeatures->analysis_url);
        $spotifyAudioFeaturesObj->setDurationMs($spotifyAudioFeatures->duration_ms);
        $spotifyAudioFeaturesObj->setTimeSignature($spotifyAudioFeatures->time_signature);

        $spotifyAudioFeaturesObj->setTag($warakinTrack);

        $this->em->persist($spotifyAudioFeaturesObj);
        $this->em->flush();

        return $warakinTrack;
    }

}
