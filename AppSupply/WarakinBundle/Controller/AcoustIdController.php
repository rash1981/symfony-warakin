<?php

namespace AppSupply\WarakinBundle\Controller;

use AppSupply\WarakinBundle\Controller\WarakinController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Finder\Finder;
use Doctrine\ORM\EntityManager;
use \GetId3\GetId3Core as GetId3;


use AppSupply\WarakinBundle\Entity\AcoustId;
use AppSupply\WarakinBundle\Entity\Tags;
use AppSupply\WarakinBundle\Entity\Artist;
use AppSupply\WarakinBundle\Entity\Album;
use AppSupply\WarakinBundle\Entity\Year;


if (!function_exists('array_key_first')) {
	function array_key_first(array $arr) {
			foreach($arr as $key => $unused) {
					return $key;
			}
			return NULL;
	}
}

/*
	AcoustId controller contains functions to interact with the AcoudstID web api
*/

class AcoustIdController extends WarakinController
{

	public function getTrackAcoustIdAction(){

		set_time_limit(8000);

		$synchService = $this->get('warakin.service.synch');

		return $this->getJsonResponse(
			$synchService->synchSocialMediaRelatedArtists()
		);
	}

	function analyzeTracksAction(){

		$batchSize = 15;

		$em = $this->getDoctrine()->getManager();
		// $tracks = $em
		// 	->getRepository('AppSupplyWarakinBundle:Tags')
		// 	->findOneById( $id );

		if(array_key_exists('offset', $_GET)){
			$offset = intval($_GET['offset']);
		}else{
			$offset = 0;
		}

		$q = $em->createQuery('SELECT t FROM AppSupplyWarakinBundle:Tags t left join t.acoustId a where a.identity is null')->setMaxResults($batchSize)->setFirstResult($offset);//->setFirstResult($offset);
		//$q->setMaxResults(24000);
		$tracks = $q->getResult();

		foreach($tracks as $key=>$track){   
      $this->analyzeTrackAction($track);
		}

		//sleep(5);
    echo "<script>window['location'].href='?offset=".($offset+$batchSize)."'</script>";
		die();
		$response = new Response();
		$response->headers->set('Content-Type', 'application/json');
		$response->setContent(array('blegh'));

		return $response;
    }

    
    function analyzeTrackAction($track){

			$em = $this->getDoctrine()->getManager();
			$currentLocale = setlocale(LC_ALL, array('en_US.utf8'));
			putenv('LC_ALL='.$currentLocale);

			//$urlEncoded = '/media/MP3.3/G/Gorillaz\ -\ Demon\ Days/02\ gorillaz\ -\ last\ living\ souls.mp3';
			$id = $track->getId(); //31551;
			$url = $track->getUrl();//'/media/MP3.3/G/Gorillaz - Demon Days/02 gorillaz - last living souls.mp3';
			echo '<h3>Track ('.$id.') url: '.$url . '<br/>';
			//if(file_exists($url) && !file_exists('/media/junk/Essentia/'.$id.'_frames.json') ){

			$output = [];

			//$url = str_replace(' ', '\ ', $url);
			
			//var_dump($url);

			$cmd = 'fpcalc '.escapeshellarg( $url );

			//var_dump($cmd);
			// echo $currentLocale;
			// // $locale='en_US.UTF-8';
			// setlocale(LC_ALL,$locale);

			//echo exec('locale charmap');
			$outputTxt = exec($cmd, $output, $stuff);

			echo '<h3>Output</h3><pre>';
			var_dump($output);
			echo '</pre>';

			$identity = false;

			if( array_key_exists(2, $output)){

				// identity expresses the audio fingerprint
				$identity = substr($output[2],12);
				// duration expresses the length of track used to generate the fingerprint
				$duration = substr($output[1],9);

			}elseif( array_key_exists(1, $output)){
				if(strpos($output[1], "FINGERPRINT") !== false){
				// identity expresses the audio fingerprint
					$identity = substr($output[1],12);
					// duration expresses the length of track used to generate the fingerprint
					$duration = substr($output[0],9);
				}
			}

			if($identity ){
				// echo '<h3>OutputTxt</h3><pre>';
				// var_dump($outputTxt);
				// echo '</pre><br/><br/>';
		
				$acoustId = $track->getAcoustId();
				// echo '<br/><h3>AcoustId</h3><pre>';
				// var_dump($acoustId);
				// echo '</pre><br/>';
				
				if(!$acoustId){
					$acoustId = new AcoustId();
				}
		
				echo '<h3>Duration</h3><pre>';
				var_dump($duration);
				echo '</pre><h3>Identity</h3><pre>';
				var_dump($identity);
				echo '</pre><br/><br/>';
		
				$acoustId->setIdentity($identity);
				$acoustId->setDuration($duration);
				
				$track->setAcoustId($acoustId);
				
				$em->persist($track);
				$em->flush();
		
				// }else if(file_exists($url) && file_exists('/media/junk/Essentia/'.$id.'_frames.json') ){
				//     $outputTxt = 
		
				// }

			}else{
				echo " something is up, fix <br/>";
			}
      return $output;
	}

	public function getStatsAction(){

		$stats = [];

		$em = $this->getDoctrine()->getManager();


		$count = $em->createQueryBuilder()->select('count(t)')
			->from('AppSupplyWarakinBundle:Tags','t')
			//->andWhere('lfti is not null')
			//->groupBy('t')
			->getQuery()
			->getResult();
		
		$stats["TotalTracks"] = $count[0][1];

		$count = $em->createQueryBuilder()->select('count(t)')
			->from('AppSupplyWarakinBundle:Tags','t')
			->leftJoin('t.acoustId', 'a')
			->where('a is null')
			->getQuery()
			->getResult();
		
		$stats["NoAcoustIdData"] = $count[0][1];

		
		$count = $em->createQueryBuilder()->select('count(t)')
			->from('AppSupplyWarakinBundle:Tags','t')
			->leftJoin('t.acoustId', 'a')
			->where('a.identity is not null')
			->getQuery()
			->getResult();
		
		$stats["AcoustIdFingerprint"] = $count[0][1];

		
		$count = $em->createQueryBuilder()->select('count(t)')
			->from('AppSupplyWarakinBundle:Tags','t')
			->leftJoin('t.acoustId', 'a')
			->where('a.acoustid is not null')
			->getQuery()
			->getResult();
		
		$stats["AcoustIds"] = $count[0][1];

		$count = $em->createQueryBuilder()->select('count(distinct a.recording_mbid)')
			->from('AppSupplyWarakinBundle:Tags','t')
			->leftJoin('t.acoustId', 'a')
			->where('a.recording_mbid is not null')
			->getQuery()
			->getResult();
		
		$stats["UniqueRecordingMbids"] = $count[0][1];

		$count = $em->createQueryBuilder()->select('count(distinct a.artist_mbid)')
			->from('AppSupplyWarakinBundle:Tags','t')
			->leftJoin('t.acoustId', 'a')
			->where('a.artist_mbid is not null')
			->getQuery()
			->getResult();

		$stats["UniqueArtistMbids"] = $count[0][1];

		$count = $em->createQueryBuilder()->select('count(distinct a.release_mbid)')
			->from('AppSupplyWarakinBundle:Tags','t')
			->leftJoin('t.acoustId', 'a')
			->where('a.release_mbid is not null')
			->getQuery()
			->getResult();

		$stats["UniqueReleaseMbids"] = $count[0][1];

		$count = $em->createQueryBuilder()->select('count(t)')
			->from('AppSupplyWarakinBundle:Tags','t')
			->leftJoin('t.acoustId', 'a')
			->where('a.apiResult is not null')
			->andWhere('a.apiResult != :emptyParam')
			->setParameter('emptyParam', '')
			->getQuery()
			->getResult();

		$stats["ApiResponses"] = $count[0][1];


		
		$response = new Response();
		$response->headers->set('Content-Type', 'application/json');
		$response->setContent(json_encode($stats));

		return $response;
	}

	function fetchTracksMetaDataAction(){

		$batchSize = 12;

		$em = $this->getDoctrine()->getManager();

		if(array_key_exists('offset', $_GET)){
			$offset = intval($_GET['offset']);
		}else{
			$offset = 0;
		}

		
		// $q = $em->getRepository("AppSupplyWarakinBundle:Tags")->createQueryBuilder('t')
		// 		->where('
		// 			t.title LIKE :fileExtension1 OR 
		// 			t.title LIKE :fileExtension2 OR 
		// 			t.title LIKE :fileExtension3 OR  
		// 			t.title LIKE :fileExtension4 OR 
		// 			t.title LIKE :fileExtension5 OR 
		// 			t.title LIKE :fileExtension6 OR 
		// 			t.title LIKE :fileExtension7 OR 
		// 			t.title LIKE :fileExtension8 OR 
		// 			t.title LIKE :fileExtension9 OR 
		// 			t.title LIKE :fileExtension10 OR 
		// 			t.title LIKE :fileExtension11 OR 
		// 			t.title LIKE :fileExtension12 OR
		// 			t.title LIKE :fileExtension13 OR 
		// 			t.title LIKE :fileExtension14 OR 
		// 			t.title LIKE :fileExtension15 OR
		// 			t.title LIKE :track1 OR
		// 			t.title LIKE :track2 OR
		// 			t.title LIKE :track3
		// 		')
		// 		->setParameter('fileExtension1', '%.mp3')
		// 		->setParameter('fileExtension2', '%.Mp3')
		// 		->setParameter('fileExtension3', '%.MP3')
		// 		->setParameter('fileExtension4', '%.flac')
		// 		->setParameter('fileExtension5', '%.Flac')
		// 		->setParameter('fileExtension6', '%.FLAC')
		// 		->setParameter('fileExtension7', '%.mp4')
		// 		->setParameter('fileExtension8', '%.Mp4')
		// 		->setParameter('fileExtension9', '%.MP4')
		// 		->setParameter('fileExtension10', '%.m4a')
		// 		->setParameter('fileExtension11', '%.M4a')
		// 		->setParameter('fileExtension12', '%.M4A')
		// 		->setParameter('fileExtension13', '%.wav')
		// 		->setParameter('fileExtension14', '%.Wav')
		// 		->setParameter('fileExtension15', '%.WAV')
		// 		->setParameter('track1', '%track%')
		// 		->setParameter('track2', '%Track%')
		// 		->setParameter('track3', '%TRACK%')
		// 		->setMaxResults($batchSize)
		// 		->setFirstResult($offset)
		// 		->getQuery();


		
		// $q = $em->getRepository("AppSupplyWarakinBundle:Tags")->createQueryBuilder('t')
		// 		->setMaxResults($batchSize)
		// 		->setFirstResult($offset)
		// 		->getQuery();

		
		$q = $em->createQuery('SELECT t FROM AppSupplyWarakinBundle:Tags t left join t.acoustId a where  a.apiResult is null  order by a.updated desc')->setMaxResults($batchSize)->setFirstResult(3);//;->setFirstResult($offset);

		// $builder->andWhere('type IN (:string)');
		// $builder->setParameter('string', array('first','second'), \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
		
		//$q->setMaxResults(24000);
		$tracks = $q->getResult();
		if (!$tracks){
			die('fetchTracksMetaDataAction: broken sql');
		}else{
			$results = [];
			foreach($tracks as $key=>$track){   
				echo $track->getTitle()." <br/>\n";
				echo "------------------------------------------------------------------------------------- <br/>\n<pre>";
				$result = $this->fetchTrackMetaData($track);
				echo "</pre>\n------------------------------------------------------------------------------------- \n<br/>\n<br/>\n";
				if($result){
					//compare stuff 
					$results[$track->getId()] = $result;
				}
			}
			//$myfile = file_put_contents('AcoustIdScan.json', json_encode($results).PHP_EOL , FILE_APPEND | LOCK_EX);
		}

		//sleep(5);
        echo "<script>window['location'].href='?offset=".($offset+$batchSize)."'</script>";
		die();
		$response = new Response();
		$response->headers->set('Content-Type', 'application/json');
		$response->setContent(array('blegh'));

		return $response;
	
	}
	
	function fetchTrackMetaData($track){
		// get AcoustId by fingerprint

		//meta
		$acoustId = $track->getAcoustId();

		// if acoustid present
		if($acoustId){
			//check if it's only a fingerprint, or that an acoustId has been acquired
			// if it's a only a fingerprint, fetch metdata and store it
			if(!$acoustId->getAcoustid() || !$acoustId->getApiResult()){
				$metas = [
					'recordings+releases'//, 'recordings', 'recordingids', 'releases', 'releaseids', 'releasegroups', 
					//'releasegroupids', 'tracks', 'compress', 'usermeta', 'sources'
				];

				$fetchAcoustIdUrl = 'https://api.acoustid.org/v2/lookup?client=tbmehmTjaz&duration='.
					$acoustId->getDuration().
					'&fingerprint='.$acoustId->getIdentity();

				try{
					$acoustIdResponse = json_decode(file_get_contents($fetchAcoustIdUrl));
				}catch( \Exception $e ){
					echo $e->getMessage()." <br/>\n";
					return false;
				}
				//var_dump($acoustIdResponse);

				$recordingPickedByDuration = null;
				$recordingPickedByYear = null;
				$recordingPickedByDirname = null;

				$apiResultAggregate = [
					"acoustIdLookup" => $acoustIdResponse,
					"candidateRecordings" => []
				];

				foreach($acoustIdResponse->results as $key=>$acoustIdKey){
					if($acoustIdKey->score > .8){
						// // fetch MusicBrainzId by AcoustId
						
						// $fetchAcoustIdMetaUrl = 'https://api.acoustid.org/v2/lookup?client=tbmehmTjaz&meta=recordingids&trackid='.$acoustIdKey->id;
						// $acoustIdMetaResponse = file_get_contents($fetchAcoustIdMetaUrl);

						//var_dump(json_decode($acoustIdMetaResponse));
						// fetch releases metadata from AcoustId API

						// $fetchAcoustIdMetaUrl = 'https://api.acoustid.org/v2/lookup?client=tbmehmTjaz&meta=releases&trackid='.$acoustIdKey->id;
						// $acoustIdMetaResponse = file_get_contents($fetchAcoustIdMetaUrl);

						//var_dump(json_decode($acoustIdMetaResponse));

						// $fetchAcoustIdMetaUrl = 'https://api.acoustid.org/v2/lookup?client=tbmehmTjaz&meta=recordings&trackid='.$acoustIdKey->id;
						// $acoustIdMetaResponse = file_get_contents($fetchAcoustIdMetaUrl);

						//var_dump(json_decode($acoustIdMetaResponse));


						// loop through requestable data
						$apiResultAggregate["candidateRecordings"][$key] = [];
						foreach($metas as $i=>$meta){
							$fetchAcoustIdMetaUrl = 'https://api.acoustid.org/v2/lookup?client=tbmehmTjaz&meta='.$meta.'&trackid='.$acoustIdKey->id;
							$acoustIdMetaResponse = json_decode(file_get_contents($fetchAcoustIdMetaUrl));

							$apiResultAggregate["candidateRecordings"][$key][$meta] = $acoustIdMetaResponse;

							//echo $meta." <br/>\n";
							//var_dump($acoustIdMetaResponse);

							if($meta === 'recordings+releases'){
								//var_dump($acoustIdMetaResponse->results);
								// loop through found recordings to 
								// compare with track to determine the closest match
								// see which duration matches best 
								$duration = $track->getLength();
								//var_dump($acoustIdMetaResponse->results);


								// loop through found releases to 
								// compare with track to determine the closest match
								// - find the release with the potential artist
								// - find the oldest release entry
								// - prefer between the 5 and 20 tracks 
								//   (but settle for less)

								// find recording with a release matching the dirname
								foreach($acoustIdMetaResponse->results as $k=>$value){

									if(property_exists($value, 'recordings')){

										foreach($value->recordings as $recording){
											//var_dump($record);

											if(property_exists($recording, 'releases')){
												foreach($recording->releases as $release){
													//	echo $release->title."\n";
													
													// see what release matching Dirname can be found. If multiple matches, compare by oldest release.
													if(property_exists($release, 'title')){
														//var_dump($release);
														$urlParts = explode("/", $track->getUrl());
														$isNum = is_numeric(strpos(strtolower($urlParts[count($urlParts)-2]), strtolower($release->title)));
														if( $isNum === true){
															//echo $urlParts[count($urlParts)-2]. " :: ".$release->title."::".(strpos(strtolower($urlParts[count($urlParts)-2]), strtolower($release->title)))."\n";
															//var_dump($isNum);
															if(property_exists($release, 'releaseevents')){
																if(property_exists($release->releaseevents[0], 'date')){
																	if($recordingPickedByDirname === null){

																		$recordingPickedByDirname = $recording;
																		$recordingPickedByDirname->releases = array($release);
																		$recordingPickedByDirname->acoustid = $acoustIdKey->id;
																		//echo "Dirname set to ".$recordingPickedByDirname->releases[0]->title." (".$release->releaseevents[0]->date->year.")\n";

																	}else if($release->releaseevents[0]->date->year < $recordingPickedByDirname->releases[0]->releaseevents[0]->date->year){
																		
																		$recordingPickedByDirname = $recording;
																		$recordingPickedByDirname->releases = array($release);
																		$recordingPickedByDirname->acoustid = $acoustIdKey->id;
																		//echo "Dirname set to ".$recordingPickedByDirname->releases[0]->title." (".$release->releaseevents[0]->date->year.")\n";

																	}
																}
															}elseif($recordingPickedByDirname === null){
																$recordingPickedByDirname = $recording;
																$recordingPickedByDirname->releases = array($release);
																$recordingPickedByDirname->acoustid = $acoustIdKey->id;
																$recordingPickedByDirname->releases[0]->releaseevents[] = (object) array('date' =>(object) array('year' => date('Y')));
																//echo "Dirname set to ".$recordingPickedByDirname->releases[0]->title." (no date)\n";
															}
														}
													}
												}
											}
										}
									}
								}

								// find recording with exactly matching length
								foreach($acoustIdMetaResponse->results as $k=>$value){
									//$duration
									if(property_exists($value, 'recordings')){
										foreach($value->recordings as $recording){
											if(property_exists($recording, 'duration')){
												if($recording->duration == $duration){
													//var_dump($recording->releases[0]->title);
													//$recordingPickedByDuration->releases = array($recording);
													if(property_exists($recording, 'releases')){
														foreach($recording->releases as $release){
															if(property_exists($release, 'releaseevents')){
																if(property_exists($release, 'date')){
																	if($recordingPickedByDuration === null){
																		$recordingPickedByDuration = $recording;
																		$recordingPickedByDuration->releases = array($release);
																		$recordingPickedByDuration->acoustid = $acoustIdKey->id;
																	}else if($release->releaseevents[0]->date->year < $recordingPickedByDuration->releases[0]->releaseevents[0]->date->year){
																		$recordingPickedByDuration->releases = array($release);
																		$recordingPickedByDuration->acoustid = $acoustIdKey->id;
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}

								// find earliest recording featuring the track
								foreach($acoustIdMetaResponse->results as $k=>$value){
									if(property_exists($value, 'recordings')){
										foreach($value->recordings as $recording){
											if(property_exists($recording, 'releases')){
												foreach($recording->releases as $release){
													// see what earliest year of release can be found
													if(property_exists($release, 'releaseevents') && strpos( strtolower($track->getTitle()), strtolower($recording->title) )){
														if( property_exists($release->releaseevents[0], 'date') ){
															// echo 'strpos( strtolower('.$track->getTitle().'), strtolower('.$recording->title.') ): ';
															// var_dump( strpos( strtolower($track->getTitle()), strtolower($recording->title) ) ); 
															if($recordingPickedByYear === null){
																// if pickedRelease is null, set a release
																$recordingPickedByYear = $recording;
																//$recordingPickedByYear->releases = array();
																$recordingPickedByYear->releases = array($release);
																$recordingPickedByYear->acoustid = $acoustIdKey->id;

															}else if($release->releaseevents[0]->date->year < $recordingPickedByYear->releases[0]->releaseevents[0]->date->year){
																// if release is older then the picked release, set it as the picked release
																$recordingPickedByYear = $recording;
																//$recordingPickedByYear->releases = array();
																$recordingPickedByYear->releases = array($release);
																$recordingPickedByYear->acoustid = $acoustIdKey->id;

															}else if($release->track_count < $recordingPickedByYear->releases[0]->track_count){
																// if release is as old as the picked release, but has less tracks on it, set it as the picked release
																$recordingPickedByYear = $recording;
																//$recordingPickedByYear->releases = array();
																$recordingPickedByYear->releases = array($release);
																$recordingPickedByYear->acoustid = $acoustIdKey->id;

															}
														}
													}
												}
											}
										}
									}
								}
								sleep(1);
							}
						}
						sleep(2);
					}
					//var_dump($acoustIdResponse);
				}
				// Prefer PickedByDirname over PickedByYear
				// If PickedByDuration is available, see how much diffrence in playlength there is between the two.
				// If the difference is less then 5 seconds, pick byDirname,. if not, default to ByDuration
				// PickedByDirname->Recording->duration - Track->length  PickedByDuration->Recording->duration - Track->length


				if(
					$recordingPickedByDirname != null &&
					$recordingPickedByYear != null &&
					$recordingPickedByDuration != null
				){
					if(
						$recordingPickedByDirname->id === $recordingPickedByYear->id &&
						$recordingPickedByYear->id === $recordingPickedByDuration->id
					){
						$score = 100;
					}else if(
						$recordingPickedByDirname->id === $recordingPickedByYear->id ||
						$recordingPickedByDirname->id === $recordingPickedByDuration->id ||
						$recordingPickedByYear->id === $recordingPickedByDuration->id
					){
						$score = 90;
					}else{
						$score = 70;
					}
				}else if(
					$recordingPickedByDirname != null &&
					$recordingPickedByYear != null
				){
					if($recordingPickedByDirname->id === $recordingPickedByYear->id){
						$score = 80;
					}else{
						$score = 60;
					}
				}else if(
					$recordingPickedByDirname != null &&
					$recordingPickedByDuration != null
				){
					if($recordingPickedByDirname->id === $recordingPickedByDuration->id){
						$score = 80;
					}else{
						$score = 60;
					}
				}else if(
					$recordingPickedByYear != null &&
					$recordingPickedByDuration != null
				){
					if($recordingPickedByYear->id === $recordingPickedByDuration->id){
						$score = 70;
					}else{
						$score = 50;
					}
				}else if(
					$recordingPickedByDirname != null 
				){
					$score = 50;

				}else if(
					$recordingPickedByYear != null
				){
					$score = 30;
				}else if(
					$recordingPickedByDuration != null
				){
					$score = 20;
				}


				if($recordingPickedByDirname != null){
					echo(
						"PickedByDirname(".$score.")	: #".$recordingPickedByDirname->releases[0]->id." : ".$recordingPickedByDirname->artists[0]->name.
						" - ".$recordingPickedByDirname->releases[0]->title." (".$recordingPickedByDirname->releases[0]->releaseevents[0]->date->year.") - ".
						$recordingPickedByDirname->title." <br/>\n"
					);
					$recordingPickedByDirname->score = $score;
					$this->commitAcoustidMetaData($acoustId, $recordingPickedByDirname, $apiResultAggregate);
				}

				if($recordingPickedByYear != null){
					echo(
						"PickedByYear(".$score.")	: #".$recordingPickedByYear->releases[0]->id." : ".$recordingPickedByYear->artists[0]->name.
						" - ".$recordingPickedByYear->releases[0]->title." (".$recordingPickedByYear->releases[0]->releaseevents[0]->date->year.") - ".
						$recordingPickedByYear->title." <br/>\n"
					);
					$recordingPickedByYear->score = $score;
					$this->commitAcoustidMetaData($acoustId, $recordingPickedByYear, $apiResultAggregate);
				}
				
				if($recordingPickedByDuration != null){
					echo(
						"PickedByDuration(".$score.")	: #".$recordingPickedByDuration->releases[0]->id." : ".$recordingPickedByDuration->artists[0]->name.
						" - ".$recordingPickedByDuration->releases[0]->title." (".$recordingPickedByDuration->releases[0]->releaseevents[0]->date->year.") - ".
						$recordingPickedByDuration->title." <br/>\n"
					);
					$recordingPickedByDuration->score = $score;
					$this->commitAcoustidMetaData($acoustId, $recordingPickedByDuration, $apiResultAggregate);
				}


				if($recordingPickedByDirname != null){
					return $recordingPickedByDirname;
				}else if($recordingPickedByYear != null){
					return $recordingPickedByYear;
				}else if($recordingPickedByDuration != null){
					return $recordingPickedByDuration;
				}
				
				echo "No Matches found. <br/>\n";

				$this->commitApiResult($acoustId, $apiResultAggregate);
				return false;
			}else{
				// return AcoustId as result;
				echo "AcoustId was set previously. <br/>\n";
				return $acoustId;
			}
		}else{
			echo "No AcoustId. <br/>\n";
			return false;
		}
	}


	function sanitizeAlbumsAction(){
		set_time_limit(0);
		ini_set('memory_limit', '2048M');
		/**
		 *  sanitize albums
		 * similar_text ( string $first , string $second ) 
		 * 
		 *    select all tracks for album.id
		 *  
		 *  loop through all tracks to 
		 *    - find the acoustid.api_result.release most present in the trakcs associated with the album
		 *    - compare retrieved acoustid.release_name(s) to track.album.name using similar_text to determine final album selection
		 */

		// select al.id, count(ac.release_mbid) as releaseMbidCount
		// from 
		// 	tags as t left join 
		// 	album as al on t.album = al.id left join
		// 	acoustid as ac on t.id = ac.tags_id
		// where al.id is not null
		// group by al.id
		// having
		// 	releaseMbidCount > 1
		// order by releaseMbidCount desc;

		$em = $this->getDoctrine()->getManager();
		$propName = 'recordings+releases'; //stupid fix for using a plus in a prop name

		$result = $em->createQuery(
												' SELECT 
															a, a.id, count(ac.release_mbid) as rMbid
													FROM 
														AppSupplyWarakinBundle:Album a left join 
														a.tags as t left join 
														t.acoustId as ac
													GROUP BY
														a
													HAVING
														rMbid > 0
													ORDER BY
														 rMbid desc'
												)->setMaxResults(80)->setFirstResult(0)->getResult();
		//var_dump($result);

		foreach($result as $resultIndex => $resultAlbum ){
			$tracks = $resultAlbum[0]->getTags();

			echo '<h3>Album: '.$resultAlbum[0]->getName().' ('.count($tracks).')</h3>';

			$releaseCandidates = [];

			foreach($tracks as $trackindex => $albumTrack){
				/**
				 * 	For each track, determine which mbid releases are associated with the track
				 *  and store them in the releaseCandidates array, counting the occurences
				 */

				$artist = $albumTrack->getArtist() ? $albumTrack->getArtist()->getName() : "[No artist]"; 
				//echo $artist." - ".$albumTrack->getTitle().'<br/>';

				$acoustId = $albumTrack->getAcoustId();
				if($acoustId){
					$acApiResult = json_decode( $acoustId->getApiResult() );
	
					//var_dump($acApiResult->candidateRecordings);echo '<br/><br/>';
					
					// $.candidateRecordings[0]."recordings+releases".results[0].id
					$trackMatch = [];
					foreach( $acApiResult->candidateRecordings as $trackReleaseCandidate){
            // echo('<BR><BR>-----------------------------------------------------------------<br>');
            // echo('<BR><BR>-----------------------------------------------------------------<br>');
            // echo('<BR><BR>candidate -----------------------------------------------------------------<br>');
            // //print_r ($trackReleaseCandidate);
            // echo('<BR><BR>-----------------------------------------------------------------<br>');
            // echo('<BR><BR> propname('.$propName.')-----------------------------------------------------------------<br>');
            // print_r ($trackReleaseCandidate->$propName);
            // echo('<BR><BR>-----------------------------------------------------------------<br>');
            // print_r ( ($trackReleaseCandidate->$propName == '') );
            // echo('<BR><BR>-----------------------------------------------------------------<br>');
            // echo('<BR><BR>-----------------------------------------------------------------<br>');
            if( 
              $trackReleaseCandidate && 
              property_exists($trackReleaseCandidate, $propName) && 
              $trackReleaseCandidate->$propName != '' && 
              property_exists($trackReleaseCandidate->$propName, 'results') && 
              count($trackReleaseCandidate->$propName->results) 
            ){
              if(property_exists($trackReleaseCandidate->$propName->results[0], 'recordings')){
                //echo '<pre>'; var_dump( $trackReleaseCandidate->$propName->results[0]->recordings ); echo '</pre><br/>';
                foreach($trackReleaseCandidate->$propName->results[0]->recordings as $recordingKey=>$recording){
                  if(property_exists($recording, 'releases')){
                    // echo '<pre>';var_dump($recording->releases);echo '<pre>';
                    foreach( $recording->releases as $releaseKey=>$releaseObject ){
                      //echo '<pre>';var_dump($releaseObject->id);echo '</pre><br/>';
                      if(!in_array($releaseObject->id, $trackMatch)){
                        $releaseCandidates[] = $releaseObject->id;
                        $trackMatch[] = $releaseObject->id;
                        $releaseObjectsStore[$releaseObject->id] = $releaseObject;
                      }
                    }
                  }
                }
              }
            }
					}
				}
			}
			$releaseCount = array_count_values($releaseCandidates);
			arsort($releaseCount); reset($releaseCount);
			$rMbid = array_key_first($releaseCount);
			/**
			 * 	There is now a series of candidates found. If the first candidate (after sorting by occurence)
			 *  is far below the amount fo tracks on the album, reject the candidates.
			 *  But only when certain the local data is not an amalgamated album of two
			 */
			$selectedRelease = false;
			$i = 0;
			while(!$selectedRelease && $tracks[$i]){
				$acoustId = $tracks[$i]->getAcoustId();

				if($acoustId){
					$acoustIdApiResult = json_decode($acoustId->getApiResult());
					if( 
						count($acoustIdApiResult->candidateRecordings) > 0 &&
						count($acoustIdApiResult->candidateRecordings[0]->$propName->results) > 0 &&
						property_exists($acoustIdApiResult->candidateRecordings[0]->$propName->results[0], 'recordings')
					){
						foreach($acoustIdApiResult->candidateRecordings[0]->$propName->results[0]->recordings as $record){
							if(property_exists($record, 'releases')){
								foreach( $record->releases as $release ){
									if($release->id === $rMbid){
										$selectedRelease = $release;
									}
								}
							}
						}
					}
				}
				$i++;
			}

			$totalLibraryTracks = count($tracks);

			$totalMatchingLibraryTracks = $releaseCount[$rMbid];

			$totalMbidTracks = $releaseObjectsStore[$rMbid]->track_count;

			echo " LibraryAlbumTotal: ".$totalLibraryTracks."<br/>";
			echo " MatchingLibraryAlbumTotal: ".$releaseCount[$rMbid]."<br/>";
			echo " MbidAlbumTotal: ".$totalMbidTracks."<br/>";

			if ( $totalMbidTracks == $totalMatchingLibraryTracks && $totalMatchingLibraryTracks == $totalLibraryTracks){
				$found = ' FOUND: 100% ';
			} elseif ( $totalMbidTracks > .66 * $totalMatchingLibraryTracks && $totalMbidTracks < 1.33 * $totalMatchingLibraryTracks){
				$found = ' FOUND: RECOGNISED MORE THEN HALF THE TRACKS!! ';
			} elseif (  $totalMbidTracks < .66 * $totalMatchingLibraryTracks ){
				$found = ' NOT FOUND: Too many tracks on HD. Duplicate tracks present?';
			} elseif (  $totalMbidTracks > 1.33 * $totalMatchingLibraryTracks ){
				$found = ' NOT FOUND: Too little MbIds identified. Is the album too incomplete? ';
			} else {
				$found = ' ERROR: Something is confusing, this statement shouldnt be reached. ';
				 // test if super group that needs to be split by testing if the filtering the current trackset, does match the proposed album candidate.
			}

			echo $rMbid.' ('.$releaseCount[$rMbid].');'.$found.' PATH HERE<br/><pre>';


			
			foreach($tracks as $trackindex => $albumTrack){
				$acoustId = $albumTrack->getAcoustId();
				if($acoustId){
					// check for matching release MbId

					$acApiResult = json_decode( $acoustId->getApiResult() );
	
					//var_dump($acApiResult->candidateRecordings);echo '<br/><br/>';
					
					// $.candidateRecordings[0]."recordings+releases".results[0].id
					$hasMatchingReleaseMbid = false;
					foreach( $acApiResult->candidateRecordings as $trackReleaseCandidate){
            if( 
              $trackReleaseCandidate && 
              property_exists($trackReleaseCandidate, $propName) && 
              $trackReleaseCandidate->$propName != '' && 
              property_exists($trackReleaseCandidate->$propName, 'results') && 
              count($trackReleaseCandidate->$propName->results) 
            ){
              if(property_exists($trackReleaseCandidate->$propName->results[0], 'recordings')){
                //echo '<pre>'; var_dump( $trackReleaseCandidate->$propName->results[0]->recordings ); echo '</pre><br/>';
                foreach($trackReleaseCandidate->$propName->results[0]->recordings as $recordingKey=>$recording){
                  if(property_exists($recording, 'releases')){
                    // echo '<pre>';var_dump($recording->releases);echo '<pre>';
                    foreach( $recording->releases as $releaseKey=>$releaseObject ){
                      //echo '<pre>';var_dump($releaseObject->id);echo '</pre><br/>';
                      if($releaseObject->id == $rMbid){
                        $hasMatchingReleaseMbid =  true;
                      }
                    }
                  }
                }
              }
            }
					}
					$trackStr = sprintf('%04d', ($trackindex+1)).'. ';
					if($hasMatchingReleaseMbid){
						$trackStr .= '( '.$rMbid.' ) '. ($albumTrack->getArtist() ? $albumTrack->getArtist()->getName().' - ' : '').$albumTrack->getTitle();
					}else{
						$trackStr .= '(            DEVIATING RMBID           ) '. ($albumTrack->getArtist() ? $albumTrack->getArtist()->getName().' - ' : '').$albumTrack->getTitle();
					}
				}else{
					$trackStr .= '(     NO MBID WAS SET, BROKEN TRACK?   ) '.$albumTrack->getArtist()->getName().' - '.$albumTrack->getTitle();
				}
				echo str_pad($trackStr, 90).' <br/>                                               '.$albumTrack->getUrl().' <br/>';
			}

			var_dump($selectedRelease);
			echo '</pre><br/>';
		}

		die('done');
	}

	private function commitAcoustidMetaData($acoustId, $recording, $apiResult=null){

		$acoustId->setArtistMbid($recording->artists[0]->id);
		$acoustId->setArtistName( substr($recording->artists[0]->name, 0, 255) );

		$acoustId->setReleaseMbid($recording->releases[0]->id);
		$acoustId->setReleaseName( substr($recording->releases[0]->title, 0, 255) );

		$acoustId->setAcoustid($recording->acoustid);
		$acoustId->setRecordingMbid($recording->id);
		$acoustId->setRecordingTitle( substr( $recording->title, 0, 255) );
		
		$acoustId->setYear($recording->releases[0]->releaseevents[0]->date->year);
		$acoustId->setScore($recording->score);
		$acoustId->setApiResult(json_encode($apiResult));
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($acoustId);
		$em->flush();
		
	}

	private function commitApiResult($acoustId, $apiResult){

		$acoustId->setApiResult(json_encode($apiResult));
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($acoustId);
		$em->flush();
		
	}

	/**
	 *  Store found metadata from AcoustId to Tag table for misisng or blatantly incorect
	 * 	properties.
	 * 	The function takes the content from the AcoustId data set and loops through them and
	 * 	determines what metadata of a track would be improved, and if applicable, updates them.
	 */

	function commitTracksMetaDataAction($score=80){

		$batchSize = 25;

		$em = $this->getDoctrine()->getManager();
		// $tracks = $em
		// 	->getRepository('AppSupplyWarakinBundle:Tags')
		// 	->findOneById( $id );

		if(array_key_exists('offset', $_GET)){
			$offset = intval($_GET['offset']);
		}else{
			$offset = 0;
		}

		
		$q = $em->getRepository("AppSupplyWarakinBundle:AcoustId")->createQueryBuilder('a')
				->where('
					a.acoustid is not null 
				')
				->andWhere('a.score >= :score')
				->setParameter(':score', $score)
				->setMaxResults($batchSize)
				->setFirstResult($offset)
				->getQuery();


		// $builder->andWhere('type IN (:string)');
		// $builder->setParameter('string', array('first','second'), \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
		
		//$q->setMaxResults(24000);
		$tracks = $q->getResult();
		
		if (!$tracks){
			die('broken sql');
		}else{
			$results = [];
			foreach($tracks as $key=>$track){   
				echo $track->getTag()->getTitle()." <br/>\n";
				echo "------------------------------------------------------------------------------------- <br/>\n";
				$result = $this->commitTrackMetaData($track);
				echo "------------------------------------------------------------------------------------- \n<br/>\n<br/>\n";
				if($result){
					//compare stuff 
					$results[$track->getId()] = $result;
				}
			}
			//$myfile = file_put_contents('AcoustIdScan.json', json_encode($results).PHP_EOL , FILE_APPEND | LOCK_EX);
		}

		//sleep(5);
        echo "<script>window['location'].href='?offset=".($offset+$batchSize)."'</script>";
		die();
		$response = new Response();
		$response->headers->set('Content-Type', 'application/json');
		$response->setContent(array('blegh'));

		return $response;

  }
	
	private function commitTrackMetaData($acoustId){


		$em = $this->getDoctrine()->getManager();

		$qb = $em->createQueryBuilder();

		$track = $acoustId->getTag();

		// check lastFm MBid

		// check artist
		$artist = $track->getArtist();
		$oldArtistName = '';
		if($artist){
			echo 'Current Artist:'.$artist->getName()."<br/>\n";
			$oldArtistName = $artist->getName();
		}
		if($acoustId->getArtistName()){
			if($oldArtistName != $acoustId->getArtistName()){
				echo 'New Artist:'.$acoustId->getArtistName()."<br/>\n";

				$newArtist = $em
					->getRepository('AppSupplyWarakinBundle:Artist')
					->findOneByName( $acoustId->getArtistName() );

				if(!$newArtist){
					$newArtist = new Artist();
					$newArtist->setName($acoustId->getArtistName());

					$em->persist($newArtist);
				}
				
				$track->setArtist($newArtist);
			}
		}

		// check album
		$album = $track->getAlbum();
		$oldAlbumTitle = '';
		if($album){
			echo 'Old Album:'.$album->getName()."<br/>\n";
			$oldAlbumTitle = $album->getName();
		}
		if($acoustId->getReleaseName()){
			if($oldAlbumTitle != $acoustId->getReleaseName()){
				echo 'New Album:'.$acoustId->getReleaseName()."<br/>\n";

				$newAlbum = $em
					->getRepository('AppSupplyWarakinBundle:Album')
					->findOneByName( $acoustId->getReleaseName() );

				if(!$newAlbum){
					$newAlbum = new Album();
					$newAlbum->setName($acoustId->getReleaseName());

					$em->persist($newAlbum);
					// $em->flush();
				}
				
				$track->setAlbum($newAlbum);
			}
		}

		// check track title
		$oldTrackTitle = $track->getTitle();
		echo 'Old Title:'.$oldTrackTitle."<br/>\n";
		if($acoustId->getRecordingTitle()){
			if($oldTrackTitle != $acoustId->getRecordingTitle()){
				echo 'New Title:'.$acoustId->getRecordingTitle()."<br/>\n";
				$track->setTitle($acoustId->getRecordingTitle());
			}
		}

		// check track year
		$year = $track->getYear();
		$oldYearName = '';
		if($year){
			echo 'Old Album:'.$year->getName()."<br/>\n";
			$oldYearName = $year->getName();
		}
		if($acoustId->getYear()){
			if($oldYearName != $acoustId->getYear()){
				echo 'New Year:'.$acoustId->getYear()."<br/>\n";

				$newYear = $em
					->getRepository('AppSupplyWarakinBundle:Year')
					->findOneByName( $acoustId->getYear() );

				if(!$newYear){
					$newYear = new Year();
					$newYear->setName($acoustId->getYear());

					$em->persist($newYear);
					// $em->flush();
				}
				
				$track->setYear($newYear);
			}
		}

		$em->persist($track);
		$em->flush();

		return false;
	}

}


