<?php

namespace AppSupply\WarakinBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/*
	AcoustId controller contains functions to interact with the AcoudstID web api
*/

class WarakinController extends Controller
{

	protected function getJsonResponse($data){

		$serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($data, 'json');
        
		$response = new Response();
		$response->headers->set('Content-Type', 'application/json; charset=UTF-8');

		$response->setContent($json);

		return $response;
	}

}


