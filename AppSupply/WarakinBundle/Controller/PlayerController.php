<?php

namespace AppSupply\WarakinBundle\Controller;

use AppSupply\WarakinBundle\Controller\WarakinController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;

use AppSupply\WarakinBundle\Entity\Album;
use AppSupply\WarakinBundle\Entity\Tags;
use AppSupply\WarakinBundle\Entity\lastFmTag;

class PlayerController extends WarakinController
{
    public function homeAction()
    {
		$dataService = $this->get('warakin.service.data');
		
		$genreGroups = $this->get('doctrine')->getManager()
			->getRepository('AppSupplyWarakinBundle:GenreGroup')
			->findAll();

		//var_dump($popTags[0]->getBpm());
        return $this->render(
        	'AppSupplyWarakinBundle:Player:home.html.twig',
  			array(
				'home'=>array(
					"artists" => $this->retrieveHomeArtistTops(),
					"albums" => $this->retrieveHomeAlbumTops(),
					"tracks" => $this->retrieveHomeTrackTops(),
					"crates" => $this->retrieveUnderConstructionCrates(),
					"genregroups" => $genreGroups
				),
				'value'=>new Tags()
			)
        );
	}
	
	private function retrieveCrates()
    {
		// $dataService = $this->get('warakin.service.data');
		
		// $genreGroups = $this->get('doctrine')->getManager()
		// 	->getRepository('AppSupplyWarakinBundle:GenreGroup')
		// 	->findAll();

		return $this->get('doctrine')->getManager()
			->getRepository('AppSupplyWarakinBundle:Playlists')
			->findByType('Crate');
		//var_dump($playlistId);
	}
	
	private function retrieveUnderConstructionCrates()
    {
		$dataService = $this->get('warakin.service.data');
		
		$genreGroups = $this->get('doctrine')->getManager()
			->getRepository('AppSupplyWarakinBundle:GenreGroup')
			->findAll();

		return $this->get('doctrine')->getManager()->createQueryBuilder()
			->select(
				'p, p.id as id, p.name as name, count(DISTINCT t.id) taggedTrackCount'
			)
			->from('AppSupplyWarakinBundle:Playlists', 'p')
			->leftJoin('p.playlistTracks', 'pt')
			->leftJoin('pt.track', 't')
			->innerJoin('t.seratoFeatures', 'sf')
			->innerJoin('sf.seratoCuepoints', 'sc')
			// ->where('p.type = :crate')
			// ->setParameter("crate", "Crate")
			// ->andWhere('pe.id is null')
			// ->andWhere('t.bitrate > 127')
   			->orderBy('taggedTrackCount', 'desc')
			->groupBy('p.id')
			->setMaxResults(12)
		   	->getQuery()
		   	->getResult();
		//var_dump($playlistId);
	}
	
	public function loadPlaylistAction($playlistId)
    {
		$dataService = $this->get('warakin.service.data');
		
		$genreGroups = $this->get('doctrine')->getManager()
			->getRepository('AppSupplyWarakinBundle:GenreGroup')
			->findAll();

		$playlist = $this->get('doctrine')->getManager()
			->getRepository('AppSupplyWarakinBundle:Playlists')
			->findOneById($playlistId);
		//var_dump($playlistId);
		//var_dump($playlist);
		
        return $this->render(
        	'AppSupplyWarakinBundle:Player:home.html.twig',
  			array(
				'home'=>array(
					"artists" => $this->retrieveHomeArtistTops(),
					"albums" => $this->retrieveHomeAlbumTops(),
					"tracks" => $this->retrieveHomeTrackTops(),
					"genregroups" => $genreGroups,
					"playlist" => $playlist
				),
				'value'=>new Tags()
			)
        );
    }

	private function retrieveHomeTrackTops(){

		/* TAGS SELECTIONS */
 
        $newTags = $this->get('doctrine')->getManager()->createQueryBuilder()
			->select(
				't'
			)
			->from('AppSupplyWarakinBundle:Tags', 't')
			->leftJoin('t.statistics', 's')
			->leftJoin('t.playEvents', 'pe')
			->where('s.id IS NULL')
			->andWhere('pe.id is null')
			->andWhere('t.bitrate > 127')
   			->orderBy('t.createdate', 'desc')
            ->setMaxResults(42)
		   	->getQuery()
		   	->getResult();
 
        $popTags = $this->get('doctrine')->getManager()->createQueryBuilder()
				->select(
					't'
				)
					->from('AppSupplyWarakinBundle:Tags', 't')
					->leftJoin('t.statistics','s')
				->where('s.rating > 0')
				->andWhere('s.rating IS NOT NULL')
				->orderBy('s.rating', 'desc')
				->orderBy('s.playcounter/3', 'desc')
				->setMaxResults(42)
				->getQuery()
				->getResult();
 
		$seratoTags = $this->get('doctrine')->getManager()->createQueryBuilder()
				->select(array(
					't'
					))
					->from('AppSupplyWarakinBundle:Tags', 't')
					->leftJoin('t.seratoFeatures','sf')
					->leftJoin('sf.seratoCuepoints','sc')
				->groupBy('t')
				->having('count(sc) > 0')
				->orderBy('sf.updated', 'desc')
				->setMaxResults(12)
				->getQuery()
				->getResult();
 
		$newFavoriteTags = $this->get('doctrine')->getManager()->createQueryBuilder()
				->select(array(
					't'
					))
					->from('AppSupplyWarakinBundle:Tags', 't')
					->leftJoin('t.statistics','s')
				->where('s.rating > 7')
				->groupBy('t')
				->orderBy('s.updated', 'desc')
				->setMaxResults(42)
				->getQuery()
				->getResult();

		return array(
			"New" => $newTags,
			"Pop" => $popTags,
			"Random" => array(),
			"Serato" => $seratoTags,
			"newFavorites" => $newFavoriteTags
		);
	}

	private function retrieveHomeArtistTops(){

		/* ARTISTS SELECTIONS */
 
        $newArtists = $this->get('doctrine')->getManager()->createQueryBuilder()
            ->select(
                array(
                    'a.id as id',
                    'a.name as name',
					'MIN(t.createdate) as mincreate',
                )
            )
			->from('AppSupplyWarakinBundle:Artist', 'a')
			->leftJoin('a.tags','t')
            ->where('t.deleted = 0')
	        ->groupBy('a.id')
   			->orderBy('mincreate', 'desc')
            ->setMaxResults(10)
		   	->getQuery()
		   	->getResult();
 
        $popArtists = $this->get('doctrine')->getManager()->createQueryBuilder()
            ->select(
                array(
                    'a.id as id',
                    'a.name as name',
					'COUNT(t.id) as rated_tracks',
					'SUM(s.rating)/COUNT(t.id) as total_rating',
                )
            )
			->from('AppSupplyWarakinBundle:Artist', 'a')
	        ->leftJoin('a.tags','t')
	        ->leftJoin('t.statistics','s')
			->where('s.rating > 0')
			->andWhere('s.rating IS NOT NULL')
			->andWhere('t.deleted = 0')
		   	->having('COUNT(t.id) > 10')
            ->groupBy('a.id')
   			->orderBy('total_rating', 'desc')
            ->setMaxResults(10)
		   	->getQuery()
		   	->getResult();

		//var_dump($popArtists);

		return array(
			"New" => $newArtists,
			"Pop" => $popArtists,
			"Random" => array(),
		);
	}

	private function retrieveHomeAlbumTops(){
		/* ALBUMS SELECTIONS */
 
        $newAlbums = $this->get('doctrine')->getManager()->createQueryBuilder()
            ->select(
                array(
                    'a.id as id',
                    'a.name as name',
					'MIN(t.createdate) as mincreate',
                )
            )
			->from('AppSupplyWarakinBundle:Album', 'a')
	        ->leftJoin('a.tags','t')
			->leftJoin('t.playEvents', 'pe')
			->where('t.deleted = 0')
			->andWhere('pe.id IS NULL')
			->andWhere('t.bitrate > 127')
            ->groupBy('a.id')
   			->orderBy('mincreate', 'desc')
            ->setMaxResults(10)
		   	->getQuery()
		   	->getResult();
		
		foreach($newAlbums as $key=>$value){
			$album = new Album($value['id']);
			$newAlbums[$key]['artist']= $album->getArtist();
		}
 
        $popAlbums = $this->get('doctrine')->getManager()->createQueryBuilder()
            ->select(
                array(
                    'a.id as id',
                    'a.name as name',
					'COUNT(t.id) as rated_tracks',
					'SUM(s.rating)/COUNT(t.id) as total_rating',
                )
            )
			->from('AppSupplyWarakinBundle:Album', 'a')
	        ->leftJoin('a.tags','t')
	        ->leftJoin('t.statistics','s')
			->where('s.rating > 0')
			->andWhere('s.rating IS NOT NULL')
			->andWhere('t.deleted = 0')
		   	->having('COUNT(t.id) > 2')
            ->groupBy('a.id')
   			->orderBy('total_rating', 'desc')
            ->setMaxResults(10)
		   	->getQuery()
		   	->getResult();

		foreach($popAlbums as $key=>$value){
			$album = new Album($value['id']);
			$popAlbums[$key]['artist']= $album->getArtist();
		}

		return array(
			"New" => $newAlbums,
			"Pop" => $popAlbums,
			"Random" => array(),
		);
	}

    public function trackAction($track_id)
    {
		$dataService = $this->get('warakin.service.data');
		$track = $dataService->getTrack($track_id);

		$seratoCuepoints = [];
		if($track->getSeratoFeatures()){
			$seratoCuepoints = $track->getSeratoFeatures()->getSeratoCuepoints();
		}

		$relatedLastFmArtists = [];
		$relatedSpotifyArtists= [];
		if($track->getArtist()){
			$relatedLastFmArtists = $track->getArtist()->getRelatedLastFmArtists();
			$relatedSpotifyArtists = $track->getArtist()->getRelatedSpotifyArtists();
		}
		
		return $this->render(
        	'AppSupplyWarakinBundle:Player:track.html.twig',
			  array('track_id'=>$track_id,
				 'track'=>$track,
				 'track_cuepoints'=>$seratoCuepoints,
				 //'relatedLastFmTracks'=>$track->getRelatedLastFmTracks(),
				 'relatedLastFmArtists'=>$relatedLastFmArtists,
				 'relatedSpotifyArtists'=>$relatedSpotifyArtists
			  )
		);
	}
	
	public function tracksLatestAction(){
		//var_dump($popTags[0]->getBpm());
		$newTags = $this->get('doctrine')->getManager()->createQueryBuilder()
		->select(
			't'
		)
		->from('AppSupplyWarakinBundle:Tags', 't')
		->leftJoin('t.statistics', 's')
		->where('s.id IS NULL')
		   ->orderBy('t.createdate', 'desc')
		->setMaxResults(1800)
		   ->getQuery()
		   ->getResult();

        return $this->render(
        	'AppSupplyWarakinBundle:Player:tracks.latest.html.twig',
  			array(
				"home"=>array(
					"tracks" => array(
						"New" => $newTags
					)
				)
			)
        );
	}

	public function tracksLatestFavoritesAction(){
		//var_dump($popTags[0]->getBpm());
		$newTags = $this->get('doctrine')->getManager()->createQueryBuilder()
		->select(array(
			't'
			))
			->from('AppSupplyWarakinBundle:Tags', 't')
			->leftJoin('t.statistics','s')
		->where('s.rating > 7')
		->groupBy('t')
		->orderBy('s.created', 'desc')
		->setMaxResults(300)
		->getQuery()
		->getResult();

        return $this->render(
        	'AppSupplyWarakinBundle:Player:tracks.latest.html.twig',
  			array(
				"home"=>array(
					"tracks" => array(
						"New" => $newTags
					)
				)
			)
        );
	}
	
	public function tracksUglyAction(){
		//var_dump($popTags[0]->getBpm());
		$newTags = $this->get('doctrine')->getManager()->createQueryBuilder()
		->select(
			't'
		)
		->from('AppSupplyWarakinBundle:Tags', 't')
			->leftJoin('t.artist', 'a')
			->leftJoin('t.album', 'al')
		->where('a.id IS NULL')
		->andWhere('al.id IS NULL')
		->andWhere('t.lastFmMbid IS NULL')
		->orderBy('t.createdate', 'desc')
		->setMaxResults(300)
		   ->getQuery()
		   ->getResult();

        return $this->render(
        	'AppSupplyWarakinBundle:Player:tracks.latest.html.twig',
  			array(
				"home"=>array(
					"tracks" => array(
						"New" => $newTags
					)
				)
			)
        );
	}
	
	public function tracksSeratoAction(){
		//var_dump($popTags[0]->getBpm());
		$tracks = $this->get('doctrine')->getManager()->createQueryBuilder()
				->select(array(
					't'
					))
				->from('AppSupplyWarakinBundle:Tags', 't')
				->leftJoin('t.seratoFeatures','sf')
				->leftJoin('sf.seratoCuepoints','sc')
				->groupBy('t')
				->having('count(sc) > 3')
				->orderBy('sf.updated', 'desc')
				->setMaxResults(300)
				->getQuery()
				->getResult();

        return $this->render(
        	'AppSupplyWarakinBundle:Player:tracks.latest.html.twig',
  			array(
				"home"=>array(
					"tracks" => array(
						"New" => $tracks
					)
				)
			)
        );
	}
	
	public function tracksSeratoEasyWinsAction(){
		//var_dump($popTags[0]->getBpm());
		$tracks = $this->get('doctrine')->getManager()->createQueryBuilder()
				->select(array(
					't'
					))
				->from('AppSupplyWarakinBundle:Tags', 't')
				->leftJoin('t.seratoFeatures','sf')
				->leftJoin('sf.seratoCuepoints','sc')
				->leftJoin('t.statistics','s')
				->groupBy('t')
				->having('count(sc) <= 3')
				->andHaving('count(sc) > 0')
				->orderBy('s.rating', 'desc')
				
				->setMaxResults(300)
				->getQuery()
				->getResult();

        return $this->render(
        	'AppSupplyWarakinBundle:Player:tracks.latest.html.twig',
  			array(
				"home"=>array(
					"tracks" => array(
						"New" => $tracks
					)
				)
			)
        );
	}
	
	public function tracksHistoryAction(){
		//var_dump($popTags[0]->getBpm());
		$playevents = $this->get('doctrine')->getManager()->createQueryBuilder()
				->select('pe, MAX(pe.created) as max_created')
				->from('AppSupplyWarakinBundle:PlayEvent', 'pe')
				->leftJoin('pe.track', 't')
				->groupBy('pe.track')
				->setMaxResults(300)
				->orderBy('max_created', 'DESC')
				->getQuery()
				->getResult();
		$tracks = $playevents;
		$tracks = [];
		foreach($playevents as $key=>$event){

			//var_dump($event																			);
			$tracks[] = $event[0]->getTrack();

		}

		// $qb->select(array('DISTINCT i.id', 'i.name', 'o.name'))
		// 		->from('Item', 'i')
		// 		->join('i.order', 'o')
		// 		->where(
		// 			$qb->expr()->in(
		// 				'o.id',
		// 				$qb2->select('o2.id')
		// 					->from('Order', 'o2')
		// 					->join('Item', 
		// 						   'i2', 
		// 						   \Doctrine\ORM\Query\Expr\Join::WITH, 
		// 						   $qb2->expr()->andX(
		// 							   $qb2->expr()->eq('i2.order', 'o2'),
		// 							   $qb2->expr()->eq('i2.id', '?1')
		// 						   )
		// 					)
		// 					->getDQL()
		// 			)
		// 		)
		// 		->andWhere($qb->expr()->neq('i.id', '?2'))
		// 		->orderBy('o.orderdate', 'DESC')
		// 		->setParameter(1, 5)
		// 		->setParameter(2, 5)
		// 		;
				

        return $this->render(
        	'AppSupplyWarakinBundle:Player:tracks.latest.html.twig',
  			array(
				"home"=>array(
					"tracks" => array(
						"New" => $tracks
					)
				)
			)
        );
	}


	
	public function tagAction($tag_id){
		//var_dump($popTags[0]->getBpm());


		$lastFmTag = $this->get('doctrine')->getManager()->createQueryBuilder()
			->select(array(
				'lft'
				))
			->from('AppSupplyWarakinBundle:LastFmTag', 'lft')
			->where('lft.id = :tagId')
			->setParameter('tagId', $tag_id)
			->getQuery()
			->getResult();

		$tracks = $this->get('doctrine')->getManager()->createQueryBuilder()
			->select(array(
				't'
				))
			->from('AppSupplyWarakinBundle:Tags', 't')
			->leftJoin('t.lastFmTags','lft')
			->leftJoin('t.statistics','s')
			->where('lft.id = :tagId')
			->groupBy('t')
			->orderBy('s.rating', 'desc')
			->setParameter('tagId', $tag_id)
			->getQuery()
			->getResult();

        return $this->render(
        	'AppSupplyWarakinBundle:Player:tracks.latest.html.twig',
  			array(
				"home"=>array(
					"tracks" => array(
						"New" => $tracks,
						"Title" => $lastFmTag[0]->getName()
					)
				)
			)
        );
	}


	
	public function tagsAction(){
		//var_dump($popTags[0]->getBpm());
		$tracks = $this->get('doctrine')->getManager()->createQueryBuilder()
				->select('lft, count(t.id) AS mycount')
				->from('AppSupplyWarakinBundle:LastFmTag', 'lft')
				->leftJoin('lft.tracks','t')
				->where('lft.active = 1')
				->groupBy('lft')
				->orderBy('mycount', 'desc')
				->setMaxResults(300)
				->getQuery()
				->getResult();

        return $this->render(
        	'AppSupplyWarakinBundle:Player:tags.html.twig',
  			array(
				"home"=>array(
					"tracks" => array(
						"New" => $tracks
					)
				)
			)
        );
	}
}
