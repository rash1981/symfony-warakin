<?php

namespace AppSupply\WarakinBundle\Controller;

use AppSupply\WarakinBundle\Controller\WarakinController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Finder\Finder;
use Doctrine\ORM\EntityManager;
use \GetId3\GetId3Core as GetId3;

/*
	Utility controller contains a number of functions

	Library Scanner

	The 
*/

class UtilityController extends WarakinController
{

	var $dirs = array(
		/*
		 * test set

		'/media/MP3.1/A/aux raus - aux raus (2006)',
		'/media/MP3.NEW/Urfaust/',
		'/media/MP3.NEW/Vive La Fête',
		*/
		
		//'/media/MP3.NEW/00-INCOMING',
		
		//'/media/MP3.NEW',

		'/media/MP3.1/',

		// '/media/MP3.1/#',	
		
		//'/media/MP3.1/A',
		
		//'/media/MP3.1/B',
		
		//'/media/MP3.2',

		//'/media/MP3.2/C',
		//'/media/MP3.2/C/Charlie Parker - Birth of the Bebop',
		
		// '/media/MP3.2/D',
		
		// '/media/MP3.3',

		//'/media/MP3.3/E',
		
		//'/media/MP3.3/F',
		//'/media/MP3.3/G',
		
		//'/media/MP3.3/H',
		//'/media/MP3.3/I',

		//'/media/MP3.3/J',
		//'/media/MP3.3/K',
		
		//'/media/MP3.4',
		//'/media/MP3.4/L',
		
		//'/media/MP3.4/M',

		//'/media/MP3.4/N',
		
		//'/media/MP3.4/O',
		
		// '/media/MP3.5',
		
		//'/media/MP3.5/P',
		
		// '/media/MP3.5/Q',
		// '/media/MP3.5/R',
		
		// '/media/MP3.5/S',
		
		// '/media/MP3.6/',

		// '/media/MP3.6/T',
		
		// '/media/MP3.6/U',
		// '/media/MP3.6/V',
		
		// '/media/MP3.6/W',
		// '/media/MP3.6/X',
		// '/media/MP3.6/Y',
		// '/media/MP3.6/Z',


		// '/media/MP3.NEW',

		// '/media/MP3.VA',

		// '/media/MP3.VA/mp3.tracks',
		// '/media/MP3.VA/Collections',
		// '/media/MP3.VA/Odd Future Wolf Gang Kill Them All',
		
		// '/media/MP3.VA/LARA',
		// '/media/MP3.VA/OST',
		//'/media/MP3.VA/VA',

		// '/media/junk/Wget-Opendirs/pc-dj.com/downloads/Pcdj Resources',
		// '/media/junk/Wget-Opendirs/home.innet.pro',
		// '/media/junk/Wget-Opendirs/pixieradio.com',
		// '/media/junk/Wget-Opendirs/dancehall.fr'
		
	);


	public function testTracksAction(){
		return $this->getJsonResponse(
			$this->checkTracksOnHD()
		);
	}


	public function testTracksTagsAction(){
		return $this->getJsonResponse(
			$this->testTracksTags()
		);
	}


	public function findTracksAction(){
		header('Content-Type: text/html; charset=UTF-8');
		return $this->getJsonResponse(
			$this->findTracksUrls(
				$this->checkTracksOnHD()
			)
		);
	}


	public function fixTracksAction(){
		return $this->getJsonResponse(
			$this->fixTracksUrlsAction(
				$this->findTracksUrls(
					$this->checkTracksOnHD()
				)
			)
		);
	}

	public function synchSeratoFeaturesAction(){
		set_time_limit(8000);

		$synchService = $this->get('warakin.service.synch');

		return $this->getJsonResponse(
			$synchService->synchSeratoFeatures()
		);
	}


	public function synchSocialMediaRelatedArtistsAction(){

		set_time_limit(8000);

		$synchService = $this->get('warakin.service.synch');

		return $this->getJsonResponse(
			$synchService->synchSocialMediaRelatedArtists()
		);
	}

	public function analyzeTagsMd5Action(){

		set_time_limit(8000);

		$synchService = $this->get('warakin.service.synch');

		return $this->getJsonResponse(
			$synchService->analyzeTagsMd5()
		);
	}


	public function synchSocialMediaRelatedTracksAction(){

		set_time_limit(8000);

		$synchService = $this->get('warakin.service.synch');

		return $this->getJsonResponse(
			$synchService->synchSocialMediaRelatedTracks()
		);
	}

	
	/*
		Test to see if tracks in database still exist on the harddrive
	*/

	public function checkTracksOnHD()
	{
		//set_time_limit(0);
		// setup data processing object requisites
		$dataService = $this->get('warakin.service.data');
		$em = $this->getDoctrine()->getManager();
		
		// set operational variables
		$response = array(
			'notFound'=>array(),
			'found'=>array()
		);
		//$batchSize = 100;
		$batchSize = 100;
		$i = 0;

		// start operation
		$q = $em->createQuery('SELECT t.url FROM AppSupplyWarakinBundle:Tags t');
		//$q->setMaxResults(24000);
		$urls = $q->getResult();

		$lostCount = 0;

		foreach($urls AS $row) {
			$responseLine = '';
			//var_dump($row);
		    if (!file_exists('/'.$row['url'])){
				++$lostCount;
				$response['notFound'][] = $row['url'];
		    }else{
		    	$response['wasFound'][] = $row['url'];
		    }
		    if (($i % $batchSize) == 0) {
		        //$em->flush(); // Executes all updates.
		   		$em->clear(); // Detaches all objects from Doctrine!
		    }
		    ++$i;
		}
		echo '## LossCount: '.$lostCount."<br/>\n";

		// ouput content
		//var_dump($response['notFound']);
		//return $this->getJsonResponse($newUrls);
        //$response = new Response();
		return $response['notFound'];
	}

	/*
		Test to see if tracks are lacking mp3 tags
	*/
	public function testTracksTags()
	{
		//set_time_limit(0);
		// setup data processing object requisites
		$dataService = $this->get('warakin.service.data');
		$em = $this->getDoctrine()->getManager();
		
		// set operational variables
		$response = array(
		);
		//$batchSize = 100;
		$batchSize = 100;
		$i = 0;

		// start operation
		$q = $em->createQuery('SELECT t.url FROM AppSupplyWarakinBundle:Tags t WHERE t.artist is null or t.title is null');
		//$q->setMaxResults(24000);
		$urls = $q->getResult();

		$lostCount = 0;

		foreach($urls AS $row) {
		    // $row['url'];
			$newTags = $this->getId3Tags($row['url']);
			if($newTags){
				if( array_key_exists('artist', $newTags) && array_key_exists('title', $newTags) ){
									    
				    $track = $em
				               ->getRepository('AppSupplyWarakinBundle:Tags')
				               ->findOneByUrl($row['url']);

					$oldArtist = $track->getArtist();

					$newArtist = $em
				               ->getRepository('AppSupplyWarakinBundle:Artist')
				               ->findOneByName($newTags['artist']);
				               
				    if($newArtist){
				    	echo 'newArtist found: '.$newArtist->getName()."<br/>\n";
				    } else {
				    	echo 'artist "'.$newTags['artist'].'" not exist, time to create<br/>';
				    	$newArtist = new \AppSupply\WarakinBundle\Entity\Artist();
							$newArtist->setName($newTags['artist']);
							$em->persist($newArtist);
							$em->flush();

				    }

	   				if( $track->getTitle() != $newTags['title']  || $oldArtist != $newArtist ){

							var_dump($row);
	   					echo 'new artist:"'.$newArtist->getName().'"<br/>';
	   					echo 'new title:"'.$newTags['title'].'"<br/><br/>';
	   					$track->setArtist($newArtist);
	   					$track->setTitle($newTags['title']);
	   					$response[] = $row;
						//$em->persist($track);

	   				}

   				}
			}
		}
		echo '## updateCount: '.count($response)."<br/>\n";

		// ouput content
		//var_dump($response['notFound']);
		//return $this->getJsonResponse($newUrls);
        //$response = new Response();
		return $response;
	}

	public function findTracksUrls($urls)
	{
		$newUrls = array();
		$itemsFound = 0;
		$itemsNotFound = 0;
		foreach ($urls as $key => $url) {
			
			if(count($newUrls) == 400 ){ break;}
			// echo '--------------------------------------------------------------------------------<br/>';
			// echo 'SEARCHING:'.$url."<br/>\n";
			# code...


			/**
			 *	fix UTF8 incompatibility
			 */
			 
			$urlParts = explode('/', $url);
			$searchUrl = '';
			foreach ($urlParts as $key => $urlPart) {
				if($key == count($urlParts)-1) {
					$searchFileUri = $urlPart;
					$searchFileRegex =  preg_replace("/[^A-Za-z0-9 .()#'=-]/", '*', $urlPart);
				} else{
					$searchUrl .= '/'.preg_replace("/[^A-Za-z0-9 .()#'-]/", '*', $urlPart);
				}
			}
			//$newUrl = preg_replace("/\*+/", '[.]+', $newUrl);
			
			$searchUrl = str_replace('//', '/', preg_replace("/\*+/", '*', $searchUrl));

			$searchFileUriStripped = preg_replace("/\*+/", '', $searchFileRegex);

			$searchFileRegex = preg_replace("/\*+/", 'REGPLACEHOLDER', $searchFileRegex);
			$searchFileRegex = preg_quote($searchFileRegex, '/');
			//$searchFileRegex = str_replace('\\', '\\\\', $searchFileRegex);
			$searchFileRegex = str_replace('REGPLACEHOLDER', '.+?', $searchFileRegex);
			
			// echo 'FILENAME REGEX:'.$searchFileRegex."<br/>\n";
			// echo 'FILENAME STRIPPED:'.$searchFileUriStripped."<br/>\n";
			// echo 'FILENAME SEARCH:'.$searchFileUri."<br/>\n";


			/**
			 *	find file
			 */

			$found = false;
			if(file_exists($searchUrl)){
				//echo $searchUrl."\n";
				$finder = new Finder();
				$nomatch = '';
				foreach($finder->in($searchUrl) as $file){
					//var_dump($file->getFilename());	
					$filename = $file->getFilename();
					if($filename == $searchFileUri){
						//echo 'FOUND FILENAME: '.$file->getPath().'/'.$filename.' == '.$url."<br/>\n";
						$newUrls[] = array(
							"new"=>substr($file->getPath().'/'.$filename, 1),
							"old"=>$url
						);
						$found = true;
					} else if($filename == $searchFileUriStripped){
						//echo 'FOUND STRIPPED: '.$file->getPath().'/'.$filename.' == '.$url."<br/>\n";
						$newUrls[] = array(
							"new"=>substr($file->getPath().'/'.$filename, 1),
							"old"=>$url
						);
						$found = true;
					} else if (preg_match('/'.$searchFileRegex.'/', $filename)) {
						//echo 'FOUND REGEX: '.$file->getPath().'/'.$filename.' == '.$file->getPath().'/'.$searchFileUri."<br/>\n";
						$newUrls[] = array(
							"new"=>substr($file->getPath().'/'.$filename, 1),
							"old"=>$url
						);
						$found = true;
					} else{
						$nomatch .= '## NO MATCH: '.$searchUrl.'/'.$filename."<br>\n";
					}
				}

			}else if(!strrpos($url, 'Seo Taiji') && !strrpos($url, 'Sig')){ //!strrpos($url, 'Vive La F') && 
				echo $url."<br/>/n";
				set_time_limit(3000);

				$tempUrls = array();
				$tempUrls[] = $this->iterateDirectoryToFindFile($url, $searchFileRegex, $searchFileUri, $urlParts, '/media/MP3.1');
				$tempUrls[] = $this->iterateDirectoryToFindFile($url, $searchFileRegex, $searchFileUri, $urlParts, '/media/MP3.2');
				$tempUrls[] = $this->iterateDirectoryToFindFile($url, $searchFileRegex, $searchFileUri, $urlParts, '/media/MP3.3');
				$tempUrls[] = $this->iterateDirectoryToFindFile($url, $searchFileRegex, $searchFileUri, $urlParts, '/media/MP3.4');
				$tempUrls[] = $this->iterateDirectoryToFindFile($url, $searchFileRegex, $searchFileUri, $urlParts, '/media/MP3.5');
				$tempUrls[] = $this->iterateDirectoryToFindFile($url, $searchFileRegex, $searchFileUri, $urlParts, '/media/MP3.6');
				$tempUrls[] = $this->iterateDirectoryToFindFile($url, $searchFileRegex, $searchFileUri, $urlParts, '/media/MP3.NEW');
				$tempUrls[] = $this->iterateDirectoryToFindFile($url, $searchFileRegex, $searchFileUri, $urlParts, '/media/MP3.VA');
				foreach($tempUrls as $index=>$url){
					if(is_array($url)){
						$newUrls[] = $url;
			    		$found = true;
			    	}
			    }
			}

			if($found != true){
				//echo $nomatch;
				//echo 'NOT FOUND: '.$searchUrl.'/'.$searchFileUri."<br/>\n";
				++$itemsNotFound;
			}
		}		

		echo '## Found '.count($newUrls).', '.$itemsNotFound.' not found.'."\n\n";
		return $newUrls;
	}


	private function iterateDirectoryToFindFile($url, $searchFileRegex, $searchFileUri, $urlParts, $dir){
		// directory with file in it does not exist
		// find the file using recursive directory iteration
		$em = $this->getDoctrine()->getManager();
		$timelimit = ini_get('max_execution_time');
		set_time_limit($timelimit + 300000000000);
		// Construct the iterator
		//$it = new \RecursiveDirectoryIterator("/media/MP3.3");
		$it = new \RecursiveDirectoryIterator($dir);
		$track = $em
			->getRepository('AppSupplyWarakinBundle:Tags')
			->findOneByurl( $url );
		// Loop through files
		foreach(new \RecursiveIteratorIterator($it) as $file) {
			$filename = $file->getFilename();
			$score = 0;  
		    if (preg_match('/'.$searchFileRegex.'/', $filename)) {
				$trackCheck = $em
								->getRepository('AppSupplyWarakinBundle:Tags')
								->findOneByurl( $file );
				/*$em
						->getRepository('AppSupplyWarakinBundle:Tags')
						->findOneByurl( preg_replace("/[^A-Za-z0-9 .()#'=-]/", '*', '.'.$file ) );*/
				if($trackCheck==false){
					// filename is the same
					if($searchFileUri == $filename){
						$score += 30;
					} 
					
					// filename has same length
					if(strlen($searchFileUri) == strlen($filename)){
						$score += 20;
					}
					
					// parent directory name matches stored parent directory
					if( $urlParts[count($urlParts)-2] == basename(substr($file,0, 0-strlen($filename)))){
						echo '## '.$urlParts[count($urlParts)-2].' == '.basename(substr($file,0, 0-strlen($filename)))."<br>\n";
						$score += 20;
					}



					$tags = $this->getId3Tags($file);
					//var_dump($tags);
					if($tags != false){
						// artist tag matches stored artist name
						$artist = $track->getArtist();
						if($artist){
							if($artist->getName() == $tags['artist']){
								$score += 10;
							}
						}
						// title tag matches stored title name
						if($track->getTitle() == $tags['title']){
							$score += 10;
						}

						// album tag matches stored album name
						$album = $track->getAlbum();
						if($album){
							if($album->getName() == $tags['album']){
								$score += 10;
							}
						}

						// filesize is the same for stored and found file
						echo '## 	'.filesize($file)."<br/>\n## 	".$track->getFilesize()."<br/>\n"; 
						if(filesize($file)>=$track->getFilesize()){
							$score += 35;
						}
						
					}
					
					

					// check if the foun file isnt already in the track database

					$testFoundTrack = $em
						->getRepository('AppSupplyWarakinBundle:Tags')
						->findOneByurl( preg_replace("/[^A-Za-z0-9 .()#'=-]/", '*', '.'.$file ) );


						//$searchFileRegex =  preg_replace("/[^A-Za-z0-9 .()#'=-]/", '*', $urlPart);

					/*if($testFoundTrack){	
						echo "##testFoundTrack<br/>\n";
						echo '##.'.$file."\n##".$testFoundTrack->getUrl()."\n";
						echo "##<br/>\n";
					}*/
					if($score > 79 && !$testFoundTrack ){
						echo '## Found file ( "'.$url.'" )in different dir: '.$file." <br/>\n";
						echo '## The system is sure about this match, score:'.$score."<br/>\n";
						return array(
							//"new"=>substr($file->getPath().'/'.$filename, 1),
							"new"=>'.'.$file,
							"old"=>$url,
							"score"=>$score
						);
						// return $newUrls;
					} else if($score > 85 && !$testFoundTrack){
						
					} else {

						echo "## 	The system is in doubt about this match<br/>\n";
						echo '## 	'.$url."<br/>\n";
						echo '## 	'.$file."<br/>\n";
						echo "## 	(score:".$score.")<br/>\n";
						//return false;
					}
				}else{
					echo '## 	'.$file.' exists in db already:'.($trackCheck==false)."<br/>\n";

				}
		   		echo "<br/>\n<br/>\n<br/>\n";
		    }
		}
	}


	public function fixTracksUrlsAction($urls)
	{
		//header('Content-Type: text/html; charset=UTF-8');
		$dataService = $this->get('warakin.service.data');
		
		$em = $this->getDoctrine()->getManager();

		$sql = 'SET foreign_key_checks = 0;'."\n";

		$trackCount = 0 ;
		foreach($urls as $key => $url){
			//$url = $urls[0];
 			//var_dump($url);
			if($trackCount < $trackCount + 400){

				// test if new track url already exists
				$track = $em
					->getRepository('AppSupplyWarakinBundle:Tags')
					->findOneByurl( $url['new']  );

				// var_dump($url);
				// echo '<br/>';
				if(!$track){
					// get track
					$track = $em
						->getRepository('AppSupplyWarakinBundle:Tags')
						->findOneByurl($url['old']);

						if($track){
							
							// evil sql
							$sql .= 'update tags set url="'. $url['new'].'" where url like "'.preg_replace("/[^A-Za-z0-9 .()#'\/=-]+/", '%',$url['old']).'" ;'."\n";
							$sql .= 'update statistics set url="'.$url['new'].'" where url like "'.preg_replace("/[^A-Za-z0-9 .()#'\/=-]+/", '%',$url['old']).'";'."\n";
							//$sql .= 'update uniqueid set url="'.$url['new'].'" where url like "'.preg_replace("/[^A-Za-z0-9 .()#'\/=-]+/", '%',$url['old']).'";'."\n";
							//$sql .= 'update tags_labels set url="'.$url['new'].'" where url like "'.preg_replace("/[^A-Za-z0-9 .()#'\/=-]+/", '%',$url['old']).'";'."\n\n\n";

					   		$em->clear(); // Detaches all objects from Doctrine!

							++$trackCount;

						} else {
							echo '## track not exist in db:'.$url['old']."<br/>\n"."\n";
						}
				}else{
					echo '## track exists already:'.$url['new'].'</br>'."\n";
				}
			}


			//var_dump($track->getUrl());

			//die();
		}
		$sql .= 'SET foreign_key_checks = 1; ##'; 

		echo $sql;
		echo "\n ## ".$trackCount.' lost files found';
	}


	public function scanForFilesAction()
	{
		// alter system parameters to accomodate scanning
		$timelimit_count = 5000;
		set_time_limit($timelimit_count);
		ini_set('memory_limit','320M');
		header('Content-Type: text/html; charset=utf-8');
		
		// setup variables for scan
		$dataService = $this->get('warakin.service.data');
		$i = 0;

		// set scanning directories
		

		// set filetypes to scan for
		$ftypes = array(
					'flac','Flac','Flac',
					'm4a','M4a','M4A',
					'm4p','M4p','M4P',
					'mp3','Mp3','MP3',
					// 'mpg','Mpg','MPG',
					'ogg','Ogg','OGG',
					'wav','Wav','WAV',
					'wma','Wma','WMA'
				);

		// create storage array
		$filelist = array();

		// loop thourgh dirs and scan them
		$em = $this->getDoctrine()->getManager();
		$t=0;
		foreach ($this->dirs as $dir) {
			$finder = new Finder();
			
			foreach ($finder->files()->in($dir)->name('/.?\.('.implode('|', $ftypes).')$/') as $key => $file) {
			//foreach ($finder->files()->in($dir)->name('/.?\.('.implode('|', $ftypes).')$/')->date('since 6 months ago') as $key => $file) {
				$timelimit_count += 60;
				set_time_limit($timelimit_count);
				// if track does not exist, analyze it
				if(!$this->checkTrackExistsInDb($file, $em)){
					echo 'File .'.$file." is new<br/>\n";
					$filelist[$file.''] = $this->getId3Tags($file);
					//break;

				} else {
					// echo 'File .'.$file." already present<br/>\n";
				}

			}

			echo 'traversed '.$dir."\n"."\n";
			//sbreak;
				
		}
		//var_dump($filelist);
		
		foreach ($filelist as $track)  {
			$this->storeTrack($track, $em);
		}
		
		return $this->getJsonResponse('NULL');
	}

	private function checkTrackExistsInDb($url, &$em){

		// see if track exists in DB
		$track = $em
			->getRepository('AppSupplyWarakinBundle:Tags')
			->findOneByurl( $url );
		
		return $track;

	}


	private function storeTrack($trackData=array(), &$em) {


		echo '<pre>';
		var_dump($trackData);
		echo '</pre>';
		if($trackData){
			//$em = $this->getDoctrine()->getManager();
			
			// store track in db

			$track = new \AppSupply\WarakinBundle\Entity\Tags();

			//echo 'pathname: '.$trackData['url']->getPathName()."<br/>\n";

			//$track->setUrl($trackData['url']->getPathName());
			$track->setUrl($trackData['url']);
			$track->setDeleted(0);
			$track->setCreatedate(new \DateTime());
			$track->setModifydate(new \DateTime());

			if(array_key_exists('title', $trackData)){
				if(
					$trackData['title'] !== null && 
					$trackData['title'] !== ''
				){
					$track->setTitle($trackData['title']);
				}else{
					$track->setTitle(basename($trackData['url']));
				}
			}else{
				$track->setTitle(basename($trackData['url']));
			}

			echo '<h3>Store track called for trackTitle: <i>'.$track->getTitle().'</i></h3>';



			if($track->getTitle()!== null && $track->getTitle()!== '') {
				//$trackStatistics = new \AppSupply\WarakinBundle\Entity\Statistics();

				// if(array_key_exists('url', $trackData)){
				// 	$track->setUrl($trackData['url']);
				// }

				if(array_key_exists('duration', $trackData)){
					echo "setting track duration to ".$trackData['duration']."<br/>\n";
					$track->setLength($trackData['duration']);
				}else{
					echo "no duration found for track<br/>\n";				
				}

				if(array_key_exists('track_number', $trackData)){
					$track->setTrack(intval($trackData['track_number']));
				}

				if(array_key_exists('bitrate', $trackData)){
					$track->setBitrate($trackData['bitrate']/1000);
				}

				if(array_key_exists('samplerate', $trackData)){
					$track->setSamplerate($trackData['samplerate']);
				}

				if(array_key_exists('filesize', $trackData)){
					$track->setFilesize($trackData['filesize']);
				}

				if(array_key_exists('filetype', $trackData)){
					$track->setFiletype($trackData['filetype']);
				}

				if(array_key_exists('bpm', $trackData)){
					$track->setBpm($trackData['bpm']);
				}

				if(array_key_exists('artist', $trackData)){
					if(!empty($trackData['artist'])){
						// check if artist exists in db
						$artist = $em
								->getRepository('AppSupplyWarakinBundle:Artist')
								->findOneByName($trackData['artist']);

						// if artist does not exists create artist
						if(!$artist){
							echo 'create new artist for name: '.$trackData['artist']."<br/>\n";
							$artist = new \AppSupply\WarakinBundle\Entity\Artist();
							$artist->setName($trackData['artist']);
							$em->persist($artist);
						}else{
							echo 'artist exists for name: '.$trackData['artist']."<br/>\n";	
						}
						$track->setArtist($artist);
					}
					
				}

				if(array_key_exists('album', $trackData)){
					if(!empty($trackData['album'])){
						// check if album exists in db
						echo 'check if album exists for name: '.$trackData['album']."<br/>\n";
						$album = $em
								->getRepository('AppSupplyWarakinBundle:Album')
								->findOneByName($trackData['album']);
						// if album does not exists; create it
						if(!$album){
							echo 'create new album for name: '.$trackData['album']."<br/>\n";
							$album = new \AppSupply\WarakinBundle\Entity\Album();
							$album->setName($trackData['album']);
							$em->persist($album);
						}else{
							echo 'album exists for name: '.$trackData['album']."<br/>\n";
						}
						$track->setAlbum($album); 
					}
				}


				if(array_key_exists('year', $trackData)){
					if(!empty($trackData['year'])){
						echo 'going to set year to '.$trackData['year']."<br/>\n";

						// check if year exists in db
						try{
							$year = $em
								->getRepository('AppSupplyWarakinBundle:Year')
								->findOneByName($trackData['year']);
						} catch(\Doctrine\DBAL\DBALException $e){
							echo 'could not query year for track : '.$track->getTitle().', year : '.$trackData['year']."<br/>\n";
							echo '<pre>';
							var_dump($e->getMessage());
							echo '</pre>';
						} 

						//if(isset($year)){
							// if year does not exists; create it
							if(!$year){
								echo 'create new year for name: '.$trackData['year']."<br/>\n";
								$year = new \AppSupply\WarakinBundle\Entity\Year();
								$year->setName($trackData['year']);
								$em->persist($year);
							}else{
								echo 'year exists for name: '.$trackData['year']."<br/>\n";
							}

							$track->setYear($year);
						//}
					}else{
						echo "track year is empty.<br/>\n";
					}
				}else{
					echo "no year found for track.<br/>\n";
				}

				
				if(array_key_exists('genre', $trackData)){
					if(!empty($trackData['genre'])){
						// check if genre exists in db
						echo 'genre: '.$trackData['genre']."<br/>\n";

						try{
							$genre = $em
								->getRepository('AppSupplyWarakinBundle:Genre')
								->findOneByName($trackData['genre']);
						} catch(\Doctrine\DBAL\DBALException $e){
							echo 'could not query genre for track : '.$track->getTitle().', genre: '.$trackData['genre']."<br/>\n";
							echo '<pre>';
							var_dump($e->getMessage());
							echo '</pre>';
						} 

						//if(isset($genre)){
							// if genre does not exists; create it
							if(!$genre){
								echo 'create new genre for name: '.$trackData['genre']."<br/>\n";
								$genre = new \AppSupply\WarakinBundle\Entity\Genre();
								$genre->setName($trackData['genre']);
								$em->persist($genre);
							}else{
								echo 'genre exists for name: '.$trackData['genre']."<br/>\n";
							}

							$track->setGenre($genre);
						// }else{
						// 	echo "Genre object was not set.<br/>\n";
						// }
					}else{
						echo "No genre string found.<br/>\n";
					}
				}else{
					echo "No genre found.<br/>\n";
				}

				
				try{
					$em->persist($track);
					//$em->clear(); // Detaches all objects from Doctrine!
				} catch(\Doctrine\DBAL\DBALException $e){
					echo 'could not store : '.$track->getTitle()."<br/>\n";
					echo '<pre>';
					var_dump($e->getMessage());
					echo '</pre>';

				} catch(\Doctrine\ORM\ORMException $e){
					echo 'something wrong : '.$track->getTitle()."<br/>\n";
					echo '<pre>';
					var_dump($e->getMessage());
					echo '</pre>';
				}

				try{
					$em->flush(); // Detaches all objects from Doctrine!
				} catch(\Doctrine\DBAL\DBALException $e){
					echo 'could not store : '.$track->getTitle()."<br/>\n";
					echo '<pre>';
					var_dump($e->getMessage());
					echo '</pre>';
					//$msg = '### Message ### \n'.$e->getMessage().'\n### Trace ### \n'.$e->getTraceAsString();
					//$this->container->get('logger')->critical($msg);

				} catch(\Doctrine\ORM\ORMException $e){
					echo 'something wrong : '.$track->getTitle()."<br/>\n";
					echo '<pre>';
					var_dump($e->getMessage());
					echo '</pre>';
					//$msg = '### Message ### \n'.$e->getMessage().'\n### Trace ### \n'.$e->getTraceAsString();
					//$this->container->get('logger')->critical($msg);
				}

			}
		}
			// echo '<pre>';
			// var_dump($track);
			// echo '</pre>';
	}

	private function getId3Property($tagSet, $propName){
		if(array_key_exists($propName, $tagSet)){
			if(is_array($tagSet[$propName])){
				return $tagSet[$propName][0];
			} else {
				return $tagSet[$propName];
			}
		} else {
			return null;
		}
	}

	private function getId3Tags($file){

		
		
		$getId3 = new GetId3();
		$tags = array();
		$tags['url'] = $file;			
		$tags["year"] = null;
		$tags["genre"] = null;
		$tags["artist"] = null;
		$tags["track_number"] = null;
		$tags["album"] = null;
		$tags["title"] = null;
		$tags["duration"] = null;
		$tags["samplerate"] = null;
		$tags["bitrate"] = null;
		$tags["filesize"] = null;
		$tags["filetype"] = null;
		$tags["bpm"] = null;

		$audioTags = $getId3
			            ->setOptionMD5Data(true)
			            ->setOptionMD5DataSource(true)
			            ->setEncoding('UTF-8')
			            ->analyze($file)
			        ;

		//var_dump($audioTags);
        
		// if (isset($audio['error'])) {
        //     throw new \RuntimeException(sprintf('Error at reading audio properties from "%s" with GetId3: %s.', $mp3File, $audio['error']));
        // }           
        // $this->setLength(isset($audio['playtime_seconds']) ? $audio['playtime_seconds'] : '');


		if(array_key_exists('filesize', $audioTags)){
			$tags['filesize'] = $audioTags['filesize'];
		}

		if(array_key_exists('fileformat', $audioTags)){
			$tags['filetype'] = $audioTags['fileformat'];
		}

		if(array_key_exists('audio', $audioTags)){
			if(array_key_exists('bitrate', $audioTags['audio'])){
				$tags['bitrate'] = $audioTags['audio']['bitrate'];
			}
			if(array_key_exists('sample_rate', $audioTags['audio'])){
				$tags['samplerate'] = $audioTags['audio']['sample_rate'];
			}
		}

		if(array_key_exists('playtime_seconds', $audioTags)){
			$tags['duration'] = $audioTags['playtime_seconds'];
		}

        if(array_key_exists('tags', $audioTags)){

	        if(array_key_exists('id3v2', $audioTags['tags'])){					
				$tags["year"] = $this->getId3Property($audioTags['tags']['id3v2'], "year");
				$tags["genre"] = $this->getId3Property($audioTags['tags']['id3v2'], "genre");
				$tags["artist"] = $this->getId3Property($audioTags['tags']['id3v2'], "artist");
				$tags["track_number"] = $this->getId3Property($audioTags['tags']['id3v2'], "track_number");
				$tags["album"] = $this->getId3Property($audioTags['tags']['id3v2'], "album");
				$tags["title"] = $this->getId3Property($audioTags['tags']['id3v2'], "title");	
			
				if(array_key_exists('bpm', $audioTags['tags']['id3v2'])){
	        		$tags["bpm"] = $audioTags['tags']['id3v2']['bpm'][0];
				}

	        } else if(array_key_exists('id3v1', $audioTags['tags'])){
	   //      	foreach($audioTags['tags']['id3v1'] as $k=>$v){
				// 	$filelist[$file.''][$k] = $v[0];
				// }				
				$tags["year"] = $this->getId3Property($audioTags['tags']['id3v1'], "year");
				$tags["genre"] = $this->getId3Property($audioTags['tags']['id3v1'], "genre");
				$tags["artist"] = $this->getId3Property($audioTags['tags']['id3v1'], "artist");
				$tags["track_number"] = $this->getId3Property($audioTags['tags']['id3v1'], "track_number");
				$tags["album"] = $this->getId3Property($audioTags['tags']['id3v1'], "album");
				$tags["title"] = $this->getId3Property($audioTags['tags']['id3v1'], "title");
				
				// bpm research 
				if(array_key_exists('TBPM', $audioTags['tags']['id3v1'])){
					echo "found bpm! TBPM :  <br/>\n";
					var_dump($audioTags['tags']['id3v1']['TBPM']);
				}
				if(array_key_exists('bpm', $audioTags['tags']['id3v1'])){
					echo "found bpm! bpm :  <br/>\n";
					var_dump($audioTags['tags']['id3v1']['bpm']);
				}
				if(array_key_exists('beats_per_minute', $audioTags['tags']['id3v1'])){
					echo "found bpm! beats per minute :  <br/>\n";
					var_dump($audioTags['tags']['id3v1']['beats_per_minute']);
				}	
				
	        } else if($audioTags['fileformat']=='mp4'){
					//echo  '<pre>';
	        		//var_dump($audioTags['quicktime']['moov']['subatoms'][2]['subatoms'][0]['subatoms'][1]['subatoms']);
	        		if(array_key_exists('subatoms', $audioTags['quicktime']['moov'])){
	        			if(array_key_exists('subatoms', $audioTags['quicktime']['moov']['subatoms'][2]['subatoms'][0])){
			        		foreach($audioTags['quicktime']['moov']['subatoms'][2]['subatoms'][0]['subatoms'][1]['subatoms'] as $k=>$v){
			        			if(strpos($v['hierarchy'], 'ART') || strpos($v['hierarchy'], 'art')){
									$tags['artist'] =  str_replace('data', '', $v['data']);
			        			}
			        			if(strpos($v['hierarchy'], 'ALB') || strpos($v['hierarchy'], 'alb')){
									$tags['album'] = str_replace('.data', '', $v['data']);
			        			}
			        			if(strpos($v['hierarchy'], 'NAM') || strpos($v['hierarchy'], 'nam')){
									$tags['title'] = str_replace('data', '', $v['data']);
			        			}
			        			if(strpos($v['hierarchy'], 'GEN') || strpos($v['hierarchy'], 'gen')){
									$tags['genre'] = str_replace('data', '', $v['data']);
			        			}
			        		}
		        		}
	        		}
	        		// var_dump($filelist[$file.'']);
					// echo  '</pre>';
					// die();
			} else {
        $tags["title"] = $file;
        echo 'no tags found in '.$file."<br/>\n"; 
        // echo  '<pre>';
        // var_dump($audioTags);
        // echo  '</pre>';
        // die();
    
      }

    }/*else{
		    echo 'no tags found in '.$file."<br/>\n"; 
			$tags["title"] = $file;
    }*/

    /// normalize $tags['track_number'] due to 07/11 notation being an issue
    if(strpos($tags['track_number'], '/')){
      $tags['track_number'] = substr($tags['track_number'], 0, strpos($tags['track_number'], '/'));
    }
		return $tags;

	}

	function essentiaAnalyzeTracksAction(){

		$batchSize = 5;

		$em = $this->getDoctrine()->getManager();
		// $tracks = $em
		// 	->getRepository('AppSupplyWarakinBundle:Tags')
		// 	->findOneById( $id );

		if(array_key_exists('offset', $_GET)){
			$offset = intval($_GET['offset']);
		}else{
			$offset = 0;
		}

		$q = $em->createQuery('SELECT t FROM AppSupplyWarakinBundle:Tags t')->setMaxResults($batchSize)->setFirstResult($offset);
		//$q->setMaxResults(24000);
		$tracks = $q->getResult();

		foreach($tracks as $key=>$track){

			//$urlEncoded = '/media/MP3.3/G/Gorillaz\ -\ Demon\ Days/02\ gorillaz\ -\ last\ living\ souls.mp3';
			$id = $track->getId(); //31551;
			$url = $track->getUrl();//'/media/MP3.3/G/Gorillaz - Demon Days/02 gorillaz - last living souls.mp3';
			var_dump($url);echo '<br/>';
			if(file_exists($url) && !file_exists('/media/junk/Essentia/'.$id.'_frames.json') ){
				$output = [];

				//$url = str_replace(' ', '\ ', $url);
				
				//var_dump($url);

				$cmd = '/media/junk/Essentia/essentia-extractors/essentia-extractors-v2.1_beta2/streaming_extractor_freesound '.escapeshellarg($url).'   /media/junk/Essentia/'.$id;
				$cmd = '/media/junk/Essentia/essentia-extractors/essentia-extractors-v2.1_beta2/streaming_extractor_freesound '.escapeshellarg($url).'   /media/junk/Essentia/'.$id;

				//var_dump($cmd);
				$outputTxt = exec($cmd, $output, $stuff);
				
				echo '<br/><br/><pre>';
				var_dump($stuff);
				echo '</pre><br/><br/><pre>';
				var_dump($output);
				echo '</pre><br/><br/>';
				//var_dump($outputTxt);


				$cmd = 'rm /media/junk/Essentia/'.$id.'_frames.json';
				//var_dump(file_get_contents('/media/junk/Essentia/'.$id.'_statistics.yaml'));$cmd = '/media/junk/Essentia/essentia-extractors/essentia-extractors-v2.1_beta2/streaming_extractor_freesound '.escapeshellarg($url).'   /media/junk/Essentia/'.$id;

				//var_dump($cmd);
				$outputTxt = exec($cmd, $output, $stuff);
			}
			
		}
		sleep(5);
        echo "<script>window['location'].href='?offset=".($offset+$batchSize)."'</script>";
		die();
		$response = new Response();
		$response->headers->set('Content-Type', 'application/json');
		$response->setContent(array('blegh'));

		return $response;
	}



	function getStatsAction(){

		$stats = [];

		$em = $this->getDoctrine()->getManager();


		$count = $em->createQueryBuilder()->select('count(t)')
			->from('AppSupplyWarakinBundle:Tags','t')
			//->andWhere('lfti is not null')
			//->groupBy('t')
			->getQuery()
			->getResult();
		
		$stats["TotalTracks"] = $count[0][1];

		$count = $em->createQueryBuilder()->select('count(t)')
			->from('AppSupplyWarakinBundle:Tags','t')
			//->leftJoin('AppSupplyWarakinBundle:LastFmTrackInfo','lfti')
			->where('t.lastFmUrl is null')
			->orWhere('t.lastFmUrl = :emptyVal')
			->setParameter('emptyVal', '')
			//->andWhere('lfti is not null')
			//->groupBy('t')
			->getQuery()
			->getResult();
		
		$stats["NoLastFm"] = $count[0][1];

		$count = $em->createQueryBuilder()->select('count(t)')
			->from('AppSupplyWarakinBundle:Tags','t')
			//->leftJoin('AppSupplyWarakinBundle:LastFmTrackInfo','lfti')
			->where('t.lastFmUrl is not null')
			->andWhere('t.lastFmUrl != :emptyVal') 
			->setParameter('emptyVal', '')
			//->andWhere('lfti is not null')
			//->groupBy('t')
			->getQuery()
			->getResult();
		
		$stats["LastFmUrl"] = $count[0][1];

		$count = $em->createQueryBuilder()->select('count(t)')
			->from('AppSupplyWarakinBundle:Tags','t')
			//->leftJoin('AppSupplyWarakinBundle:LastFmTrackInfo','lfti')
			->where('t.lastFmMbid is null')
			->orWhere('t.lastFmMbid = :emptyVal')
			->setParameter('emptyVal', '')
			//->andWhere('lfti is not null')
			//->groupBy('t')
			->getQuery()
			->getResult();
		
		$stats["LastFmNoMbId"] = $count[0][1];

		$count = $em->createQueryBuilder()->select('count(t)')
			->from('AppSupplyWarakinBundle:Tags','t')
			//->leftJoin('AppSupplyWarakinBundle:LastFmTrackInfo','lfti')
			->where('t.lastFmMbid is not null')
			->andWhere('t.lastFmMbid != :emptyVal')
			->setParameter('emptyVal', '')
			//->andWhere('lfti is not null')
			//->groupBy('t')
			->getQuery()
			->getResult();
		
		$stats["LastFmMbId"] = $count[0][1];

		$count = $em->createQueryBuilder()->select('count(t)')
			->from('AppSupplyWarakinBundle:Tags','t')
			//->leftJoin('AppSupplyWarakinBundle:LastFmTrackInfo','lfti')
			->where('t.lastFmMbid is not null')
			->andWhere('SIZE(t.lastFmTags) = 0')
			->andWhere('t.lastFmMbid != :emptyVal')
			->setParameter('emptyVal', '')
			//->andWhere('lfti is not null')
			//->groupBy('t')
			->getQuery()
			->getResult();

		$stats["LastFmMbIdNoTags"] = $count[0][1];

		$count = $em->createQueryBuilder()->select('count(t)')
			->from('AppSupplyWarakinBundle:Tags','t')
			//->leftJoin('AppSupplyWarakinBundle:LastFmTrackInfo','lfti')
			->where('t.lastFmMbid is not null')
			->andWhere('SIZE(t.lastFmTags) > 0')
			->andWhere('t.lastFmMbid != :emptyVal')
			->setParameter('emptyVal', '')
			//->andWhere('lfti is not null')
			//->groupBy('t')
			->getQuery()
			->getResult();

		$stats["LastFmMbIdWithTags"] = $count[0][1];


		
		$response = new Response();
		$response->headers->set('Content-Type', 'application/json');
		$response->setContent(json_encode($stats));

		return $response;
	}

	function synchLastFmTracksInfoAction(){

		$batchSize = 1;

		$em = $this->getDoctrine()->getManager();

		if(array_key_exists('offset', $_GET)){
			$offset = intval($_GET['offset']);
		}else{
			$offset = 0;
		}
		// 	todo: preselect tracks without last fm tags to make sure there is no waste actions

		/*
				SELECT 
					t1.ID
				FROM tags t1
						LEFT JOIN tracks_last_fm_tags t2 ON t1.ID = t2.tracks_id
				WHERE t2.tracks_id IS NULL
				limit 0, 10
		*/



		//$q = $em->createQuery('SELECT t FROM AppSupplyWarakinBundle:Tags t')->setMaxResults($batchSize)->setFirstResult($offset);
		//$q->setMaxResults(24000);
		
		$tracks = $em->createQueryBuilder()->select('t')
			->from('AppSupplyWarakinBundle:Tags','t')
			->where('SIZE(t.lastFmTags) = 0')
			->andWhere('t.lastFmMbid is not null')
			->andWhere('t.lastFmMbid != :emptyVal')
			->setParameter('emptyVal', '')
			->setMaxResults($batchSize)
			->setFirstResult($offset)
			->getQuery()
			->getResult();
		
		if(count($tracks) < 1){
			echo "<script>window['location'].href='?offset=0'</script>";
			die();
		}
		//$q = $em->createQuery('SELECT t FROM AppSupplyWarakinBundle:Tags t')->setMaxResults($batchSize)->setFirstResult($offset);
		//$q->setMaxResults(24000);
		//$tracks = $q->getResult();

		$tracksSucceeded = 0;

		foreach($tracks as $key=>$track){
			$trackId = $track->getId();
			$this->synchLastFmTrackInfo($track);
			
			$returnedTrack = $em
				->getRepository('AppSupplyWarakinBundle:Tags')             
				->findOneById($trackId);
			
			if( count($returnedTrack->getLastFmTags() ) > 0){
				$tracksSucceeded += 1;
			}

		}
		
		

		echo "<script>window['location'].href='?offset=".($offset+$batchSize - $tracksSucceeded)."'</script>";
		
		die();
		$response = new Response();
		$response->headers->set('Content-Type', 'application/json');
		$response->setContent(array('blegh'));

		return $response;
	}

	function synchLastFmTrackInfoAction($trackId){
		//$urlEncoded = '/media/MP3.3/G/Gorillaz\ -\ Demon\ Days/02\ gorillaz\ -\ last\ living\ souls.mp3';
		$em = $this->getDoctrine()->getManager();
		$track = $em
					->getRepository('AppSupplyWarakinBundle:Tags')
					->findOneById($trackId);
		$this->synchLastFmTrackInfo($track);

		$response = new Response();
		$response->headers->set('Content-Type', 'application/json');
		$response->setContent(array('blegh'));

		return $response;
	}

	function synchLastFmTrackInfo($track){
		$synchService = $this->get('warakin.service.synch');
		$id = $track->getId(); //31551;
		$url = $track->getUrl();//'/media/MP3.3/G/Gorillaz - Demon Days/02 gorillaz - last living souls.mp3';
		echo $id.'::'.$url.'<br/>';
		if(file_exists($url)){
			$synchService->fetchLastFmTrackInfo($track);
		}
	}

	function synchDiscogsTrackInfo($track){
		$discogs = $this->container->get('discogs');
		
		$id = $track->getId(); //31551;
		$url = $track->getUrl();//'/media/MP3.3/G/Gorillaz - Demon Days/02 gorillaz - last living souls.mp3';
		var_dump($url);echo '<br/>';
		if(file_exists($url)){
			$discogs->fetchLastFmTrackInfo($track);
		}
	}

	function rescanAlbumId3Tags($albumId){
		$em = $this->getDoctrine()->getManager();
		// get tracks for album
		$album = $em
			->getRepository('AppSupplyWarakinBundle:Album')
			->findOneById($albumId);

		$albumTracks = $album->getTags();

		// for each track, scan the id3 tags
		foreach($albumTracks as $index=>$albumTrack){
			// if tags are different then the tags in the db, overwrite them

			// commit the changes to the db
		}
	}
}


