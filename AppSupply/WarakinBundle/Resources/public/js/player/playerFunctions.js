
// The Audio player
let my_media = null;
let mediaTimer = null;
// duration of media (track)
let dur = -1;

// need to know when paused or not
let is_paused = false;
let is_stopped = false;



//Set audio position on page
function setAudioPosition(position) {
    position = Math.round(my_media.position);

    if(position>0){
        //$("#audio_position").html(secToMin(position));
        $('#trackProgressBar .bar').width(Math.round( position / my_media.duration * 100)+ '%');
	}
    else if(is_stopped)
    {
        $("#audio_position").html("stopped");
    }
    else
    {
	    $("#audio_position").html("Loading...");
        //$("#audio_position").html('0:00');
	}
}

function pad(number, length) {
    let str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}

function secToMin(seconds){
    let minutes = Math.floor(seconds/60);
	let newSeconds = pad(parseInt(seconds - minutes*60), 2);
	return minutes+':'+newSeconds;
}

function playTrack(trackHash){
    $("#progressBarContainer .cuePoint").remove();
    console.log('playTrack('+trackHash+') called');

    //console.log('next audio: '+PLAYLISTINDEX+' - '+$('#playlist').children().length );
    if (PLAYLISTINDEX == $('#playlist').children().length && PLAYLISTSETTINGS.autonext==true){
        generateNextTracks($('#playlist li:last-child'));
    }
    
    // fetch track element
    let trackEl = $('#'+trackHash);
    //console.log('trackEl', trackEl);
    let src = trackEl.data('src').replace("%5C'", "'").replace("#", "%23");
    let playlist = $('#playlist');
    

    let scrollTop = playlist.scrollTop();
    let position = trackEl.position();

    playlist.scrollTop(position.top + scrollTop - 111 /*- 3 * $('#playlist li').height() */);

    // console.log('SERATO FEATURES:');
    // console.log(trackEl.find('.serato').data('seratoFeatures'));
    let seratoFeatures = trackEl.find('.serato').data('seratoFeatures');
    if(seratoFeatures){
        // console.log('SERATO FEATURES:');
        // console.log($(trackEl).data('object'));
        let duration = $(trackEl).data('object').duration*1000; //trackEl.find('.duration').data('duration');
        
        $.each(seratoFeatures.serato_cuepoints, function(cuePointKey, cuePointVal) {
            // console.log('SERATO CUEPOINT');
            // console.log(cuePointVal);
            /**
             *  color:0
                id:217
                order:1
                position:88250
             */

            positionByPercentage = Math.round(cuePointVal.position/duration * 100); 
            let cuePointEl = $("<span class=\"cuePoint\"></span>");
            cuePointEl.css('left', positionByPercentage+"%");
            cuePointEl.css('border-top-color', cuePointVal.color);
            console.log(cuePointEl);
            $("#progressBarContainer").append(cuePointEl);
            cuePointEl.click( () => {
                //console.log('clicked track');
                seekAudioMs(cuePointVal.position);
            });
        });
    }

    // highlight playing track
    $('#playlist .active').removeClass('active');
    $('#playlist li:nth-child('+PLAYLISTINDEX+')').addClass('active');

    // set currently playing meta data
    $('#artistName').text(trackEl.find('.artist').text());
    $('#trackName').text(trackEl.find('.title').text());
    $('#albumName').text(trackEl.find('.album').text());
    $('#genreName').text(trackEl.find('.genre').text());
    $('#media_dur').text(trackEl.find('.duration').text());


    if ('mediaSession' in navigator) {
        navigator.mediaSession.metadata = new MediaMetadata({
            title: trackEl.find('.title').text(),
            artist: trackEl.find('.artist').text(),
            album: trackEl.find('.album').text(),
            artwork: [
            { src: trackEl.data('cover'),   sizes: '96x96',   type: 'image/png' },
            { src: trackEl.data('cover'), sizes: '128x128', type: 'image/png' },
            { src: trackEl.data('cover'), sizes: '192x192', type: 'image/png' },
            { src: trackEl.data('cover'), sizes: '256x256', type: 'image/png' },
            { src: trackEl.data('cover'), sizes: '384x384', type: 'image/png' },
            { src: trackEl.data('cover'), sizes: '512x512', type: 'image/png' },
            ]
        });
    }

    //console.log('playTrack.cover', trackEl.data('cover'));
    let coverParent = $('#currentAlbumCover').parent();
	coverParent.empty();
	coverParent.append('<img id="currentAlbumCover" src="'+trackEl.data('cover')+'">');

    //set status bar
    $('#trackProgressBar .bar').css('width', '0%');

    document.title = trackEl.find('.artist').text()+' - '+trackEl.find('.title').text();

    // update playbutton states
    $('#playButton').css('display', 'none');
    $('#pauseButton').css('display', 'inline-block');

    playFile(trackHash, src);
}

function playFile(trackHash, src) {
    //  Set active
    console.log('Called playFile('+trackHash+')');
    
    stopAudio();
    is_stopped = false;
    
    // ui niceties
    //$("#media_dur").html("0:00");
    $("#audio_position").html("Loading...");
    let onSuccessCallback = onSuccess;
    // Create Media object from src
    my_media = soundManager.createSound({
        id: trackHash,
        //autoLoad: true,
        url: ['app_dev.php/data/track/'+trackHash+'/fileById'],
        onplay:function(){
            if(this.readyState == 2){
                //console.log('onplay: track load error / decode error. play next track.');
                //nextAudio();
            }
        },
        onload:function(success){
            if(success == true){
                $.getJSON(PREFIX+'app_dev.php/data/track/'+trackHash+'/logplay?localTime='+(new Date().toJSON()), function(data) {
                    console.log('Logged play for track: '+trackHash);
                });
            }
        },
//      onpause:function(){console.log('paused');},
//             		onstop:function(){console.log('stopped'); 
// //                        if(this.readyState == 2){
// //                                console.log('onstop: track load error / decode error. play next track.');
// //                                nextAudio();
// //                        }
//             		},
        onfinish:function(){
            //console.log('WARAKIN-BROWSER: finished playing');
            onSuccessCallback();
        },
		// onload:function(){
  //           if(this.readyState == 2){
  //                   console.log('onload: track load error / decode error. play next track.');
  //                   nextAudio();
  //           }
		// },
		ondataerror:function(){
			//console.log('soundmanager: track error, play next');
			nextAudio()
		},
		whileplaying:function(){
            setAudioPosition(my_media.position/1000);
		},
		whileloading:function(){

			//console.log('whileloading: sound '+this.id+' loading, '+ this.bytesLoaded +' of '+ this.bytesTotal );
            if(this.readyState == 2){
                //console.log('whileloading: track load error / decode error. play next track.');
                //nextAudio();
            } else {
                $('#loadProgressBar .bar').css('width', Math.round(( this.bytesLoaded/this.bytesTotal) * 100 ) +'%');
            }
		}

    });
    
    my_media.play();
}

//onSuccess Callback
function onSuccess() {
    //console.log('Track.onSuccess called.');
    $('#messages').html("");
    
    clearInterval(mediaTimer);
    
    is_paused = false;
    dur = -1;
    
    if(is_stopped===false){
        nextAudio();
    }
}

//onError Callback
function onError(error) {
    $('#messages').html( "error " + error.code + ":" + error.message );
    alert('Load Error occured!'+"\n"+"Check your connection."+"\n"+'code: '    + error.code    + "\n" + 'message: ' + error.message + '\n');
    clearInterval(mediaTimer);
    mediaTimer = null;
    my_media.release();
    my_media = null;
    is_paused = false;
    is_stopped = true;
    setAudioPosition("0");
    nextAudio()
}

function onStatus(status){
    console.log("audioStatus:");
    console.log(status);
}

function seekAudio(percentage){

    //console.log('Called skipAudio( ' +percentage+' )');
    //console.log('duration: '+ $('#playlist li:nth-child('+PLAYLISTINDEX+')').data('duration'));

    let duration = $('#playlist li:nth-child('+PLAYLISTINDEX+')').data('duration');
    position = Math.round(percentage * duration  * 1000); 

    //console.log('Skip to position '+position+' ms');
   
    seekAudioMs( position );

}

function seekAudioMs(ms){

    //console.log('Called skipAudio( ' +percentage+' )');
    //console.log('duration: '+ $('#playlist li:nth-child('+PLAYLISTINDEX+')').data('duration'));
    //console.log('Skip to position '+position+' ms');
    
    my_media.setPosition( ms );

}


//Pause audio
function pauseAudio() 
{
    $('#playButton').css('display', 'inline-block');
    $('#pauseButton').css('display', 'none');
    if (is_paused) return;
    if (my_media) {
        is_paused = true;
        my_media.pause();
    }
}

function nextAudio() 
{
    PLAYLISTINDEX = parseInt( PLAYLISTINDEX )+1;
    //stopAudio();
    trackId = $('#playlist li:nth-child('+PLAYLISTINDEX+')').attr('id');
    if(trackId){
        playTrack(trackId );
    }
}

//Stop audio
function stopAudio() 
{
    if (my_media) {
        //my_media.stop();
        my_media.destruct();
	    //soundManager.destroySound($('#playlist li:nth-child('+PLAYLISTINDEX+')').attr('id'));
    }
	
    //$('#messages').html("");
    
    is_stopped = true;
    is_paused = false;
    dur = 0;
}


function removeTrackFromPLaylist(el)
{
    if(el.index() < PLAYLISTINDEX ){
        PLAYLISTINDEX = PLAYLISTINDEX -1;
    }
    el.remove();
}


function clearPlaylist()
{
    PLAYLISTINDEX = 0;
    $('#playlist').empty();
    $('#playlistname').css('display', 'none');
    //$('#playlistsave').css('display', 'none');
    $('#playlistname').data('playlist-id', undefined);
    $('#playlistname').html('');
}


// test for url formatting
function isUrl(s) 
{
    let regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
    return regexp.test(s);
}
