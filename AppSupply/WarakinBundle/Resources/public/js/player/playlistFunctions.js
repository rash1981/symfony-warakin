//playlistFunctions

// function addTrackElToPlaylist(trackEl, options){
// 	// console.log("addTrackElToPlaylist called");
// 	// console.log(trackEl.parent().parent().data('object'));
// 	addTrackToList(trackEl.parent().parent().data('object'), '#playlist', playTrackFromList, []);
// }

function addTrackToPlaylist(trackObj, options){
	console.log("addTrackToPlaylist called");
	console.log('addTrackToPlaylist.trackObj:');
	console.log(trackObj);
	console.log('addTrackToPlaylist.options:');
	console.log(options);
	
	if(!trackObj.hasOwnProperty('id') && trackObj.parent){
		let trackEl = trackObj.parent().parent();
		console.log(trackEl);
		console.log(trackEl.data('object'));
		trackObj = trackEl.data('object');
	}
	addTrackToList(trackObj, '#playlist', playTrackFromList, options);
}

function addTrackToList(trackObj, listSelector, clickAction, options){
	console.log('addTrackToList called');
	// fix rating if track has no statistics property set
	if(!trackObj.statistics){
		trackObj.statistics = new Object();
		trackObj.statistics.rating = 0;
	}

	console.log('---------------------[ track ]-----------------------------');
	console.log(trackObj);
	console.log('---------------------[ /track ]-----------------------------');

	// dirty fix for generated playlist rating
	if(trackObj.rating){
		trackObj.statistics.rating = trackObj.rating;
	}
	if(!trackObj.statistics.rating){
		trackObj.statistics.rating = 0;
	}
	
	// find a cover in the object
	if(trackObj.cover){
		cover = trackObj.cover;
	}else if(trackObj.album){
		cover = "data/albums/"+trackObj.album.id+"/cover";
	}else{
		cover = "bundles/appsupplywarakin/img/defaultAlbum.png";
	}

	if(trackObj.year){
		if(trackObj.year.name){
			trackObj.year = trackObj.year.name;
		}
	}

	/**
	 *  determine track key from service metadata
	 */

	// TODO:  checking for Traktor, Rekordbox, VirtualDJ
	// TODO:  allow key prioritisation, ie. "i use traktor. pick de traktor keys over the other software cuepoint sources, as it is most likely i have managed my stuff in traktor."
	
	trackObj.music_key = false;

	// if serato_features are available, use these
	if(trackObj.serato_features){
		if(trackObj.serato_features.music_key){
			trackObj.music_key = trackObj.serato_features.music_key;
		}

	// if spotify data is available, use this key in lieu of the (manual) serato settings
	}else if (trackObj.spotify_audio_features){
		if(trackObj.spotify_audio_features.music_key){
			// TODO: transform spotify key notation to camelot / traditional chords notation
			// 		 see spotify documentation	:	https://developer.spotify.com/web-api/get-audio-features/
			//		 and pitch class notation	:	https://en.wikipedia.org/wiki/Pitch_class

			/**
			 * 	0	C (also B♯, Ddouble flat)
			 *	1	C♯, D♭ (also Bdouble sharp)
			 *	2	D (also Cdouble sharp, Edouble flat)
			 *	3	D♯, E♭ (also Fdouble flat)
			 *	4	E (also Ddouble sharp, F♭)
			 *	5	F (also E♯, Gdouble flat)
			 *	6	F♯, G♭ (also Edouble sharp)
			 *	7	G (also Fdouble sharp, Adouble flat)
			 *	8	G♯, A♭
			 *	9	A (also Gdouble sharp, Bdouble flat)
			 *	10  t or A,	A♯, B♭ (also Cdouble flat)
			 *	11  e or B, B (also Adouble sharp, C♭)
			 * 
			 */
			 keyPitchClassCodes = [
				 'C',
				 'C#',
				 'D',
				 'D#',
				 'E',
				 'F',
				 'F#',
				 'G',
				 'G#',
				 'A',
				 'A#',
				 'B'
			];
			trackObj.music_key = keyPitchClassCodes[trackObj.spotify_audio_features.music_key];
		}
	}

	//trackObj.music_key = tonalKeyToCamelotKey(trackObj.music_key);
	

	let trackData = {
			'id':trackObj.id,
			/*'url':trackObj.url,*/
			'title':trackObj.title,
			'artist':trackObj.artist,
			'cover':cover,
			'genre':trackObj.genre,
			'duration':trackObj.length,
			'bitrate':trackObj.bitrate,
			'last_fm_url':trackObj.last_fm_url,
			'serato':trackObj.serato_features,
			'music_key':tonalKeyToCamelotKey(trackObj.music_key),
			'year':trackObj.year,
			'statistics':trackObj.statistics,
			'bpm':trackObj.bpm,
			'playlist_tracks':trackObj.playlist_tracks
		}

	if(trackObj.album){
		trackData.album = trackObj.album.name;
		trackData.album_id = trackObj.album.id;
	}

	track = createTrackEl(trackData);

	console.log(track.data('object'));
   	track.prop("id", trackObj.id);
	track.find('.info a').attr('href','player/track/'+trackObj.id);

	track.find('.title').click( function() {
		console.log('clicked track');
		console.log(clickAction);
		console.log('this:');
		console.log($(this));
		//clickAction($(this));
		clickAction($(this));
	});


	if(options){
		if(options.hasOwnProperty('insertAt')){
			//console.log('foudn insertAT option:'+options.insertAt);
			$("#playlist > li:nth-child(" + (options.insertAt) + ")").after(track);
		}else{
			$(listSelector).append(track);
		}
	}else{
		$(listSelector).append(track);
	}

	if(listSelector == '#playlist'){
		calculatePlaylistLength();
	}
}

function tonalKeyToCamelotKey(tonalKey){
	let camelotKey = false;
	//console.log('tonalKey: '+tonalKey);
	if(tonalKey){
		const keyRosetta = [
			// CAMELOT 1
			[
				'01A',				// CAMELOT WHEEL
				11, 				// PITCH CLASS
				'A-Flat Minor',		// TRADITIONAL	
				'Abm'
			],
			[
				'01A',				// CAMELOT WHEEL
				11, 				// PITCH CLASS
				'G-Sharp Minor',	// TRADITIONAL	
				'G#m'
			],	
			[
				'01B',				// CAMELOT WHEEL
				11 /* e or B */,	// PITCH CLASS
				'B Major',			// TRADITIONAL	
				'B'
			],
			[
				'02A',				// CAMELOT WHEEL
				6, 					// PITCH CLASS
				'E-Flat Minor',		// TRADITIONAL	
				'Ebm'
			],

			// CAMELOT 2
			[
				'02B',				// CAMELOT WHEEL
				6, 					// PITCH CLASS
				'F-Sharp Major',	// TRADITIONAL	
				'F#'
			],
			[
				'02B',				// CAMELOT WHEEL
				6, 					// PITCH CLASS
				'G-Flat Major',		// TRADITIONAL	
				'Gb'
			],
			[
				'03A',				// CAMELOT WHEEL
				1,	 				// PITCH CLASS
				'B-Flat Minor',		// TRADITIONAL	
				'Bbm'
			],
			[
				'03B',				// CAMELOT WHEEL
				1, 					// PITCH CLASS
				'D-Flat Major',		// TRADITIONAL	
				'Db'
			],
			[	
				'03B',				// CAMELOT WHEEL
				1, 					// PITCH CLASS
				'C-Sharp Major',	// TRADITIONAL	
				'C#'
			],

			// CAMELOT 4
			[
				'04A',				// CAMELOT WHEEL
				8, 					// PITCH CLASS
				'F Minor',			// TRADITIONAL	
				'Fm'
			],
			[
				'04B',				// CAMELOT WHEEL
				8,	 				// PITCH CLASS
				'A-Flat Major',		// TRADITIONAL	
				'Ab'
			],
			[
				'04B',				// CAMELOT WHEEL
				8,	 				// PITCH CLASS
				'G-Sharp Major',	// TRADITIONAL	
				'G#'
			],

			// CAMELOT 5
			[
				'05A',			// CAMELOT WHEEL
				3, 				// PITCH CLASS
				'C Minor',		// TRADITIONAL	
				'Cm'
			],
			[
				'05B',			// CAMELOT WHEEL
				3, 				// PITCH CLASS
				'E-Flat Major',	// TRADITIONAL	
				'Eb'
			],
			[
				'05B',			// CAMELOT WHEEL
				3, 				// PITCH CLASS
				'D-Sharp Major',	// TRADITIONAL	
				'D#'
			],

			// CAMELOT 6
			[
				'06A',			// CAMELOT WHEEL
				10, 			// PITCH CLASS
				'G Minor',		// TRADITIONAL	
				'Gm'
			],
			[
				'06B',			// CAMELOT WHEEL
				10, 			// PITCH CLASS
				'B-Flat Major',	// TRADITIONAL	
				'Bb'
			],
			[
				'06B',			// CAMELOT WHEEL
				10, 			// PITCH CLASS
				'A-Sharp Major',	// TRADITIONAL	
				'A#'
			],

			// CAMELOT 7
			[
				'07A',			// CAMELOT WHEEL
				5, 				// PITCH CLASS
				'D Minor',		// TRADITIONAL	
				'Dm'
			],
			[
				'07B',			// CAMELOT WHEEL
				5, 				// PITCH CLASS
				'F Major',		// TRADITIONAL	
				'F'
			],
			[
				'08A',			// CAMELOT WHEEL
				0, 			// PITCH CLASS
				'A Minor',		// TRADITIONAL	
				'Am'
			],
			[
				'08B',			// CAMELOT WHEEL
				0, 				// PITCH CLASS
				'C Major',		// TRADITIONAL	
				'C'
			],
			[
				'09A',			// CAMELOT WHEEL
				7, 				// PITCH CLASS
				'E Minor',		// TRADITIONAL	
				'Em'
			],
			[
				'09B',			// CAMELOT WHEEL
				7, 				// PITCH CLASS
				'G Major',		// TRADITIONAL	
				'G'
			],
			[
				'10A',			// CAMELOT WHEEL
				2, 				// PITCH CLASS
				'B Minor',		// TRADITIONAL	
				'Bm'
			],
			[
				'10B',			// CAMELOT WHEEL
				2, 				// PITCH CLASS
				'D Major',		// TRADITIONAL	
				'D'
			],
			[
				'11A',			// CAMELOT WHEEL
				9, 				// PITCH CLASS
				'F-Sharp Minor',// TRADITIONAL	
				'F#m'
			],
			[
				'11B',			// CAMELOT WHEEL
				9, 				// PITCH CLASS
				'A Major',		// TRADITIONAL	
				'A'
			],
			
			// CAMELOT 12
			[
				'12A',			// CAMELOT WHEEL
				4, 				// PITCH CLASS
				'C-Sharp Minor',// TRADITIONAL	
				'C#m'
			],
			[
				'12A',			// CAMELOT WHEEL
				4, 				// PITCH CLASS
				'D-Flat Minor',	// TRADITIONAL	
				'Dbm'
			],
			[
				'12B',			// CAMELOT WHEEL
				4, 				// PITCH CLASS
				'E Major',		// TRADITIONAL	
				'E'
			]
		];

		camelotKey = '('+tonalKey+')';
		//assign properties to element
		if(typeof(tonalKey) === 'string'){
			//console.log('tonalKey: '+tonalKey);
			$.each(keyRosetta, function(keyKey, keyVal) {
				//console.log('tonalkey:'+keyVal);
				if(keyVal[3].toLowerCase() === tonalKey.toLowerCase().replace(' ', '') ){
					// console.log("let camelotKey: "+camelotKey);
					// console.log("camelotKey: "+keyVal[0]);
					camelotKey = keyVal[0];
				}
			});
		}
		//console.log("Final found camelotKey: "+camelotKey);
	}
	return camelotKey;

}

function playTrackFromList(El){
	PLAYLISTINDEX = El.parent().parent().index()+1;
    pauseAudio();
	playTrack(El.parent().parent().attr('id') );
}

function createTrackEl(trackData){
	let track = template = $("#templates .track").clone();
	//console.log('createTrackEL: trackData.id:'+trackData.id);
	//console.log('trackData', trackData);
	track.data('id', trackData.id);

	/*track.data('src', trackData.url);*/
	track.data('object', trackData)
	track.data('cover', trackData.cover);
	track.data('duration', trackData.duration);
	track.find('.title').text(trackData.title);

	//assign properties to element
	$.each(trackData, function(trackKey, trackVal) {
		//  console.log('trackkey: '+trackKey);
		//  console.log('trackVal: ');
		//  console.log(trackVal);
		if (trackKey == 'artist' && trackVal !== undefined) {
			track.find('.artist').text(trackVal.name);
		} else if (trackKey == 'duration') {
			track.find('.'+trackKey).text(secToMin(trackVal));
		} else if (trackKey == 'statistics') {
			$.each(trackVal, function(statKey, statVal) {
				if (statKey == 'rating') {
					track.find(".rating_display").raty({
						half    : true,
						starType: 'i',
						score   : statVal/2,
						click	: function(score, evt) {
							//console.log('Rating for '+$(this).parent().parent().data('src')+' set to '+score);
							$.getJSON(PREFIX+'data/track/'+trackData.id+'/rating/'+score, function(data) {
								//console.log('Rating for '+this.track_src+' set to '+score);
						 	});
						},
					});
				} else {
					track.find('.'+statKey).text(statVal);
				}
			});
		} else if (trackKey == 'bpm' && trackVal) {
			track.find('.'+trackKey).text(parseFloat(trackVal).toFixed(2));
		} else if (trackKey == 'genre'){
			if( trackVal){
				track.find('.'+trackKey+' select').attr('id', 'genre-for-track-'+trackData.id);
				track.find('.'+trackKey+' select option:first-child').text(trackVal);
			}
			// set interactive elements 
			track.find('.'+trackKey+' select').on('change', function(){
				//console.log(this.value);
				$.getJSON(PREFIX+'data/track/'+trackData.id+'/genre/'+this.value, function(data) {
					//console.log(data);
				});
			});
		} else if (trackKey == 'last_fm_url' && trackVal) {
			track.find('.'+trackKey).html('<a href="'+trackVal+'" rel="noopener noreferrer" target="_blank"><i class="fab fa-lastfm"></i></a>');
		} else if (trackKey == 'serato') {
			if(trackVal){
				//console.log('serato features found');
				//console.log('now check for cuepoints:');
				//console.log(trackVal);
				if(trackVal.serato_cuepoints.length > 0){
					track.find('.'+trackKey).css('visibility', 'visible');
					track.find('.'+trackKey).data('seratoFeatures', trackVal);
					track.find('.'+trackKey).html(trackVal.serato_cuepoints.length+' '+track.find('.'+trackKey).html());
				}
			} else {
				// 	track.find('.'+trackKey).css('visibility', 'visible');
				// 	track.find('.'+trackKey).data('seratoFeatures', trackVal);
				// 	track.find('.'+trackKey).html(trackVal.serato_cuepoints.length+' '+track.find('.'+trackKey).html());
			}
		} else  if (trackKey == 'playlist_tracks' && trackVal	) {
			//console.log('playlist_tracks found');
			//console.log(trackVal);
			let playlistIds = [];
			$.each(trackVal, function(valKey, playlistTrack) {
				//console.log('playlistTrack('+trackData.id+').playlist.id:'+playlistTrack.playlist.id);
				playlistIds.push(playlistTrack.playlist.id);
			});
			track.data('playlistIds', playlistIds);
		} else  if (trackKey == 'cover' && trackVal	) {
			//console.log('playlist_tracks found');
			//console.log(trackVal);
			track.find('.'+trackKey+" img").prop("src", trackVal);
		} else {
			track.find('.'+trackKey).text(trackVal).addClass(trackKey+'-'+trackVal);
		}

	});

	track.data("trackData", trackData);

	track.find('.info').on('click',function(){
		console.log($(this).parent().parent().data('id'));
	});

	return track;

}

function emptyPlaylist(){
	if(confirm("Are you sure you want to clean the playlist")){
		$("#playlistUl").children().remove();
		$("#playlistUl").listview('refresh');
	}
}

function showSaveTrackToPlaylistModal(trackEl){

	// console.log('trackEl');	
	// console.log(trackEl);
	//loadPlaylists();
		
	const id = $(trackEl).prop('id');
	
	$('#saveTrackToPlaylist').attr('data-track-id', id);

	// highlight the playlists the track is already in
	$('#saveTrackToPlaylist ol li').removeClass('inList');

	let playlistIds = trackEl.data('playlistIds');
	// console.log('all data properties');
	// console.log(trackEl.data());
	// console.log('playlistIds');
	// console.log(playlistIds);

	$.each($('#saveTrackToPlaylist ol li'), function(elKey, elVal){

		console.log('existingPlaylistId: '+$(elVal).data('playlistId'));
		if(playlistIds){
			$.each(playlistIds, function(key, val) {
				// console.log('trackPlaylistId:'+val);
				// console.log(val);
				if($(elVal).data('playlistId') == val){
					// console.log('Adding inList class to playlist');
					$(elVal).addClass('inList');
				}
			});
		}
	});

	$('#saveTrackToPlaylist').modal('show');
}

function saveTrackToPlaylist(playlistId){

	$.getJSON(PREFIX+PATH+SYMFONYROOT+'data/playlists/'+playlistId+'/addTrack/'+$('#saveTrackToPlaylist').attr('data-track-id'), function(data) {
		//console.log('track saved to playlist');
		//console.log(data);
		$('#saveTrackToPlaylist').modal('hide');
	});
}

function getGenres(){

	$('#loaderModal').modal();

	const url = PREFIX+'data/genres';

	$.mobile.showPageLoadingMsg ();
	$.getJSON(url, function(data) {
		$("#playlistUl").empty();

		let genreHtml = '';
		$.each(data, function(key, val) {
			genreHtml += '<input type="checkbox" name="checkbox-'+val.id+'" id="checkbox-'+val.id+'" value="'+val.id+'" />';
			genreHtml += '<label for="checkbox-'+val.id+'">'+val.name+'</label>';
		});

		$("#genreList").append(genreHtml);
		$('#genreList').trigger("create");

		$('#loaderModal').modal('hide');
	});
}

function generatePlaylist(length){


	//$('div.preloader').show();
	
    $('div.preloader').removeClass('fadeOut');
    $('div.preloader').addClass('fadeIn');

	let url = PREFIX+PATH+SYMFONYROOT+'data/playlists/generate/';
	let args = {};

	//get selected genres
	let genreSelection = new Array();
	let genreList = $("#generatorGenreList input[type=\"checkbox\"]:checked");
	$.each(genreList, function(key, val){
		//console.log($(val).attr("value"));
		genreSelection.push($(val).attr("value"));
	});
	args['genres'] = genreSelection;
	
	//get selected genreGroups
	let genreGroupSelection = new Array();
	let genreGroupList = $("#genreGroupSettings input[type=\"checkbox\"]:checked");
	$.each(genreGroupList, function(key, val){
		//console.log($(val).attr("value"));
		genreGroupSelection.push($(val).attr("value"));
	});
	//console.log(genreGroupSelection);
	args['genreGroups'] = genreGroupSelection;

	//get selected playnext style
	let playnextSetting = $("#playnextSetting input[type=\"radio\"]:checked").attr("value");
	//console.log('playnextSetting:'+playnextSetting);
	args['playnextSetting'] = playnextSetting;


	let song_playcount = $('#song_playcount').val();
	if(song_playcount>0){
		args['song_playcount']=song_playcount;
	}
	let num_songs = $('#num_songs').val();
	if(num_songs){
		args['length']=num_songs;
	}
	let song_age_max = $('#song_age_max').val();
	if(song_age_max>0){
		args['song_age_max']=song_age_max;
	}
	let song_length_max = $('#song_length_max').val();
	if(song_length_max){
		args['song_length_max']=song_length_max;
	}
	let favorites = $('#favorites').is(':checked');
	if(favorites){
		args['favorites']=favorites;
	}
	let min_rating = $('#min_rating').val();
	if(min_rating && min_rating > 0){
		args['min_rating']=min_rating;
	}
	/*
	let listeners = $('#listeners').val();
	if($('#listeners').is(':checked')){
		args['listeners']=listeners;
	}
	*/
	let serato = $('#serato').val();
	if($('#serato').is(':checked')){
		args['serato']=serato;
	}

	// scan files already in playlist and add them as an array object to be ignored by the server side
	/*
	$.each($('#playlist li.track'), function(key, val){console.log($(val).prop('id'))});
	*/
	let excludeTracks = [];
	$.each($('#playlist li.track'), function(key, val){
		excludeTracks.push( $(val).prop('id') );
	});

	$.getJSON(url+JSON.stringify(args), function(data) {
		$("#playlist tbody").empty();
		$.each(data, function(key, val) {
			addTrackToPlaylist(val, []);
		});
		calculatePlaylistLength();
		$('#libraryTabs .nav-tabs a[href=#libraryPlaylist]').tab('show');
		window.location.hash = '#libraryPlaylist';
		//$('div.preloader').hide();

		$('div.preloader').removeClass('fadeIn');
		$('div.preloader').addClass('fadeOut');
	});

}

function calculatePlaylistLength(){
	const tracksInList = $('#playlist li');
	let totalPlayLength = 0;
	$.each(tracksInList, function(trackKey, trackVal) {
		//console.log('calc playlist length: found track:');
		//console.log( $(trackVal).data('object') );
		if($(trackVal).data('object')){
			if($(trackVal).data('object').hasOwnProperty('duration')){
				totalPlayLength += $(trackVal).data('object').duration;
			}
		}
	});
	$('.footer .duration').text(secToMin(totalPlayLength));
	//console.log("PLAYLISTLENGTH: "+secToMin(totalPlayLength));
}


function generateNextTracks(el)
{
	//console.log(el.data('id'));
	const track_id = el.data('id');
	const el_index = el.index();	
	const el_guid = 'el-'+guid();
	$( '<li class="loadTest" id="'+el_guid+'">loadin test</li>' ).insertAfter( el );
	//removeTrackFromPLaylist(el);

	$.getJSON(
		PREFIX+PATH+SYMFONYROOT+'data/playlists/nexttrack/json/'+track_id+'&trackCount=1',
		function(data){
			if(!data.error){
				$.each(
					data, 
					function(tagKey, tagVal){
						//console.log(tagVal.track);
						addTrackToPlaylist(tagVal, {insertAt:(el_index+tagKey+1)});
					}
				);
			}else{
				console.log('track was rare');
			}
			//$('#libraryPlaylists').append(playlistList);
		}
	)
	.fail( () => {
		console.log( "error loading json" );
		$("#" + el_guid ).remove();
	})
	.always( () => {
		console.log( "complete, removing loader element" );
		$("#" + el_guid ).remove();
	});

}

function updateQueuePlaylist(){
	//emptyPlaylist();

	//alert($('#playlistname').attr('data-playlist-id'));
	//console.log($('#playlist li').attr('id'));
	let newTracks = [];
	$.each(
		$('#playlist li'), 
		function(tagKey, elVal){
			//console.log($(elVal).attr('id'));
			newTracks.push($(elVal).attr('id'));
		}	
	);
	//console.log(newTracks);
	
	$.getJSON(
		PREFIX+PATH+SYMFONYROOT+'data/playlists/queue/update/'+JSON.stringify(newTracks),
		function(data){
			//console.log(data);
			//alert('Playlist Queue saved.');
		}
	);
}