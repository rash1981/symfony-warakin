		$(() => {	
			//enable search autocomplete

			function split( val ) {
			  return val.split( /,\s*/ );
			}
			function extractLast( term ) {
			  return split( term ).pop();
			}

            let searchManEl = $( "#trackManagerSearch" );
            if(searchManEl){
			  // don't navigate away from the field on tab when selecting an item
			  	searchManEl.bind( "keydown", function( event ) {
			        if ( event.keyCode === $.ui.keyCode.TAB &&
			            $( this ).data( "ui-autocomplete" ).menu.active ) {
			          	event.preventDefault();
			        }
			  	})
			  	.autocomplete({
			        source: function( request, response ) {
						$.getJSON( PATH+SYMFONYROOT+"data/search/"+extractLast( request.term ), {
							term: extractLast( request.term )
						}, response );
			        },
			        search: function() {
			          	// custom minLength
			          	let term = extractLast( this.value );
				  		//console.log('added spinner');
				  		$(this).addClass('loading-spinner');
			          	if ( term.length < 2 ) {
			            	return false;
			          	}
			        },
			        focus: function() {
			          // prevent value inserted on focus
			          return false;
			        },
			        select: function( event, ui ) {
						let terms = split( this.value );
						// remove the current input
						terms.pop();
						// add the selected item
						terms.push( ui.item.value );
						// add placeholder to get the comma-and-space at the end
						terms.push( "" );
						//this.value = terms.join( ", " );
						//console.log(ui.item.value);

						if (ui.item.id.substring(0, 5) == 'track') {
							console.log('clicked track');
                            console.log(ui.item.value);
                            document.location.href=PATH+SYMFONYROOT+"/player/track/"+ui.item.value.id;
						}else if (ui.item.id.substring(0, 6) == 'artist') {
							//console.log('clicked artist');
							$('#libraryTabs .nav-tabs a[href=#libraryArtist]').tab('show');
							window.location.hash = '#libraryArtist';
							loadArtist('data/artists/'+ui.item.id.substring(7));
						}else if (ui.item.id.substring(0, 5) == 'album') {
							//console.log('clicked album');
							//console.log(ui.item.id.substring(6));
							$('#libraryTabs .nav-tabs a[href=#libraryTracks]').tab('show');
							window.location.hash = '#libraryTracks';
							$('#libraryTracks').empty();
							loadAlbum(ui.item.id.substring(6), '#libraryTracks');
						}

						return false;
			        }
			  	}).focus( function(){
				    $(this).autocomplete("search", this.value);
				})
				.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
					let icon ='';
					let postfix = '';
					if (item.id.substring(0, 5) == 'track') {
						icon += '<i class="fa fa-music"></i> ';
						if (item.value.statistics) {
							if (item.value.statistics.rating != null && item.value.statistics.rating != 0) {
								postfix += '<div class="rating_display" style="display:inline_block; float:right; height:20px; overflow:hidden;">';
								for(let i=0; i<5; i++ ) {
									if ( (i < item.value.statistics.rating/2) && i+1 > item.value.statistics.rating/2) {
										postfix += '<img src="bundles/appsupplywarakin/img/raty/star-half.png" >&nbsp;';
									} else if (i < item.value.statistics.rating/2) {
										postfix += '<img src="bundles/appsupplywarakin/img/raty/star-on.png" >&nbsp;';
									} else  {
										postfix += '<img src="bundles/appsupplywarakin/img/raty/star-off.png" >&nbsp;';
									}
								}
								postfix += '</div>'; 
							}
						}
					}else if (item.id.substring(0, 6) == 'artist') {
						icon += '<i class="fa fa-user"></i> ';
					}else if (item.id.substring(0, 5) == 'album') {
						icon += '<i class="fa  fa-dot-circle-o"></i> ';
					}
				  	//console.log('remove spinner');
					$('.loading-spinner').removeClass('loading-spinner');
			    	return $( "<li></li>" )
			        	.data( "item.autocomplete", item )
			        	.append( "<a>" + icon + 
			        		unescape(item.label) + postfix + "</a>" )
			        	.appendTo( ul );
                };
            }
			});