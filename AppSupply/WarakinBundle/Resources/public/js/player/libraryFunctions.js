// ARTISTS PAGE

/*
	init library-artist-list
*/

function libraryCreateAlphaList(targetElementIdStr)
{

	let libraryList = $('#'+targetElementIdStr);
	libraryList.children().remove('li');


	let i;
	let libraryIndexListItem;
	let letter = '';

	
	libraryIndexListItem = '';
	libraryIndexListItem += '<li>';
	libraryIndexListItem += '	<h1>#</h1>';
	libraryIndexListItem += `	<ul id="${targetElementIdStr}"-1>`;
	libraryIndexListItem += '		<li>Test Artist';
	libraryIndexListItem += '			<ul>';
	libraryIndexListItem += '				<li>';
	libraryIndexListItem += '					<a href="">Test Album</a>';
	libraryIndexListItem += '					<ul>';
	libraryIndexListItem += '						<li><a href="">1. Test Track</a>';
	libraryIndexListItem += '					</ul>';
	libraryIndexListItem += '				</li>';
	libraryIndexListItem += '			</ul>';
	libraryIndexListItem += '		</li>';
	libraryIndexListItem += '   </ul>';
	libraryIndexListItem += '</li>';


	listAddition = $(libraryIndexListItem).filter('li');
  	libraryList.append(listAddition);
  	listAddition.bind("tap",function(){
			libraryLoadArtists(10, 0, '', targetElementIdStr+'-1');
   	});

	for(i = 1; i <= 26; i++)
	{
		letter = String.fromCharCode(64+i);
		libraryIndexListItem = '';
		libraryIndexListItem += '<li>';
		libraryIndexListItem += `		<h1>${letter}</h1>`;
		libraryIndexListItem += '    <ul id="'+targetElementIdStr+'-'+letter+'">';
		libraryIndexListItem += '        <li>Test Artist';
		libraryIndexListItem += '            <ul>';
		libraryIndexListItem += '                <li>';
		libraryIndexListItem += '                    <a href="">Test Album</a>';
		libraryIndexListItem += '                    <ul>';
		libraryIndexListItem += '                        <li><a href="">1. Test Track</a>';
		libraryIndexListItem += '                    </ul>';
		libraryIndexListItem += '                </li>';
		libraryIndexListItem += '            </ul>';
		libraryIndexListItem += '        </li>';
		libraryIndexListItem += '    </ul>';
		libraryIndexListItem += '</li>';

		//sAlpha = "<a target=" + sFrame + " href=" + sPage;
		//sAlpha += "ALPHA=" + String.fromCharCode(64+i) + ">";
		//sAlpha += String.fromCharCode(64+i) + " </a>";
		//console.log(sAlpha);

		listAddition = $(libraryIndexListItem).filter('li');
		listAddition.prop('letter', letter);
		libraryList.append(listAddition);
		
		//console.log('buidlong library interface: targetElementIdStr:'+targetElementIdStr);

	  	if(targetElementIdStr == 'library-artist-list')
	  	{
		  	listAddition.bind("tap",function(){
		  		let obj = $(this);
    			libraryLoadArtists(10, 0, obj.prop('letter')+'%', targetElementIdStr+'-'+obj.prop('letter'));
		   	});
	  	}
	  	else if(targetElementIdStr == 'library-album-list')
	  	{
		  	listAddition.bind("tap",function(){
		  		let obj = $(this);
    			libraryLoadAlbums(10, 0, obj.prop('letter')+'%', targetElementIdStr+'-'+obj.prop('letter'));
		   	});
	  	}

	}
	
	libraryList.listview("refresh");
}

function loadArtists(artist, length, offset)
{
	let artistList = $('#artistList');

	$('#artistFavorite').css('display', 'none');
	$('#artistNew').css('display', 'none');
	$('#artistRandom').css('display', 'none');

	artistList.find('.span4').empty();

	if(!offset && offset !== 0)
	{
		offset = parseInt(artistList.data('offset'));
	}
//	$('.ui-page .ui-body-a').remove();
	let url = PREFIX+PATH+SYMFONYROOT+'data/artists?length='+parseInt(length)+'&offset='+offset;

	if(artist)
	{
		//console.log("query found");
		url = PREFIX+PATH+SYMFONYROOT+'data/artistQuery/'+encodeURI(artist)+'?length='+parseInt(length)+'&offset='+offset;
		artistList.data('letter', artist);
	}
	//console.log('open json : '+url);

	$.getJSON(url, function(data)
	{	
		//console.log('data.artists.length:'+data.artists.length);  	
		//console.log('offset:'+offset);
		//console.log('length:'+length);
		if(data.artists.length == length)
		{
			artistList.data('offset', offset+length);
		}
		let i = 0;
		$.each(data.artists, function(key, val)
		{
			if( key / libraryListColumnLength == Math.round(key / libraryListColumnLength) )
			{
				i++;
			}
			//console.log(val);
		  	let artist = `<a href="#libraryArtist"  data-toggle="tab">${val.name} (${val.songCount})</a><br/>`;
			artist=$(artist);
			artist.data('url', val.dataUrl);				
			artist.data('id', val.id);
			artist.click(function() {
			 	loadArtist($(this).data('url'));
			});		  	
			$('#artistList'+i).append(artist);
		});
	});
}


function loadArtist(artistUrl)
{
	$('#loaderModal').modal();
	//console.log('loadArtist, url: '+artistUrl);
	artistTab = $('#libraryArtist');
	$.getJSON(
		PREFIX+PATH+SYMFONYROOT+artistUrl, 
		function(data){

			artistTab.find('#artistAlbumTrackList').empty();
			artistTab.find('#artistAlbumList ul').empty();

			$.each(data.albums, function(albumKey, albumVal){
				console.log(albumVal);
				let albumHtml = $(`<li><a href="#libraryTracks" data-toggle="tab" data-album-id="${albumVal.id}">${albumVal.name}</a></li>`);
				albumHtml.find('a').click( function() {
					loadAlbum($(this).data('album-id'), '#libraryTracks');
				});
				artistTab.find('#artistAlbumList ul').append(albumHtml);
				loadAlbum(albumVal.id, '#artistAlbumTrackList');
			});
			loadArtistSingles(data.id,'#artistAlbumTrackList');
			artistTab.find('p.wiki').html(data.biography);
			artistTab.find('h2').html(data.name);
			//console.log(data.images);
			artistTab.find('img').attr('src', data.images.extralarge);

			$('#loaderModal').modal('hide');
			//console.log('finshed loading artist json');
		}
	);
	//console.log('finshed loadArtist function');
	//$('#libraryTabs #libraryArtist').tab('show');
}

function loadArtistSingles(artistId, targetSelector)
{
	$('#loaderModal').modal();
	
	let url = PREFIX+PATH+SYMFONYROOT+'data/artists/'+artistId+'/singles';
	
	$.getJSON(url, function(data) {
		//console.log('loaded album data');
		//console.log(data);
		data.id = artistId;
		if(data.tracks.length > 0){
			constructAlbumHtml(data, targetSelector);
		}
		$('#loaderModal').modal('hide');
	});
}

function loadGenres()
{
	let genreList = $('#genreList');
	let generatorGenreList = $('#generatorGenreList');
	let genreEditor = $("#templates .track span.genre select")

	genreList.find('.span3').empty();

	let offset = parseInt(genreList.data('offset'));

	//console.log("query found");
	let url = PREFIX+PATH+SYMFONYROOT+'data/genres/?length='+parseInt(length)+'&offset='+offset;
	
	//console.log('open json : '+url);
	$.getJSON(url, function(data)
	{	
		//console.log('data.genres.length:'+data.genres.length);  	
		//console.log('offset:'+offset);
		//console.log('length:'+length);
		if(data.genres.length == length)
		{
			genreList.data('offset', offset+length);
		}
		/*if(.children().length > 0)
		{

		}*/
		let i = 0;
		$.each(data.genres, function(key, val)
		{
			if( key / libraryListColumnLength == Math.round(key / libraryListColumnLength) )
			{
				i++;
			}

			/**
			 *	Add genre to Genre library tab
			 */
		  	let genre = `  	<a href="#libraryGenre" data-toggle="tab">${val.name} (${val.tagsCount})</a><br/>`;
			genre=$(genre);
			genre.data('url', val.dataUrl)
			$('#genreList'+i).append(genre);

		  	genre.click( function(){
				loadGenre($(this).data('url'));
		   	});

			/**
			 *	Add genre to Generator library tab
			 */
			 generatorGenreList.append('<p><label class="checkbox"><input type="checkbox" name="genre[]" value="'+val.id+'" /> '+val.name+'</label></p>');
			/**
			 * 	Add genre to genre selection dropdown in main playlist
			 */
			genreEditor.append('<option value="'+val.id+'" > '+val.name+'</option>');
		});
	});

	genreEditor.change( function(){
		let optionSelected = $("option:selected", this);
		let valueSelected = this.value;
		//console.log('set genre to: '+valueSelected);
	});
}

function loadPlaylists(){
	let playlistsTab = $('#libraryPlaylists');
	$.getJSON(
		PREFIX+PATH+SYMFONYROOT+'data/playlists/',
		function(data){
			//console.log(data);
			let playlistList = $('<ul class="new-style"></ul>');
			let playlistEditorList = $('<ol></ol>');
			$.each(
				data, 
				function(playlistKey, playlistVal){
					//console.log(playlistVal);
					let genreList = '';
					if(playlistVal.hasOwnProperty('playlistGenres')){
						$.each(playlistVal.playlistGenres, function(genreKey, genreVal){
							if(genreKey > 3){
								return false;
							}
							genreList += genreVal.name+', ';
						});
					}
					
					playlistList.append('<li><h3>'+playlistVal.name+' ('+playlistVal.tagcount+')</h3>'+
						'<span>Mostly '+genreList+'</span>'+
						'<span class="align-right"><a href="javascript:loadPlaylist(\''+playlistVal.id+'\')" class="btn"><i class="fa fa-play-circle"></i> Play</a> '+
						'<a href="'+PREFIX+PATH+SYMFONYROOT+'data/playlists/'+playlistVal.id+'/m3u" target="_blank" class="btn"><i class="fa fa-list-alt"></i> Download Streamable Playlist</a> '+
						'<a href="'+PREFIX+PATH+SYMFONYROOT+'data/playlists/'+playlistVal.id+'/zip" target="_blank" class="btn"><i class="fa fa-cloud-download"></i> Download Tracks</a><span>'+
						'</li>');
					playlistsModalListItem  = $('<li><a href="javascript:saveTrackToPlaylist(\''+playlistVal.id+'\')" >'+playlistVal.name+'</a></li>');
					playlistsModalListItem.data('playlistId', playlistVal.id);
					playlistEditorList.append(playlistsModalListItem);
				}
			);
			$('#libraryPlaylists').html(playlistList);
			//$('#saveTrackToPlaylist .modal-body').html( playlistEditorList.clone().wrap('<ol/>').parent().html() );
			$('#saveTrackToPlaylist .modal-body').append(playlistEditorList);
		}
	);
}

function loadPlaylist(playlistId){
	//emptyPlaylist();
	$.getJSON(
		PREFIX+PATH+SYMFONYROOT+'data/playlists/'+playlistId,
		function(data){
			//console.log(data);
			$('#playlistname').text(data.name);
			$('#playlistname').attr('data-playlist-id', data.id);
			$.each(
				data['playlist_tracks'], 
				function(tagKey, tagVal){
					//console.log(tagVal.track);
					addTrackToPlaylist(tagVal.track, []);
				}
			);
			//$('#libraryPlaylists').append(playlistList);
			$('#libraryTabs .nav-tabs a[href=#libraryPlaylist]').tab('show');
            window.location.hash = '#libraryPlaylist';
		}
	);
}

function updatePlaylist(){
	//emptyPlaylist();

	if($('#playlistname').attr('data-playlist-id')){
		//alert($('#playlistname').attr('data-playlist-id'));
		//console.log($('#playlist li').attr('id'));
		let newTracks = [];
		$.each(
			$('#playlist li'), 
			function(tagKey, elVal){
				//console.log($(elVal).attr('id'));
				newTracks.push($(elVal).attr('id'));
			}	
		);
		//console.log(newTracks);
		
		$.getJSON(
			PREFIX+PATH+SYMFONYROOT+'data/playlists/'+$('#playlistname').attr('data-playlist-id')+'/update/'+JSON.stringify(newTracks),
			function(data){
				//console.log(data);
				alert('Playlist "'+$('#playlistname').text()+' '+$('#playlistname').attr('data-playlist-id')+'" saved.');
			}
		);
	}
}


	

function loadGenre(genreUrl)
{
	$('#loaderModal').modal();
	genreTab = $('#libraryTabs');
	$.getJSON(
		PREFIX+PATH+SYMFONYROOT+genreUrl, 
		function(data){
			genreTab.find('#libraryGenre img').attr('src', PREFIX+PATH+'bundles/appsupplywarakin/img/icons/genre/'+ encodeURIComponent(data.name.toLowerCase())+'.jpg');		
			genreTab.find('#libraryGenre img').attr("alt", data.name);


			genreTab.find('#genreArtistList ul').empty();
			$.each(data.artists, function(artistKey, artistVal){
				//console.log(artistVal);
				//console.log('dataUrl:'+artistVal.dataUrl );
				let artist = $('<li><a href="#libraryArtist" data-toggle="tab">'+artistVal.name+'</a></li>');
				artist=$(artist);
				let artistA = artist.find('a');
				artistA.data('url', artistVal.dataUrl);				
				artistA.data('id', artistVal.id);
				artistA.click( function() {
				 	loadArtist($(this).data('url'));
				});
				genreTab.find('#genreArtistList ul').append(artist);
			});	

			genreTab.find('#genreAlbumList ul').empty();
			$.each(data.albums, function(albumKey, albumVal){
				//console.log(albumVal);
				let albumHtml = $('<li><a href="#libraryTracks" data-toggle="tab" data-album-id="'+albumVal.id+'">'+albumVal.name+'</a></li>');
				albumHtml.find('a').click( () => {
					loadAlbum(albumVal.id);
				});
				genreTab.find('#genreAlbumList ul').append(albumHtml);
			});

			let genreName = encodeURIComponent(capitaliseFirstLetter(data.name.toLowerCase()));		
			loadGenreWikipedia(genreName);
		}
	);
	//$('#libraryTabs #libraryGenre').tab('show');
}



function loadGenreWikipedia(genreName, disregardAmbiguation){
	if(disregardAmbiguation!=true){
		disregardAmbiguation=false;
	}
	//console.log('loadGenreWikipedia: '+genreName+', '+disregardAmbiguation);
	$.getJSON(
		'http://en.wikipedia.org/w/api.php?format=json&action=query&prop=revisions&titles='+genreName+'&rvprop=content&rvsection=0&rvparse&callback=?',
		function(data){
			//console.log('test');
			//console.log(data.query.pages);
			$.each(data.query.pages, function(key, val){
				if(val.revisions){
					$.each(val.revisions[0], function(htmlIndex, htmlVal){
						//console.log(htmlVal);
						//find('ol li').
						//console.log(htmlEl);
						let li = $(htmlVal);
						li = li.find('li a');
						//console.log( li );
						let redirect = false;
						$.each(li, function(liIndex, liHtml){
							redirect = $(liHtml).html();
						});

						let disambiguation = false;
						if(disregardAmbiguation){
						}
						else
						{
							if( htmlVal.indexOf( '"'+genreName+' (disambiguation)"' ) > -1){
								disambiguation = true;
							}
						}
						
						if(redirect)
						{
							loadGenreWikipedia(redirect, true);
						}
						else if(disambiguation)
						{
							loadGenreWikipediaDisambiguation(genreName);
						}
						else
						{
							genreTab.find('#libraryGenre p.wiki').html(htmlVal);
							$('#loaderModal').modal('hide');
						}
					});
				}
			});
		}
	);
}

function loadGenreWikipediaDisambiguation(genreName){
	//console.log('loadGenreWikipediaDisambiguation: '+genreName);
	$.getJSON(
		'http://en.wikipedia.org/w/api.php?action=query&prop=links&format=json&titles='+genreName+'_(disambiguation)&callback=?',
		function(data){
			$.each(data.query.pages, function(pagesIndex, pagesVal){
				//console.log(pagesVal)
				$.each(pagesVal.links, function(linksIndex, linksVal){
					let title = linksVal.title;
					if( title.indexOf('music') > 0){
						//console.log(title);
						loadGenreWikipedia(title, true);
						$('#loaderModal').modal('hide');
					}

				});
			});
	}); 
}

function loadAlbum(album_id, targetSelector){
	$(targetSelector).empty();
	loadAlbumData(album_id, constructAlbumPage, targetSelector);
}


function loadAlbumData(album_id, callbackFunc, targetSelector){
	//console.log('loadTracks called. list length: '+$("#album-"+album_id).children().length);
	//if( $("#album-"+album_id).children().length==2 ){
	$('#loaderModal').modal();

	//console.log(album_id);

	//alert('loading albums for '+artist_name);
	let url = PREFIX+PATH+SYMFONYROOT+'data/albums/'+encodeURI(album_id);
	//console.log('open json : '+url);



	$.getJSON(url, function(data) {
		//console.log('loaded album data');
		//console.log(data);
		data.id = album_id;
		callbackFunc(data, targetSelector);

		$('#loaderModal').modal('hide');
	});
}

function constructAlbumPage(data, targetSelector){
	constructAlbumHtml(data, targetSelector);
}

function constructAlbumView(data, targetSelector){
	constructAlbumHtml(data, targetSelector);
}


function constructAlbumHtml(data, targetSelector){
	let albumList = $(targetSelector);
	let albumContainer = 
			$('<div class="albumContainer">'+
			'	<div class="coverContainer">'+
			'		<img src="'+data.cover+'" height="240" width="240" class="cover">'+
			'		<div class="btn">add "'+data.title+'" to playlist</div>'+
			'	</div>'+
			'	<div class="trackContainer">'+
			'		<h1><span class="artistName">'+data.artistName+' - </span>'+data.title+'</h1>'+
			'		<ul id="trackList-'+data.id+'" class="album playlist new-style"></ul>'+
			'	</div>'+
			'</div>');
			
	albumList.append(albumContainer);
	//console.log("album id:"+data.id);
	albumContainer.find('.btn').on('click', function(){
		addAlbumToPlaylist(data.id)
	});
 	
  $.each(data.tracks, function(key, trackObj) {
		console.log('create albumlist  trackEl:'+data.id);
		addTrackToList(trackObj, '#trackList-'+data.id, addTrackToPlaylist);
 	});
}

function addAlbumToPlaylist(album_id){
	loadAlbumData(album_id, insertAlbumTracksIntoPlaylist);
}


function insertAlbumTracksIntoPlaylist(data){
	console.log('insertAlbumTracksIntoPlaylist() called. Data:');
	console.log(data);
	let playlist = $('#playlist');
  $.each(data.tracks, function(key, val) {
	  	addTrackToPlaylist(val, []);
 	});
}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}
