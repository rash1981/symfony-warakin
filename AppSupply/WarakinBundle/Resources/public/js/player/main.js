
const HOST='81.71.93.174';
const PORT='668';
const PREFIX = '';//'http://'+HOST+':'+PORT;


const PATH='';
const SYMFONYROOT = 'app_dev.php/'
const SEARCHTIMEOUT = 0;

const PLAYLISTSETTINGS = {
                        'repeat':false,
                        'autonext':true,
                        'shuffle':false
                    };


let PLAYLISTINDEX = 1;

const libraryListColumnLength = 24;

String.prototype.hashCode = function(){
    let hash = 0, i, char;
    if (this.length == 0) return hash;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
};

$( () => {
    // pre load things

    // initialize media session object for mobile

    if ('mediaSession' in navigator) {

        navigator.mediaSession.metadata = new MediaMetadata({
            title: '',
            artist: '',
            album: ''/*,
            artwork: [
            { src: 'https://dummyimage.com/96x96',   sizes: '96x96',   type: 'image/png' },
            { src: 'https://dummyimage.com/128x128', sizes: '128x128', type: 'image/png' },
            { src: 'https://dummyimage.com/192x192', sizes: '192x192', type: 'image/png' },
            { src: 'https://dummyimage.com/256x256', sizes: '256x256', type: 'image/png' },
            { src: 'https://dummyimage.com/384x384', sizes: '384x384', type: 'image/png' },
            { src: 'https://dummyimage.com/512x512', sizes: '512x512', type: 'image/png' },
            ]*/
        });
        
        navigator.mediaSession.setActionHandler('play', function() { pauseAudio() });
        navigator.mediaSession.setActionHandler('pause', function() { pauseAudio() });
        navigator.mediaSession.setActionHandler('seekbackward', function() {});
        navigator.mediaSession.setActionHandler('seekforward', function() {});
        navigator.mediaSession.setActionHandler('previoustrack', function() {});
        navigator.mediaSession.setActionHandler('nexttrack', function() {nextAudio();});
    }    

    //generatePlaylist();
    loadGenres();

    loadPlaylist(210);

    $("#playButton").click( () => {
        // Note: two ways to access media file: web and local file
        // let src = PREFIX+'/MP3.NEW/Vaski%20-%20Zombie%20Apocalypse.mp3';
        // let src = PREFIX+'/MP3.VA/VA/VA%20-%2019%20Zomermelodieen/16%20-%20PRODIGY%20-%20Firestarter.mp3';

        // local (on device): copy file to project's /assets folder:
        // let src = '/android_asset/spittinggames.m4a';

        console.log(PLAYLISTINDEX);

	if(my_media){
		// if active track is pasued, play track
		my_media.play();
	} else {
		// else play track by playlistindex
        playTrack( $('#playlist li:nth-child('+PLAYLISTINDEX+')').attr('id') );
	}
	is_stopped = false;
	is_paused = false;

    	// update playbutton states
    	$('#playButton').css('display', 'none');
    	$('#pauseButton').css('display', 'inline-block');
    });

    $("#pauseButton").click( () => {
        pauseAudio();
    });
    
    $("#forwardButton").click( () => {
        nextAudio();
    });


    $("#playerSlider").css("display","none");

    // initiate main menu
    $("#libraryArtists").click( () => {
        //openLibraryArtistList();
    });

    $("#libraryGenresBtn").click( () => {
        //console.log('genres');
    });

    loadPlaylists();

    //$("#libraryArtistsBtn").click( () => {  

        $("#libraryArtistsA").click( () => {
            loadArtists('A%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsB").click( () => {
            loadArtists('B%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsC").click( () => {
            loadArtists('C%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsD").click( () => {
            loadArtists('D%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsE").click( () => {
            loadArtists('E%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsF").click( () => {
            loadArtists('F%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsG").click( () => {
            loadArtists('G%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsH").click( () => {
            loadArtists('H%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsI").click( () => {
            loadArtists('I%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsJ").click( () => {
            loadArtists('J%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsK").click( () => {
            loadArtists('K%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsL").click( () => {
            loadArtists('L%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsM").click( () => {
            loadArtists('M%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsN").click( () => {
            loadArtists('N%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsO").click( () => {
            loadArtists('O%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsP").click( () => {
            loadArtists('P%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsQ").click( () => {
            loadArtists('Q%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsR").click( () => {
            loadArtists('R%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsS").click( () => {
            loadArtists('S%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsT").click( () => {
            loadArtists('T%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsU").click( () => {
            loadArtists('U%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsV").click( () => {
            loadArtists('V%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsW").click( () => {
            loadArtists('W%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsX").click( () => {
            loadArtists('X%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsY").click( () => {
            loadArtists('Y%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsZ").click( () => {
            loadArtists('Z%', 3*libraryListColumnLength, 0);
        });

        $('#libraryArtists .pager .next').click( () => {
            loadArtists($('#artistList').data('letter'), 3*libraryListColumnLength);
        });
        $('#libraryArtists .pager .previous').click( () => {
            loadArtists(
                $('#artistList').data('letter'), 3*libraryListColumnLength, parseInt($('#artistList').data('offset'))-2*3*libraryListColumnLength
            );
        });
    //});


    $('#generatePlaylistButton').click( () => {
        generatePlaylist(40);
    });

    $('#progressBarContainer').on('click', function(e){

        let progressBarCoords = $('#progressBarContainer').offset();
        let progressBarSize = $('#progressBarContainer').width();

        let percentage = ( (e.pageX - progressBarCoords.left) / progressBarSize );
        console.log('skip to precentage: '+  percentage);

        seekAudio(percentage);

    });

    //$('div.preloader').hide();
    $('div.preloader').addClass('fadeOut');

});



function htmlEncode(value){
    if (value) {
        return jQuery('<div />').text(value).html();
    } else {
        return '';
    }
}

function htmlDecode(value) {
    if (value) {
        return $('<div />').html(value).text();
    } else {
        return '';
    }
}




/*
$('#playlists').live("pageinit", function(){
    $("#generatePlaylist").live('tap', function() {
        generatePlaylist();
    });
    getGenres();
});


$('#localtracks').live("pageinit", function(){

   populate(currentPath);

});
*/