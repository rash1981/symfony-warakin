
//var HOST='83.84.149.26';
var HOST='81.71.93.174';
var PORT='668';
var PREFIX = '';//'http://'+HOST+':'+PORT;


var PATH='/';
var SYMFONYROOT = ''
var SEARCHTIMEOUT = 0;

var PLAYLISTSETTINGS = {
                        'repeat':false,
                        'autonext':true,
                        'shuffle':false
                    };


var PLAYLISTINDEX = 1;

var libraryListColumnLength = 24;

String.prototype.hashCode = function(){
    var hash = 0, i, char;
    if (this.length == 0) return hash;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
};

$(function(){
    // pre load things

    //generatePlaylist();
    loadGenres();

    $("#playButton").click(function() {
        //var src = 'ftp://rash1981:xTR4_spEc@83.84.149.26:21/MP3.NEW/Xilent%20-%20Choose%20Me.mp3';
        //loadTrack(src);

        // Note: two ways to access media file: web and local file
        //var src = PREFIX+'/MP3.NEW/Vaski%20-%20Zombie%20Apocalypse.mp3';
        //var src = PREFIX+'/MP3.VA/VA/VA%20-%2019%20Zomermelodieen/16%20-%20PRODIGY%20-%20Firestarter.mp3';

        // local (on device): copy file to project's /assets folder:
        //var src = '/android_asset/spittinggames.m4a';
        console.log(PLAYLISTINDEX);

	if(document.my_media){
		// if active track is pasued, play track
		document.my_media.play();
	} else {
		// else play track by playlistindex
        	playTrack( $('#playlist li:nth-child('+PLAYLISTINDEX+')').attr('id') );
	}
	is_stopped = false;
	is_paused = false;

    	// update playbutton states
    	$('#playButton').css('display', 'none');
    	$('#pauseButton').css('display', 'inline-block');
    });

    $("#pauseButton").click(function() {
        pauseAudio();
    });
    
    $("#forwardButton").click(function() {
        nextAudio();
    });


    $("#playerSlider").css("display","none");

    // initiate main menu
    $("#libraryArtists").click(function(){
        //openLibraryArtistList();
    });

    $("#libraryGenresBtn").click(function(){
        console.log('genres');
    });

    loadPlaylists();

    //$("#libraryArtistsBtn").click(function(){  

        $("#libraryArtistsA").click(function(){
            loadArtists('A%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsB").click(function(){
            loadArtists('B%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsC").click(function(){
            loadArtists('C%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsD").click(function(){
            loadArtists('D%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsE").click(function(){
            loadArtists('E%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsF").click(function(){
            loadArtists('F%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsG").click(function(){
            loadArtists('G%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsH").click(function(){
            loadArtists('H%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsI").click(function(){
            loadArtists('I%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsJ").click(function(){
            loadArtists('J%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsK").click(function(){
            loadArtists('K%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsL").click(function(){
            loadArtists('L%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsM").click(function(){
            loadArtists('M%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsN").click(function(){
            loadArtists('N%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsO").click(function(){
            loadArtists('O%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsP").click(function(){
            loadArtists('P%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsQ").click(function(){
            loadArtists('Q%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsR").click(function(){
            loadArtists('R%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsS").click(function(){
            loadArtists('S%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsT").click(function(){
            loadArtists('T%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsU").click(function(){
            loadArtists('U%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsV").click(function(){
            loadArtists('V%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsW").click(function(){
            loadArtists('W%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsX").click(function(){
            loadArtists('X%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsY").click(function(){
            loadArtists('Y%', 3*libraryListColumnLength, 0);
        });
        $("#libraryArtistsZ").click(function(){
            loadArtists('Z%', 3*libraryListColumnLength, 0);
        });

        $('#libraryArtists .pager .next').click(function(){
            loadArtists($('#artistList').data('letter'), 3*libraryListColumnLength);
        });
        $('#libraryArtists .pager .previous').click(function(){
            loadArtists(
                $('#artistList').data('letter'), 3*libraryListColumnLength, parseInt($('#artistList').data('offset'))-2*3*libraryListColumnLength
            );
        });
    //});


    $('#generatePlaylistButton').click(function(){
        generatePlaylist(40);
    });

    /*
    if(!window.PhoneGap){
    	$( ".sortable" ).sortable({
    		containment: '#playlistUl',
    		axis: 'y',
            start: function (event, ui) {
               //var currPos1 = ui.item.index();
               ui.item.css("color", "white");
            },
            stop:  function (event, ui) {
                var delay = function() { ui.item.css({"color":""}); };
                setTimeout(delay, 12000);
            }
    	}),

    	$( ".sortable" ).disableSelection();
    }
    */

    $('#progressBarContainer').on('click', function(e){

        var progressBarCoords = $('#progressBarContainer').offset();
        var progressBarSize = $('#progressBarContainer').width();

        var percentage = ( (e.pageX - progressBarCoords.left) / progressBarSize );
        console.log('skip to precentage: '+  percentage);

        seekAudio(percentage);

    });

    $('div.preloader').hide();

});



function htmlEncode(value){
    if (value) {
        return jQuery('<div />').text(value).html();
    } else {
        return '';
    }
}

function htmlDecode(value) {
    if (value) {
        return $('<div />').html(value).text();
    } else {
        return '';
    }
}




/*
$('#playlists').live("pageinit", function(){
    $("#generatePlaylist").live('tap', function() {
        generatePlaylist();
    });
    getGenres();
});


$('#localtracks').live("pageinit", function(){

   populate(currentPath);

});
*/