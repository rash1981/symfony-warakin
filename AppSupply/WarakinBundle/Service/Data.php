<?php

namespace AppSupply\WarakinBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use BinaryThinking\LastfmBundle\Lastfm\Client\Method\ArtistMethodsClient;
use BinaryThinking\LastfmBundle\Lastfm\Client\Method\TrackMethodsClient;
use Symfony\Component\HttpFoundation\Response;

class Data
{
	private $em;
	private $lastFmArtistClient;
	private $lastFmTrackClient;

    public function __construct(EntityManager $em, ArtistMethodsClient $lastFmArtistClient, TrackMethodsClient $lastFmTrackClient)
    {
        $this->em = $em;
        $this->lastFmArtistClient = $lastFmArtistClient;
        $this->lastFmTrackClient = $lastFmTrackClient;
    }

    public function getArtists()
    {
    	$artistsCollection = $this->em
		               ->getRepository('AppSupplyWarakinBundle:Artist')
		               ->findBy(array(), array('name'=>'asc'));
		$artists = array();

		foreach($artistsCollection as $index=>$artist){
			$artists[]=$artist->getShort();
		}

		return array(
			'artists'=>$artists
		);
    }

    public function getArtist($artistIdentifier)
    {
    	if(is_numeric($artistIdentifier))
    	{
			$artist = $this->em
		               ->getRepository('AppSupplyWarakinBundle:Artist')
		               ->findOneById($artistIdentifier);

    	}
    	else
    	{
    		$artist = $this->em
		               ->getRepository('AppSupplyWarakinBundle:Artist')
		               ->findOneByName($artistIdentifier);
    	}
    	if($artist){
			$albums = array();
			foreach($artist->getAlbums() as $index=>$album){
				$albumShort = $album->getShort();
				$albumShort['artistId'] = $artist->getId();
				$albumShort['artistName'] = $artist->getName();
				$albums[]= $albumShort;
			}
			usort($albums, function($a, $b) {
				return $a['year'] - $b['year'];
			});
			// $albums[]=array(
			// 	'coverUrl'=>"/app_dev.php/data/albums/3078/cover",
			// 	'dataUrl'=>"/app_dev.php/data/artists/".$artistIdentifier.'/singles',
			// 	'id'=>0,
			// 	'name'=>"Singles",
			// 	'year'=>false
			// );
			try {
				$artistLastfm = $this->lastFmArtistClient->getInfo($artist->getName());
			}  catch (\Exception $e) {
				// artist not found on last fm
				$artistLastfm = false;
				//var_dump($e);
			} finally {
				if($artistLastfm){
					$artistLastfmImages = $artistLastfm->getImages();
					$artistLastfmBio = $artistLastfm->getBio();
				} else {
					$artistLastfmImages = '';
					$artistLastfmBio = ['content'=>''];
				}
			}
			$relatedArtists = [];
			$relatedLastFmArtists = $artist->getRelatedLastFmArtists();
			foreach($relatedLastFmArtists as $lfmArtist){
				$relatedArtists[] = $lfmArtist->getShort();
			}

			$relatedSpotifyArtists = $artist->getRelatedSpotifyArtists();
			foreach($relatedSpotifyArtists as $spotifyArtist){
				$found = false;
				foreach($relatedArtists as $relArtist){
					if($relArtist['id'] === $spotifyArtist->getId()){
						$found = true;
					}
				}
				if(!$found){
					$relatedArtists[] = $spotifyArtist->getShort();
				}
			}

			return array(
					'id'=>$artist->getId(),
					'name'=>$artist->getName(),
					'images'=>$artistLastfmImages,
					'albums'=>$albums,
					'relatedArtists'=>$relatedArtists, 
					//'relatedArtistsSpotify'=>$artist->getRelatedSpotifyArtists(),
					'biography'=>
					nl2br(str_replace(
						'User-contributed text is available under the Creative Commons By-SA License; additional terms may apply.',
						'',
							$artistLastfmBio['content']
						)
					)
				);
		} else {
			return false;
		}

    }

	public function getArtistAlbum($artistName, $albumName)
	{
		$albums = $this->em
		               ->getRepository('AppSupplyWarakinBundle:Album')
		               ->findBy(
						    array('name' => $albumName)
						);


		$match=false;
		foreach($albums as $album)
		{
			//var_dump($album->getName());
			$albumTracks = $album->getTags();
			foreach($albumTracks as $index=>$track){
				//echo $track->getArtist()->getName().' - '.$track->getAlbum()->getName().' - '.$track->getTitle();
				if($match==false){
					if($track->getArtist()->getName() == $artistName){
						$match = true;
						$artistAlbum = $album;
						$artist = $track->getArtist();
						$artistAlbumTracks = $albumTracks->toArray();
						$albumTrack = $track;
					}
				}
			}
		}

		if($match == false ){
			echo 'failed to find artist/album '.$artistName.' - '.$albumName.' <br/>';
			return false;
		}else{
			return array(
				'artist'=>$artist->getShort(),
				'album'=>$artistAlbum
			);
		}
	}

	public function getArtistQuery($query)
	{
    	$request = Request::createFromGlobals();

		$artistsQ = $this->em->createQueryBuilder()
			->select(
				array(
					'distinct a.id',
					'a.name',
					'count(t) as songCount',
					'CONCAT(\'data/artists/\', a.id) as dataUrl'
				)
			)
			->from('AppSupplyWarakinBundle:Artist', 'a')
			->innerJoin('a.tags','t')
			->where('a.name LIKE :queryStr')
			->setParameter(':queryStr', $query)
			->groupBy('a.id')
			->orderBy('a.name', 'ASC');

		if($request->query->get('offset'))
		{
			$artistsQ->setFirstResult( $request->query->get('offset') );
		}
		if($request->query->get('length'))
		{
			$artistsQ->setMaxResults( $request->query->get('length') );
		}

		$artistsResult = $artistsQ->getQuery()
				->setHydrationMode(\Doctrine\ORM\AbstractQuery::HYDRATE_SINGLE_SCALAR)
				->getResult();

		return array(
			'artists'=>$artistsResult
		);
	}

    public function getNewArtists()
    {

	    $query = $this->em->createQuery(
				'SELECT
					a,
				 	COUNT(t.id) AS trackCount
				 FROM AppSupplyWarakinBundle:Artist a
				 	JOIN a.tags t
				 	JOIN t.statistics s
				 GROUP BY a.id
				 HAVING trackCount > 4
				 ORDER BY s.createdate desc')->setMaxResults(16);

		$newArtistsResult = $query->getResult();

		foreach($newArtistsResult as $index=>$artist){
			$newArtist=$artist[0]->getShort();

			try {
				$artistLastfm = $this->lastFmArtistClient->getInfo($artist[0]->getName());
			} catch (\Exception $e) {
				$artistLastfm = null;
			}

			$defaultImages = array('extralarge'=>'defaultimg');

			$newArtist['images'] = null;

			if($artistLastfm){
				$images = $artistLastfm->getImages();
				if($images){
					$newArtist['images'] = $images;
				}
			}
			$newArtists[] = $newArtist;
		}

		return $newArtists;
	}


	public function getPlaylists(){
		ini_set('memory_limit', '2048M');
		
		$playlists = $this->em
			->getRepository('AppSupplyWarakinBundle:Playlists')
			->findBy([], array('name'=>'ASC'));
		$playlistDump = array();


		foreach( $playlists as $index => $playlist ){
			if( $playlist->getType() != 'Queue' ){
				$object = array();

				$tagCount =  $this->em
				->createQueryBuilder()
				->select(
					array(
						'count(t) as tagsCount'
					)
				)
				->from('AppSupplyWarakinBundle:Playlists', 'p')
				->leftJoin('p.playlistTracks','t')
				->where('p.id =  '.$playlist->getId())
				->groupBy('p.id')
				->getQuery()
				->getResult();
				
				//var_dump( $tagCount[0][]	);
				$object['id'] = $playlist->getId();
				$object['name'] = $playlist->getName();
				$object['tagcount'] = $tagCount[0]['tagsCount'];
				$object['type'] = $playlist->getType();
				$object['playlistGenres'] = $playlist->getPlaylistGenres();


				$playlistDump[] = $object;
			}
		}
		return $playlistDump;
	}



	public function getPlaylist($playlistId){
		$playlist = $this->em
			->getRepository('AppSupplyWarakinBundle:Playlists')
			->findOneById($playlistId);
		//var_dump($playlist->getPlaylistTracks());
		return $playlist;
	}

	
	/**
	 *	generatePlaylistAction
	 */
	public function generatePlaylist($args)
	{
		//var_dump($args);
		$args = json_decode($args);
		//var_dump($args);
		if ( ! property_exists ($args, 'offset')) {
			$args->offset = 0;
		}

		$qb = $this->em
		        ->createQueryBuilder();

		$selectArgs = 	array(
							'distinct t.id as id',
							'count(pe.id) as trackPlayCount'/*,
			        		'a.name as artist',
			        		'a.id as artist_id',
			        		'al.name as album',
			        		'CONCAT(\'data/albums/\', al.id, \'/cover\') as cover',
			        		'al.id as album_id',
							't.deviceid',
							//'t.url',
							't.title as title',
							't.length as length',
							't.track as track_number',
							't.bitrate as bitrate',
							't.lastFmUrl as last_fm_url',
							's.rating as rating',
							's.percentage',
							'y.name as year',
							//'i.path as cover',
							'g.name as genre',
							'saf.tempo as bpm',*/
				        );

		$qb
			->select($selectArgs);

       

		$qb
			->from('AppSupplyWarakinBundle:Tags', 't')
	        ->leftJoin('t.artist','a')
	        ->leftJoin('t.album','al')
	        //->innerJoin('al.images','i')
	        ->leftJoin('t.statistics','s')
	        ->leftJoin('t.playEvents','pe')
	        ->leftJoin('t.year','y')
	        ->leftJoin('t.spotifyAudioFeatures','saf')
			->leftJoin('a.relatedLastFmArtists', 'ral')
			->where('(	t.url like \'%.mp3\' OR t.url like \'%.Mp3\' OR t.url like \'%.MP3\' OR 
						t.url like \'%.flac\' OR t.url like \'%.Flac\' OR t.url like \'%.FLAC\')');

		if (property_exists($args, 'playnextSetting')) {
			if($args->playnextSetting == 'favorites'){
				$qb->addSelect('( s.rating / RAND() ) as HIDDEN rand');
				$qb->andWhere('s.rating >= 8');
			
			}else if($args->playnextSetting == 'explore'){
				$qb->andWhere('s.rating is null');
				$qb->addSelect('RAND() as HIDDEN rand');
			
			}else{
				$qb->addSelect('RAND() as HIDDEN rand');
			
			}
		}else if(property_exists($args, 'favorites')){
			$qb->addSelect('( s.rating / RAND() ) as HIDDEN rand');
			$qb->andWhere('s.rating >= 7');

		}else if(property_exists($args, 'min_rating')){
			$qb->andWhere('s.rating >= '. $args->min_rating);

		}else{
			$qb->addSelect('RAND() as HIDDEN rand');
			$qb->andWhere('(s.rating > 4 or s.rating = 0 or s.rating is null )');

		}
		
		if(property_exists($args, 'genres') || property_exists($args, 'genreGroups')){

			$qb->leftJoin('t.genre','g');
			
			if(property_exists($args, 'genres')){
				//var_dump($args->genres);
				if(count($args->genres)>0){
					$qb->andWhere('g.id IN (:gids)')->setParameter('gids', $args->genres);
				}
			}
			
			if(property_exists($args, 'genreGroups')){
				//var_dump($args->genreGroups);
				if(count($args->genreGroups)>0){
					$qb->leftJoin('g.genreGroup','gg');
					$qb->andWhere('gg.id IN (:genreGroupIds)')->setParameter('genreGroupIds', $args->genreGroups);
				}
			}
		}

		if(property_exists($args, 'song_length_max')){
			//echo('foudn song length');
			$qb->andWhere('t.length <= '.($args->song_length_max*60).' AND t.length > 0');
		}

		if(property_exists($args, 'song_age_max')){
			$song_age = $args->song_age_max-1;
			$currenttime = getdate();
			$song_age = $currenttime['year'] - $song_age;
			$qb->andWhere('y.name >= '.$song_age.' AND y.name <= '.date('Y'));
		}

		if(property_exists($args, 'song_playcount_max')){
			$qb->andWhere('s.playcounter <= '.$args->song_playcount_max);
		}

		if(property_exists($args, 'listeners')){
			$qb->andWhere('t.url like \'%/lara/%\'');
		}
		
		if(property_exists($args, 'serato')){
			$qb->leftJoin('t.seratoFeatures', 'sF');
			$qb->leftJoin('sF.seratoCuepoints', 'sC');
			$qb->andWhere('sC is not null');
		}


		if(property_exists($args, 'order')){
			$qb->orderBy('artist', 'asc');
		}else{
			/**
			 * TODO
			 * Enbable sorting by accumulated number of plays from the event log
			 * This needs to be elegantly fixed. Currently it incurs utilizing a joins on the playevent table
			 * which could be costly. Should we implement daily playcount dumps into the stats table? Would that
			 * be effficient?
			 */
			//$qb->orderBy('trackPlayCount', 'asc');

			//$qb->orderBy('count(pe.id)', 'asc');
			//$qb->orderBy('s.playcounter', 'asc');
			$qb->addOrderBy('rand', 'desc');
		}

		$qb->groupBy('t.id');

        $query = $qb->getQuery();
		//var_dump($query->getSQL());
		//die();

		// $playlist_query .= 'LIMIT '.
	 //    					$offset.', '.$length;
        if(property_exists($args, 'length')){
        	$query->setMaxResults($args->length);
        }
		$result = $query->getResult();
		//var_dump($result);
		$tagList = array();

		
		foreach($result as $tagKey => $tagVal){

			$tag = $this->em
			->getRepository('AppSupplyWarakinBundle:Tags')
			->findOneById($tagVal['id']);

			$tagList[] = $tag;

		}

		//return $result;
		return $tagList;
	}


	public function searchLibrary($searchStr=null)
	{
		$length='20';
		$offset='0';
		$order_direction='asc';

		$libraryArray = array();
		$libraryContainer = array();

		$artistsResult = $this->em->getRepository("AppSupplyWarakinBundle:Artist")->createQueryBuilder('a')
		   	->where('a.name LIKE :name')
		   	->setParameter('name', '%'.$searchStr.'%')
			->orderBy('a.name', 'ASC')
			->setMaxResults(20)
		   	->getQuery()
		   	->getResult();

		foreach($artistsResult as $artist) {
			$resultObj = $artist->getShort();
			$libraryArray[] = array(
								'id'=>'artist:'.$resultObj['id'],
								'label'=>$resultObj['name'],
								'value'=>$resultObj
							);
		}

		$albumsResult = $this->em->getRepository("AppSupplyWarakinBundle:Album")->createQueryBuilder('al')
		   	->where('al.name LIKE :name')
		   	->setParameter('name', '%'.$searchStr.'%')
				  ->orderBy('al.name', 'ASC')
				  ->setMaxResults(20)
		   	->getQuery()
		   	->getResult();

		foreach($albumsResult as $album){
			$resultObj = $album->getShort();
			$libraryArray[] = array(
								'id'=>'album:'.$resultObj['id'],
								'label'=>$resultObj['name'],
								'value'=>$resultObj
							);
		}
		//$libraryArray = array_merge($libraryArray, $libraryContainer);
		//empty($libraryContainer);



		$tracksResult = $this->em->getRepository("AppSupplyWarakinBundle:Tags")->createQueryBuilder('t')
        	->leftJoin('t.artist', 'a')
		   	->where('t.title LIKE :title')
		   	->setParameter('title', '%'.$searchStr.'%')
				  ->orderBy('a.name', 'ASC')
				  ->setMaxResults(20)
		   	->getQuery()
		   	->getResult();

		foreach($tracksResult as $track){
			//var_dump($track);die();
			//$resultObj[] = $track->getShort();
			if($track->getArtist()){
				$label = $track->getArtist()->getName().' - '.$track->getTitle();	
			}else{
				$label = $track->getTitle();
			}
			
			$libraryArray[] = array(
								'id'=>'track:',
								'label'=>$label,
								'value'=>$track
							);
		}
		//$libraryArray = array_merge($libraryArray, $libraryContainer);
		//empty($libraryContainer);

		return  $libraryArray;
	}



	/**
	 *	Utility Functions
	 */

	public function getAlbumCover($artistName, $albumName, $album=false, $track=false)
	{

		// check for cover in database
		$covers = $this->em
		               ->getRepository('AppSupplyWarakinBundle:Images')
		               ->findBy(
						    array(
						    	'album' => $albumName,
						    	'artist' => $artistName
						    )
						);
						
		$coverPath = false;
		foreach($covers as $index=>$cover){
			$coverPath = $cover->getPath();
		}

		// check for cached image with hash filename or file in album folder
		// this approach was inherited from Amarok / MusicBrainz
		$md5 = md5(strtolower($artistName.$albumName));
		$md5Various = md5("".strtolower($albumName));

		// echo($md5);
		if(file_exists('/sites/warakin.utf8/app/data/album-covers/large/'.$md5))
		{
			return  '/sites/warakin.utf8/app/data/album-covers/large/'.$md5;
		}
		elseif(file_exists('/sites/warakin.utf8/app/data/album-covers/large/'.$md5Various))
		{
			return  '/sites/warakin.utf8/app/data/album-covers/large/'.$md5Various;
		}
		elseif(file_exists('/sites/warakin.utf8/app/data/album-covers/tagcover/'.$md5))
		{
			return '/sites/warakin.utf8/app/data/album-covers/tagcover/'.$md5;
		}
		elseif(file_exists('/sites/warakin.utf8/app/data/album-covers/tagcover/'.$md5Various))
		{
			return  '/sites/warakin.utf8/app/data/album-covers/tagcover/'.$md5Various;
		}
		elseif(file_exists('/sites/warakin.utf8/app/data/album-covers/large/'.$md5.'_temp'))
		{
			return  '/sites/warakin.utf8/app/data/album-covers/large/'.$md5.'_temp';
		}
		if($album)
		{
			if(count($album->getTags())>0){
				// get first track and strip dir str from it
				//echo 'test';
				
				$tags = $album->getTags();
				$filenames =[
					"cover.jpg",
					"Cover.jpg",
					"COVER.JPG",
					"albumArtLarge.jpg",
					"folder.jpg",
					"Folder.jpg",
					"FOLDER.JPG",
					"albumArtSmall.jpg",
					"small.jpg"
				];

				foreach($filenames as $index=>$filename){
					//echo "[".$tags[0]->getUrl()."]";
					//echo "[".dirname($tags[0]->getUrl()).'/'.$filename."]<br/>";
					if( file_exists( dirname($tags[0]->getUrl()).'/'.$filename ) )
					{
						//echo dirname($tags[0]->getUrl()).'/'.$filename;
						return  dirname($tags[0]->getUrl()).'/'.$filename;
					}
				}
			}
		}
		
		if(file_exists('/'.$coverPath) && $coverPath!=NULL)
		{
			return '/'.$coverPath;//str_replace('#', '%23', str_replace(' ','%20',addslashes(substr($coverPath , 7))));
		}
		
		// try finding image on last fm
		// http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=YOUR_API_KEY&artist=cher&track=believe

		if($track){
			$tracksLastFm = $this->lastFmTrackClient->search(
				preg_replace('/[[:^print:]]/', '', $track->getTitle()),
				preg_replace('/[[:^print:]]/', '', $artistName)
			);
		
			//var_dump($tracksLastFm);
			$coverUrl = false;
			foreach($tracksLastFm as $index=>$trackLastFm){
				if(
					strpos(strtolower($artistName), strtolower($trackLastFm->getArtist()->getName()))!==false || 

					strtolower($artistName) == strtolower($trackLastFm->getArtist()->getName())
				){
					//var_dump($trackLastFm);

					try{
						$tracksInfoLastFm = $this->lastFmTrackClient->getInfo(
							preg_replace('/[[:^print:]]/', '', $trackLastFm->getArtist()->getName()),
							preg_replace('/[[:^print:]]/', '', $trackLastFm->getName()), 
							$trackLastFm->getMbid()
						);
						if($tracksInfoLastFm->getAlbum()){
							$images = $tracksInfoLastFm->getAlbum()->getImages();
							if($images){
								foreach($images as $image){
									$coverUrl = $image;
								}
								if($coverUrl!==false && $coverUrl !== ""){
									file_put_contents('/sites/warakin.utf8/app/data/album-covers/large/'.$md5 ,file_get_contents($coverUrl));
									return '/sites/warakin.utf8/app/data/album-covers/large/'.$md5;
								}
							}	
						}
					} catch (\Exception $e) {
						echo 'Caught exception: ',  $e->getMessage(), "\n";
					}
				}
			}
		}
		copy ( 
			'/sites/warakin.utf8/web/bundles/appsupplywarakin/img/defaultAlbum.png', 
			'/sites/warakin.utf8/app/data/album-covers/large/'.$md5.'_temp' 
		); 
		return '/sites/warakin.utf8/web/bundles/appsupplywarakin/img/defaultAlbum.png';

	}

	public function getGenres(){

        $genresCollection = $this->em
                    ->getRepository('AppSupplyWarakinBundle:Genre')             
                    ->findBy(array(), array('name' => 'ASC'));

        foreach($genresCollection as $index=>$genre)
        {

            $genreShort = $genre->getShort();

            // nasty hack; fix it in entity class
            $args = "{'genres':[".$genre->getId()."],'min_rating':6,'max_rating':10,'song_length':12}";

            $genreShort['favoritesCount'] = count($this->generatePlaylist($args));

            $args = "{'genres':[".$genre->getId()."]}";

            $genreShort['favorites'] = count($this->generatePlaylist($args));
            
            $genres[] =  $genreShort;
        	//array('genres'=>array(24),'length'=>12000,'min_rating'=>10,'max_rating'=>10,'song_length'=>12);

        	
        }

        return $genres;
	}

	public function getTrack($trackId){
        return $this->em
                    ->getRepository('AppSupplyWarakinBundle:Tags')             
                    ->findOneById($trackId);
	}


	public function getTags($typeId=null){
		//var_dump($typeId);
		 $qb = $this->em->createQueryBuilder()
            ->select(
                array(
                    'distinct lft.id',
                    'lft.name',
                    'count(t) as trackCount'
                )
            )
			->from('AppSupplyWarakinBundle:LastFmTag', 'lft')
	        ->innerJoin('lft.tracks','t')
            ->having('trackCount > 3')
            ->groupBy('lft.id')
			->orderBy('trackCount', 'DESC')
			->setMaxResults(10000);

		if($typeId!=null){
			//var_dump('filter by type: '.$typeId);
			$qb->where('lft.type = :typeId')->setParameter(':typeId', $typeId);
		}

		return $qb->getQuery()
			->setHydrationMode(\Doctrine\ORM\AbstractQuery::HYDRATE_SINGLE_SCALAR)
			->getResult();
	}

	public function getTag($tagId){
		
		$tagResult = $this->em->createQueryBuilder()
			->select(
				array(
					'distinct lft.id',
					'lft.name',
					'count(t) as trackCount'
				)
			)
			->from('AppSupplyWarakinBundle:LastFmTag', 'lft')
			->innerJoin('lft.tracks','t')
			->where('lft.id = :tagId')
			->setParameter(':tagId', $tagId)
			->having('trackCount > 3')
			->groupBy('lft.id')
			->orderBy('trackCount', 'DESC')
			->setMaxResults(10000)
			->getQuery()
			->setHydrationMode(\Doctrine\ORM\AbstractQuery::HYDRATE_SINGLE_SCALAR)
			->getResult();
		
		$trackResult = $this->em->createQueryBuilder()
		->select(
			array(
				'distinct t.id as id',
				'a.name as artist',
				't.title as title',
				// 'lft.name',
				// 'count(t) as trackCount'
			)
		)
		->from('AppSupplyWarakinBundle:LastFmTag', 'lft')
		->innerJoin('lft.tracks','t')
		->innerJoin('t.artist','a')
		->where('lft.id = :tagId')
		->setParameter(':tagId', $tagId)
		->groupBy('t.id')
		->setMaxResults(10000)
		->getQuery()
		->setHydrationMode(\Doctrine\ORM\AbstractQuery::HYDRATE_SINGLE_SCALAR)
		->getResult();
		$tagResult=$tagResult[0];
		$tagResult['tracks'] = $trackResult;
	return $tagResult;
	}
}
