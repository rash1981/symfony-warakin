<?php

namespace AppSupply\WarakinBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use BinaryThinking\LastfmBundle\Lastfm\Client\Method\ArtistMethodsClient;
use BinaryThinking\LastfmBundle\Lastfm\Client\Method\TrackMethodsClient;
use Symfony\Component\HttpFoundation\Response;
use SpotifyWebAPI\SpotifyWebAPI;

use Symfony\Component\Finder\Finder;
use \GetId3\GetId3Core as GetId3;

class Synch
{
	private $em;
	private $lastfm_artist_client;

    public function __construct(EntityManager $em, ArtistMethodsClient $artistClient, TrackMethodsClient $trackClient)
    {
        $this->em = $em;


        $clientId = 'b1292efee02548e1889889416961d000';
        $clientSecret = '6bff08ff88864219bd98f0edb783e07e';
        $sesh = new \SpotifyWebAPI\Session($clientId, $clientSecret);
        //$this->spotifyApi = new \SpotifyWebAPI\SpotifyWebAPI($request);

        $fact = new \Nomad145\SpotifyBundle\Factory\SpotifyApiFactory($sesh);
        $this->spotifyApi = $fact->createSpotifyApi();
        
        //$this->trackClient = $this->get('binary_thinking_lastfm.client.track');
        //$this->clientFactory = $this->get('binary_thinking_lastfm.client_factory');

        //$this->trackClient = $this->clientFactory->getClient('track', '686f2a0a882274135e2fcb131ff1fe60', "d9e1ad43533ffcb93c291dea4f10e104");
        //$this->artistClient = $this->clientFactory->getClient('artist', '686f2a0a882274135e2fcb131ff1fe60', "d9e1ad43533ffcb93c291dea4f10e104");
        $this->artistClient = $artistClient;
        $this->trackClient = $trackClient;
    }


	/**
	 *	 API Synchs 
	 *	 To keep things speedy these should be moved to their own service
	 * 	 Will keep the data service, meant for providing local data to the player,
	 *	 Speedy and low on memeory footprint by not inlcuding the relatively large codesets 
	 *	 of the Spotify, Last.fm tc. API's.
	 */


	// find related artists by artist

	public function synchSocialMediaRelatedArtists(){

    $limit = 10;
    $offset = 0;

    if(array_key_exists('offset', $_GET)){
        $offset = $_GET['offset'];
    }

		$artists = $this->em
			->getRepository('AppSupplyWarakinBundle:Artist')
			//->findBy(array(), array('name'=>'asc'));
            ->createQueryBuilder('a')
            
			->leftJoin('a.relatedLastFmArtists', 'r')
			->where('r.id IS NULL')
            //->where('a.lastFmMbid is null')
            //->andWhere('a.spotifyId is null')
            ->setFirstResult( $offset ) //231
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if(count($artists)<0){die('nothing left to do');}
        echo '<html><body><ol start="'.($offset+1).'">';

		foreach($artists as $index => $artist){

      echo '<li>Researching artist: '.$artist->getName().'<br/>';
      $relatedArtists = $this->fetchArtistRelationsFromSocial($artist);

      // var_dump($relatedArtists);
      if($relatedArtists && array_key_exists('lastFm', $relatedArtists) && $relatedArtists['lastFm']){
        echo "LastFM: ";
        foreach($relatedArtists['lastFm'] as $relatedLastFmArtist){

            $relatedWarakinArtist = $this->em
                ->getRepository('AppSupplyWarakinBundle:Artist')
                ->findOneByName($relatedLastFmArtist->getName());
            if($relatedWarakinArtist){
                echo $relatedLastFmArtist->getName().", ";
                $artist->addRelatedLastFmArtist($relatedWarakinArtist);
                //var_dump($relatedLastFmArtist);
            }
        }
        echo '<br/>';

      }

      if($relatedArtists && array_key_exists('spotify', $relatedArtists) && $relatedArtists['spotify']){
        //var_dump($relatedArtists['spotify']);
        echo "Spotify: ";
        foreach($relatedArtists['spotify'] as $relatedSpotifyArtist){

          $relatedWarakinArtist = $this->em
            ->getRepository('AppSupplyWarakinBundle:Artist')
            ->findOneByName($relatedSpotifyArtist->name);

          if($relatedWarakinArtist){
              echo $relatedSpotifyArtist->name.", ";
              $artist->addRelatedSpotifyArtist($relatedWarakinArtist);
              //var_dump($relatedSpotifyArtist);
          }

        }
        echo '<br/>';
      }
      echo '<br/><br/></li>';
      
      //die("<br/>\nwe done here");
      $this->em->persist($artist);
      $this->em->flush();
    }

    echo "</ol><script>window['location'].href='?offset=".($offset+$limit)."'</script>";

    die("<br/>\nthis page is done</body></html>");
		
	}

	public function fetchArtistRelationsFromSocial($artist){

		$randomTrack = $this->em
		               	->getRepository('AppSupplyWarakinBundle:Tags')
		               	->createQueryBuilder('t')
						->where('t.artist = :artist')
						->setParameter('artist', $artist)
     					->setMaxResults(1)
						->getQuery()
						->getResult();

		
    if($randomTrack){
        $relatedArtists = array(
            'lastFm' => $this->fetchArtistRelationsFromLastFm($randomTrack[0]),
            'spotify' => $this->fetchArtistRelationsFromSpotify($randomTrack[0])
            //'mixCloud' =>
            //'discogs' =>
            //'musicBrainz' => 
            // optional other           
        );
        return $relatedArtists;
    }else{
        echo ('no tracks for artist '.$artist->getName().' ('.$artist->getId().')');
        return [];
    }
 
	}	


    // find related tracks by track

	public function synchSocialMediaRelatedTracks(){

    $limit = 1;
    $offset = 0;


    if(array_key_exists('id', $_GET)){
        $id = intval($_GET['id']);
    }else{$id=false;}
    if(array_key_exists('offset', $_GET)){
        $offset = intval($_GET['offset']);
    }

		$qb = $this->em
			->getRepository('AppSupplyWarakinBundle:Tags')
			//->findBy(array(), array('name'=>'asc'));
			->createQueryBuilder('t')
            //->where('a.lastFmMbid is null')
            //->andWhere('a.spotifyId is null')
            ->setMaxResults($limit);
            
        if($id){
            $qb->where('t.id = :id')
                ->setParameter('id', $id);
                $offset=0;
        }elseif($offset){
            $qb->setFirstResult( $offset ); //231
        }
        
        $tags = $qb->getQuery()->getResult();
        
        echo '<html><head><meta charset="utf-8" /></head><body><ol start="'.($offset+1).'">';


		foreach($tags as $index => $track){
            $artistName = 'Unknown Artist';
            if($track->getArtist()){
                $artistName = $track->getArtist()->getName();
            }
            echo '<li>Researching track: '.$artistName.' - '.$track->getTitle().' ('.$track->getId().')<br/>';
            echo 'Researching track: '.preg_replace('/[[:^print:]]/', '', $artistName).' - '.preg_replace('/[[:^print:]]/', '', $track->getTitle()).'<br/>';
            echo 'Researching track: '.preg_replace('/[p{L}]/', '', $artistName).' - '.preg_replace('/[p{L}]/', '', $track->getTitle()).'<br/>';
            
			$relatedTracks = $this->fetchTrackRelationsFromSocial($track);

            // if($relatedTracks['lastFm']){

            //     foreach($relatedTracks['lastFm'] as $relatedLastFmTrack){
            //         $track->addRelatedLastFmtrack($relatedLastFmTrack);
            //     }

            // }
            if($relatedTracks['spotify']){

                foreach($relatedTracks['spotify'] as $relatedSpotifyTrack){

                    $relatedWarakinTrack = $this->em
			            ->getRepository('AppSupplyWarakinBundle:Tags')
                        ->findOneByName($relatedSpotifyTrack->name);

                    if($relatedWarakinTrack){
                        $track->addRelatedSpotifytrack($relatedWarakinTrack);
                        //var_dump($relatedSpotifytrack);
                    }
                    
                }

            }
            echo '<br/><br/></li>';
            
            //die("<br/>\nwe done here");
            
            //$this->em->persist($track);
            //$this->em->flush();
        }

        echo "</ol>";

        if(!$id && count($tags)>0){
            echo "<script>window['location'].href='?offset=".($offset+$limit)."'</script>";
        }
        die("<br/>\nwe doublegone done here</body></html>");
		
	}

	// find related tracks by track

	public function fetchTrackRelationsFromSocial($seedTrack){
    echo "fetchTracksRelationsFromLastFm called <br>\n";
		
    $relatedTracks = array(
			'lastFm' => $this->fetchTrackRelationsFromLastFm($seedTrack),
			'spotify' => $this->fetchTrackRelationsFromSpotify($seedTrack)
		);
		
        return $relatedTracks;
	
    }	



    /**
     *
     *      LAST.FM FUNCTIONS
     *
     */


	private function fetchArtistRelationsFromLastFm($artistTrack){
        echo "fetchArtistRelationsFromLastFm called <br>\n";
        $artist = $artistTrack->getArtist();

        if(!$artist->getLastFmMbid()){

            $lastFmArtist = $this->fetchCurrentLastFmArtist($artistTrack);

            if(method_exists($lastFmArtist, 'getName')){

                echo('Found lastFm artist: '.$lastFmArtist->getName().' :: '.$lastFmArtist->getMbid()."<br/>\n");
                //echo('Found lastFm artist: '.$lastFmArtist->getMbid()."<br/>\n");

                $artist->setLastFmMbid($lastFmArtist->getMbid());
                $this->em->persist($artist);
                $this->em->flush();

            }else{

                return false;

            }
        }else{
            echo("LastFm artist already in DB.<br/>\n");
            $lastFmArtist = $this->artistClient->getInfo($artist->getName(), $artist->getLastFmMbid());
        }
        sleep(3);
        return $lastFmArtist->getSimilar($artist->getName(), $artist->getLastFmMbid());

	}

    /**
     *  Find the current artist on last.fm
     *  @return matching artists
     *
     */

    public function fetchCurrentLastFmArtist(\AppSupply\WarakinBundle\Entity\Tags $currentTrack){
        if($currentTrack->getArtist()){
            $artistsLastFm = $this->artistClient->search($currentTrack->getArtist()->getName());
            
            $currentArtistLastFm = false;
            
            foreach($artistsLastFm as $index=>$artistLastFm){
                // echo 'Does '.$this->currentTrack->getArtist()->getName().' contain '.$trackLastFm->getArtist()->getName().' '.
                //     strpos($this->currentTrack->getArtist()->getName(), $trackLastFm->getArtist()->getName())."<br/>";
                // echo 'Is "'.$this->currentTrack->getArtist()->getName().'" the same as "'.$trackLastFm->getArtist()->getName().'"';
                //     var_dump( strtolower($currentTrack->getArtist()->getName()) == strtolower($trackLastFm->getArtist()->getName()) );


                if(
                    strpos(strtolower($currentTrack->getArtist()->getName()), strtolower($artistLastFm->getName()))!==false || 

                    strtolower($currentTrack->getArtist()->getName()) == strtolower($artistLastFm->getName())
                ){
                    $currentArtistLastFm = $artistLastFm;
                    break;
                }
            }
            //var_dump($currentArtistLastFm);
            //die('deaded');
            sleep(3);
            return $currentArtistLastFm;
        }else{
            return false;
        }
    }


    /**
     *  Find the current track on last.fm
     *  @return matching track
     *
     */

    public function fetchCurrentLastFmTrack(\AppSupply\WarakinBundle\Entity\Tags $currentTrack){

      echo "fetchCurrentLastFmTrack called <br/>\n";
      
      // if($currentTrack->getArtist() && $currentTrack->getLastFmUrl()!== null){
      if($currentTrack->getArtist() && 
          ($currentTrack->getLastFmUrl() == null || $currentTrack->getLastFmUrl() == "")){
            $tracksLastFm = $this->trackClient->search(
                preg_replace('/[[:^print:]]/', '', $currentTrack->getTitle()), 
                preg_replace('/[[:^print:]]/', '', $currentTrack->getArtist()->getName())
            );
            
            
            //var_dump($tracksLastFm);

            $currentTrackLastFm = false;
            foreach($tracksLastFm as $index=>$trackLastFm){
                // echo 'Does '.$this->currentTrack->getArtist()->getName().' contain '.$trackLastFm->getArtist()->getName().' '.
                //     strpos($this->currentTrack->getArtist()->getName(), $trackLastFm->getArtist()->getName())."<br/>";
                // echo 'Is "'.$this->currentTrack->getArtist()->getName().'" the same as "'.$trackLastFm->getArtist()->getName().'"';
                //     var_dump( strtolower($currentTrack->getArtist()->getName()) == strtolower($trackLastFm->getArtist()->getName()) );


                if(
                    strpos(strtolower($currentTrack->getArtist()->getName()), strtolower($trackLastFm->getArtist()->getName()))!==false || 

                    strtolower($currentTrack->getArtist()->getName()) == strtolower($trackLastFm->getArtist()->getName())
                ){

                    // found track, now store it 

                    /*
                    object(BinaryThinking\LastfmBundle\Lastfm\Model\Track)#524 (12) {
                        ["number":protected]=>
                        NULL
                        ["name":protected]=>
                        string(7) "Believe"
                        ["duration":protected]=>
                        int(0)
                        ["nowplaying":protected]=>
                        bool(false)
                        ["playcount":protected]=>
                        int(0)
                        ["listeners":protected]=>
                        int(8929)
                        ["loved":protected]=>
                        NULL
                        ["mbid":protected]=>
                        string(36) "575e0e2a-c30d-492a-8455-501df9229bf8"
                        ["url":protected]=>
                        string(50) "https://www.last.fm/music/Origin+Unknown/_/Believe"
                        ["streamable":protected]=>
                        int(0)
                        ["artist":protected]=>
                        object(BinaryThinking\LastfmBundle\Lastfm\Model\Artist)#521 (11) {
                            ["name":protected]=>
                            string(14) "Origin Unknown"
                            ["mbid":protected]=>
                            string(0) ""
                            ["url":protected]=>
                            string(0) ""
                            ["images":protected]=>
                            array(0) {}
                            ["streamable":protected]=>
                            int(0)
                            ["listeners":protected]=>
                            NULL
                            ["playCount":protected]=>
                            NULL
                            ["similar":protected]=>
                            array(0) {
                            }
                            ["tags":protected]=>
                            array(0) {
                            }
                            ["bio":protected]=>
                            array(0) {
                            }
                            ["weight":protected]=>
                            int(0)
                            }
                            ["images":protected]=>
                            array(4) {
                            ["small"]=>
                            string(78) "https://lastfm-img2.akamaized.net/i/u/34s/3e5297d36ec34e28b63a608924613c34.png"
                            ["medium"]=>
                            string(78) "https://lastfm-img2.akamaized.net/i/u/64s/3e5297d36ec34e28b63a608924613c34.png"
                            ["large"]=>
                            string(79) "https://lastfm-img2.akamaized.net/i/u/174s/3e5297d36ec34e28b63a608924613c34.png"
                            ["extralarge"]=>
                            string(82) "https://lastfm-img2.akamaized.net/i/u/300x300/3e5297d36ec34e28b63a608924613c34.png"
                        }
                    }
                    */

                    $currentTrack->setLastFmTitle($trackLastFm->getName());
                    $currentTrack->setLastFmArtistName($trackLastFm->getArtist()->getName());
                    $currentTrack->setLastFmUrl($trackLastFm->getUrl());
                    $currentTrack->setLastFmMbid($trackLastFm->getMbid());


                    $this->em->persist($currentTrack);
                    $this->em->flush();

                    $currentTrackLastFm = $trackLastFm;
                    break;
                }
            }
            //var_dump($currentTrackLastFm);
            return $currentTrackLastFm;
        }else if($currentTrack->getLastFmUrl()){
            return true;
        }else{
            return false;
        }
    }



    /**
     *  Fetch all track associations on Last.fm
     *  match them to warakin db
     *  @return matching track array
     */

    private function fetchTrackRelationsFromLastFm($warakinTrack){

      echo ("fetchTrackRelationsFromLastFm called <br/>\n");
        
        if(!$warakinTrack->getArtist()){return array();};

        $lastFmTrack = $this->fetchCurrentLastFmTrack($warakinTrack);
        
        $relatedTracks=array();
        
        if($lastFmTrack){
            // fetch similar tracks list from last fm
            try {
               
                if( !is_bool($lastFmTrack)  && $lastFmTrack->getMbid()){
                    $relatedTracksLastFm = $this->trackClient->getSimilar( preg_replace('/[[:^print:]]/', '', $warakinTrack->getTitle()), preg_replace('/[[:^print:]]/', '', $warakinTrack->getArtist()->getName()), $lastFmTrack->getMbid() );
                }else{
                    $relatedTracksLastFm = $this->trackClient->getSimilar( preg_replace('/[[:^print:]]/', '', $warakinTrack->getTitle()), preg_replace('/[[:^print:]]/', '', $warakinTrack->getArtist()->getName()) );
                }

            } catch (\Exception $e) {
                echo 'Track.getSimilar did not return track ',  $e->getMessage(), "\n";
                return [];
            }
            //var_dump($relatedTracksLastFm);
            
            foreach($relatedTracksLastFm as $relatedTrackLastFm){
                
                $query = $this->em->createQuery("SELECT t FROM AppSupplyWarakinBundle:Tags t JOIN t.artist a WHERE a.name = :artistName and t.title = :trackTitle");
                $query->setParameters(array(
                    'artistName' => $relatedTrackLastFm->getArtist()->getName(),
                    'trackTitle' => $relatedTrackLastFm->getName()
                ));
                $temp_tracks = $query->getResult();
                
                if(count($temp_tracks)>0){

                    echo 'Last fm track found in db:'.$temp_tracks[0]->getArtist()->getName().' - '.$temp_tracks[0]->getTitle()."<br/>\n";

                    $relatedTracks[] = $temp_tracks[0];
                    $warakinTrack->addRelatedLastFmTrack($temp_tracks[0]);

   
                }else{
                    //echo 'Related Last.fm Track: '.$relatedTrackLastFm->getArtist()->getName().' - '.$relatedTrackLastFm->getName()."<br/>\n";
                
                }
            }  

            $this->em->persist($warakinTrack);
            $this->em->flush();

        }      

        return $relatedTracks;
    }

    /**
     *  Fetch track info on Last.fm
     *  and store it in warakin db
     *  @return matching track array
     */

    public function fetchLastFmTrackInfo($warakinTrack){
        $sleep = 2;
        ini_set('memory_limit', '2048M');
        set_time_limit(0);

        $lastFmTrackInfo = $warakinTrack->getLastFmTrackInfo();
        $mbid = $warakinTrack->getLastFmMbid();

        if(!$mbid){
            $lastFmTrack = $this->fetchCurrentLastFmTrack($warakinTrack);
            if($lastFmTrack){
                $mbid = $lastFmTrack->getMbid();
            }
            sleep($sleep);
        }        

        $relatedTracks=array();
        
        if($mbid){
            
            if(!$lastFmTrackInfo){
                // fetch track info from last fm
                try {
                    if($mbid){
                        $lastFmTrackInfo = $this->trackClient->getInfo( preg_replace('/[[:^print:]]/', '', $warakinTrack->getTitle()), preg_replace('/[[:^print:]]/', '', $warakinTrack->getArtist()->getName()), $mbid );
                    }else{
                        $lastFmTrackInfo = $this->trackClient->getInfo( preg_replace('/[[:^print:]]/', '', $warakinTrack->getTitle()), preg_replace('/[[:^print:]]/', '', $warakinTrack->getArtist()->getName()) );
                    }
                    echo '<pre>'; 
                    var_dump($lastFmTrackInfo);
                    echo '</pre>';

                } catch (\Exception $e) {
                    echo 'Track.getInfo did not return data: ',  $e->getMessage(), "\n";
                    return [];
                }

                sleep($sleep);

                if($lastFmTrackInfo){
                    $lastFmInfo = $warakinTrack->getLastFmTrackInfo();
                    if(!$lastFmInfo){
                        $lastFmInfo = new \AppSupply\WarakinBundle\Entity\LastFmTrackInfo();
                    }
                    $lastFmInfo->setPlaycount($lastFmTrackInfo->getPlaycount());
                    $lastFmInfo->setListeners($lastFmTrackInfo->getListeners());
                    if(!$warakinTrack->getLastFmTrackInfo()){
                        $warakinTrack->setLastFmTrackInfo($lastFmInfo);
                    }else{
                        $this->em->persist($lastFmInfo);
                        $this->em->flush();
                    }
                }

            }

           

            // fetch track tags from last fm
            try {
                if($mbid){
                    $lastFmTrackTags = $this->trackClient->getTopTags( 
                        preg_replace('/[[:^print:]]/', '', $warakinTrack->getArtist()->getName()), 
                        preg_replace('/[[:^print:]]/', '', $warakinTrack->getTitle()), 
                        $mbid 
                    );
                }else{
                    $lastFmTrackTags = $this->trackClient->getTopTags( 
                        preg_replace('/[[:^print:]]/', '', $warakinTrack->getArtist()->getName()) ,
                        preg_replace('/[[:^print:]]/', '', $warakinTrack->getTitle())
                    );
                }
                // echo '<pre>'; 
                // var_dump($lastFmTrackTags);
                // echo '</pre>';

                
                // $tagNames = [];
                // foreach($lastFmTrackTags as $index=>$lastFmTrackTag){
                //     $tagNames[] = $lastFmTrackTag->getName();
                // }

                // $qb->select('m');
                // $qb->from('AppSupplyWarakinBundle:LastFmTag', 'lft');
                // $qb->where($qb->expr()->in('lft.name', $tagNames));
                
                // //ArrayCollection
                // $result = $qb->getQuery()->getResult();

                foreach($lastFmTrackTags as $index=>$lastFmTrackTag){
                    $tag = $this->em
                        ->getRepository('AppSupplyWarakinBundle:LastFmTag')
                        ->findOneByName($lastFmTrackTag->getName());
                    if($tag){
                        echo '<p>tag <em>'.$lastFmTrackTag->getName().'</em> exists in db </p>';
                        $lastFmTag = $tag;
                    }else{
                        // needs to be created in db
                        echo '<p>tag <em>'.$lastFmTrackTag->getName().'</em> needs to be created </p>';

                        $lastFmTag =new \AppSupply\WarakinBundle\Entity\LastFmTag();
                        $lastFmTag->setName($lastFmTrackTag->getName());
                        
                        $this->em->persist($lastFmTag);
                        $this->em->flush();
                    }

                    // add tag to Track
                    $warakinTrack->addLastFmTag($lastFmTag);
                }
                if(count($lastFmTrackTags) > 0){
                    $this->em->persist($warakinTrack);
                    $this->em->flush();

                    $playEvent = new \AppSupply\WarakinBundle\Entity\PlayEvent();
                    $playEvent->setTrack($warakinTrack);
                    $playEvent->setType("NewData-LastFmTag");

                    $this->em->persist($playEvent);
                    $this->em->flush();
                }else{
                    sleep($sleep);
                }

            } catch (\Exception $e) {
                echo 'Track.getTags did not return data: ',  $e->getMessage(), "\n";
                //return [];
            }
        }else{
            echo '<p>Current track ('.$mbid.') was not found in Last.fm database.</p>'."\n";
        }

        return $warakinTrack;
    }

    /**
     *
     *      SPOTIFY FUNCTIONS
     *
     */



	public function fetchArtistRelationsFromSpotify($artistTrack){

        $artist = $artistTrack->getArtist();

        if(!$artist->getSpotifyId()){

            $spotifyArtist = $this->fetchCurrentSpotifyArtist($artistTrack);
            
            if($spotifyArtist){
                echo('Found spotify artist: '.$spotifyArtist->name.' :: '.$spotifyArtist->id."<br/>\n");

                $artist->setSpotifyId($spotifyArtist->id);
                $this->em->persist($artist);
                $this->em->flush();
            }else{
                return false;
            }

        }else{
            echo("Spotify artist already in DB<br/>\n");
        }

        sleep(3);
        return $this->spotifyApi->getArtistRelatedArtists($artist->getSpotifyId())->artists;

	}


    /**
     *  Fetch current artist on Spotify
     *  @return Spotify artist object
     */

    public function fetchCurrentSpotifyArtist($currentTrack){

        //query Spotify

        // build up track search query

        //$q = 'track:'.$currentTrack->getTitle().' ';
        if($currentTrack->getArtist()){
            $q = 'artist:'.$currentTrack->getArtist().' ';
            $artistResult = $this->spotifyApi->search($q, 'artist');
        }else{
            $q = 'track:'.$currentTrack->getTitle().' ';
            $artistResult = $this->spotifyApi->search($q, 'artist');
        }
        

        // if not track found strip the extras from the track title and try again
        /*if(count($trackResult->tracks->items)==0){
            $strippedTitle = preg_replace('/\([^)]*\)/', '', $currentTrack->getTitle());
            $q = 'track:'.$strippedTitle.' ';
            if($currentTrack->getArtist()){
                $q .= 'artist:'.$currentTrack->getArtist().' ';
            }
            if($currentTrack->getAlbum()){
                $q .= 'album:'.$currentTrack->getAlbum()->getName();
            }
            $trackResult = $this->spotifyApi->search($q, 'track');
        }*/

        //var_dump($artistResult->artists);
        if( count($artistResult->artists->items) > 0){
            sleep(3);
            return $artistResult->artists->items[0];
        }else{
            return false;
        }
    }


    /**
     *  Fetch all track associations on Spotify
     *  match them to warakin db
     *  @return matching track array
     */

    public function fetchCurrentSpotifyTrack($currentTrack){

        //query Spotify

        // build up track search query

        $q = 'track:'.$currentTrack->getTitle().' ';
        if($currentTrack->getArtist()){
            $q .= 'artist:'.$currentTrack->getArtist().' ';
        }
        if($currentTrack->getAlbum()){
            $q .= 'album:'.$currentTrack->getAlbum()->getName();
        }
        
        $trackResult = $this->spotifyApi->search($q, 'track');

        // if not track found strip the extras from the track title and try again
        if(count($trackResult->tracks->items)==0){
            $strippedTitle = preg_replace('/\([^)]*\)/', '', $currentTrack->getTitle());
            $q = 'track:'.$strippedTitle.' ';
            if($currentTrack->getArtist()){
                $q .= 'artist:'.$currentTrack->getArtist().' ';
            }
            if($currentTrack->getAlbum()){
                $q .= 'album:'.$currentTrack->getAlbum()->getName();
            }
            $trackResult = $this->spotifyApi->search($q, 'track');
        }

        return $trackResult;
    }



    /**
     *  Fetch all track associations on Spotify
     *  match them to warakin db
     *  @return matching track array
     */

    private function fetchTrackRelationsFromSpotify($currentSpotifyTrack, $warakinTrack=null){
        $relatedTracks = array();
        
        // TODO: fetch track extended meta data
        
        // if(!$this->currentTrack->getspotifyAudioFeatures()){
        //     $this->currentTrack = $this->getSpotifyAudioFeatures($currentSpotifyTrack, $warakinTrack);
        // }

        // TODO: fetch related tracks 
         
        // Fetch related artist tracks

        

        return $relatedTracks;
    }

    public function synchSeratoFeatures(){

    $timelimit_count = 5000;
		set_time_limit($timelimit_count);
		ini_set('memory_limit','320M');
		header('Content-Type: text/html; charset=utf-8');
		
		// setup variables for scan
		$i = 0;

		// set scanning directories
		$dirs = array(
				/*
				 * test set

				'/media/MP3.1/A/aux raus - aux raus (2006)',
				'/media/MP3.NEW/Urfaust/',
				'/media/MP3.NEW/Vive La Fête',
				*/
				
				// '/media/MP3.NEW/00-INCOMING',
				
				'/media/MP3.NEW',
				'/media/MP3.1/',
				'/media/MP3.3',
        '/media/MP3.2/',
				'/media/MP3.4',
				'/media/MP3.5',
				'/media/MP3.6/',
				'/media/MP3.VA',

				// '/media/MP3.1/#',	
				
				//'/media/MP3.1/A',
				
				//'/media/MP3.1/B',
				

				//'/media/MP3.2/C',
				//'/media/MP3.2/C/Crookers/Tons Of Friends',
				//'/media/MP3.2/C/Charlie Parker - Birth of the Bebop',
				
				//'/media/MP3.2/D',
				

				//'/media/MP3.3/E',
				
				//'/media/MP3.3/F',
				//'/media/MP3.3/G',
				
				//'/media/MP3.3/H',
				//'/media/MP3.3/I',

				//'/media/MP3.3/J',
				//'/media/MP3.3/K',
				
				//'/media/MP3.4/.Trash-1000',
				//'/media/MP3.4/L',
				
				//'/media/MP3.4/M',

				//'/media/MP3.4/N',
				
				//'/media/MP3.4/O',
				
				
				//'/media/MP3.5/P',
				
				// '/media/MP3.5/Q',
				// '/media/MP3.5/R',
				
				// '/media/MP3.5/S',
				

				// '/media/MP3.6/T',
            
                // '/media/MP3.6/U',
				// '/media/MP3.6/V',
				
				// '/media/MP3.6/W',
				// '/media/MP3.6/X',
				// '/media/MP3.6/Y',
				// '/media/MP3.6/Z',


				// '/media/MP3.VA/mp3.tracks',
				//'/media/MP3.VA/Collections',
				// '/media/MP3.VA/Odd Future Wolf Gang Kill Them All',
				
				// '/media/MP3.VA/LARA',
				// '/media/MP3.VA/OST',
				// '/media/MP3.VA/VA',
			);

		// set filetypes to scan for
		$ftypes = array(
					'flac','Flac','Flac',
					'm4a','M4a','M4A',
					'm4p','M4p','M4P',
					'mp3','Mp3','MP3',
					// 'mpg','Mpg','MPG',
					'ogg','Ogg','OGG',
					'wav','Wav','WAV',
					'wma','Wma','WMA'
				);

		// create storage array
		$filelist = array();

		// loop thourgh dirs and scan them
        
		$t=0;
		foreach ($dirs as $dir) {
            echo 'iterating over directory: '.$dir."<br/>\n";
            if(strpos(''.$dir, '.Trash-1000') === false){
                $finder = new Finder();
                
                foreach ($finder->files()->in($dir)->name('/.?\.('.implode('|', $ftypes).')$/')->date('since 150 week ago') as $key => $file) {
                    $timelimit_count += 60;
                    set_time_limit($timelimit_count);
                    // if track does not exist, analyze it
                    $track = $this->em
                        ->getRepository('AppSupplyWarakinBundle:Tags')
                        ->findOneByUrl($file);
                    if($track){
                        echo $file.'<br/>';
                        if(!$track->getSeratoFeatures()){
                            $track = $this->performSeratoAssessment($track);
                            if($track != false){
                                var_dump($track->getUrl());echo "<br/>\n";
                            }
                            if($track){
                                echo 'count($track->getSeratoFeatures()->getSeratoCuepoints()):';
                                var_dump(count($track->getSeratoFeatures()->getSeratoCuepoints()));
                                echo "<br/>\n";
                                if( count( $track->getSeratoFeatures()->getSeratoCuepoints()) > 0 ){
                                    echo 'File contains serato cuepoints<br/>'."\n";
                                    //$filelist[$file.''] = $this->getId3Tags($file);

                                    $trackUpdated = new \DateTime();
                                    echo 'updated time: filemtime($track->getUrl()): '.filemtime($track->getUrl())."/".date('y-m-d h:i:s', filemtime($track->getUrl()))."<br/>\n"; 
                                    $trackUpdated->setTimestamp(filemtime($track->getUrl()));
                                    $track->setUpdated($trackUpdated);
                                    
                                    $this->em->persist($track);
                                    $this->em->flush();
                                }else{
                                    echo 'Track returned, but could not find any serato cuepoints.<br/>'."\n";
                                }
                            }else{
                                echo 'File doesnt have any serato props<br/>'."\n";
                            }
                        }else{
                            echo 'File already has serato features<br/>'."\n";
                            echo 'db cuepoint count:';
                            $dbSeratoCuepointCount = count($track->getSeratoFeatures()->getSeratoCuepoints());
                            echo $dbSeratoCuepointCount;
                            echo '<br/>';
                            echo 'Check for changes<br/>'."\n";
                            $freshTrack = $this->performSeratoAssessment($track);
                            echo '<br/>file cuepoint count:';
                            $freshTrackFeatures = $freshTrack->getSeratoFeatures();
                            $fileSeratoCuepointCount = count($freshTrackFeatures->getSeratoCuepoints());
                            echo $fileSeratoCuepointCount.'<br/>';
                            if(
                                $fileSeratoCuepointCount > $dbSeratoCuepointCount ||
                                $freshTrack->getBpm() != $track->getBpm() ||
                                $freshTrack->getCamelotKey() != $track->getCamelotKey()
                            ){
                                echo('file may have new cuepoints ('. ( $fileSeratoCuepointCount-$dbSeratoCuepointCount ).' new), store them <br/>');
                                echo('file may have new key ('. ( $fileSeratoCuepointCount-$dbSeratoCuepointCount ).' new), store it <br/>');
                                echo('file may have new bpm ('. ( $fileSeratoCuepointCount-$dbSeratoCuepointCount ).' new), store it <br/>');
                                //$track->removeSeratoFeatures();
                                //persists freshTrack()

                                $freshTrackUpdated = new \DateTime();
                                echo 'updated time: filemtime($freshTrack->getUrl()): '.filemtime($freshTrack->getUrl())."/".date('y-m-d h:i:s', filemtime($freshTrack->getUrl()))."<br/>\n"; 
                                $freshTrackUpdated->setTimestamp(filemtime($freshTrack->getUrl()));
                                $freshTrack->setUpdated($freshTrackUpdated);

                                $this->em->persist($freshTrack);
                                $this->em->flush();

                                //die('refresh to see if darkstar has been updated.<br/>');
                            }
				/*if($track->getTitle() == 'The Lead'){
					die('the lead; did it finally find the right cues?');
				}*/
                            
                        }
                        echo '-----------------------------------------------------------------------------------------<br/>';
                    }else{
                        echo 'File not in Db so serato data cant be stored.<br/>'."\n";
                    }
                }
            }
        }
        return true; 
    }

    private function performSeratoAssessment($warakinTrack){

        $getId3 = new GetId3();

        $audioTags = $getId3
                        ->setOptionMD5Data(true)
                        ->setOptionMD5DataSource(true)
                        ->setEncoding('UTF-8')
                        ->analyze($warakinTrack->getUrl());
                        

        if($warakinTrack->getSeratoFeatures()){
            $seratoFeatures = $warakinTrack->getSeratoFeatures();
        }else{
            $seratoFeatures = new \AppSupply\WarakinBundle\Entity\SeratoFeatures();
        }

        $seratoFeaturesUpdated = new \DateTime();
        $seratoFeaturesUpdated->setTimestamp(filemtime($warakinTrack->getUrl()));
        $seratoFeatures->setUpdated($seratoFeaturesUpdated);

        if(array_key_exists('id3v2', $audioTags)){
            
            if(strpos($warakinTrack->getUrl(), 'Kelis')){
                //echo '-----------------------------------------------------------------------------------------<br/><pre>';
                //      var_dump($audioTags['id3v2']);echo '</pre><br/>';
                //echo '-----------------------------------------------------------------------------------------<br/>';
            }

            if(array_key_exists('initial_key', $audioTags['id3v2']['comments'])){
                $seratoFeatures->setMusicKey($audioTags['id3v2']['comments']['initial_key'][0]);
		        echo 'Initial Key: '.$audioTags['id3v2']['comments']['initial_key'][0].'<br/>'."\n";
            }

            if(array_key_exists('bpm', $audioTags['id3v2']['comments'])){
                 $warakinTrack->setBpm($audioTags['id3v2']['comments']['bpm'][0]);
                 echo 'BPM:'.$audioTags['id3v2']['comments']['bpm'][0].'<br/>';
            }

            // check if serato things have been inserted (ie cuepoints / overview / etc.
            if(array_key_exists('GEOB', $audioTags['id3v2'])){
                // has GEOB object


                foreach($audioTags['id3v2']['GEOB'] as $key=>$geobObject){
                    
                    // overview bytecode
                    if ( array_key_exists('description', $geobObject) ){
                        if( $geobObject['description'] == 'Serato Overview'){
                            $seratoFeatures->setHasOverview(true);
                            echo $geobObject['description'] . '<br/>' ;
                            //echo $geobObject['objectdata'] . '<br/>' ;;
                            $base64ObjectData = base64_decode($geobObject['objectdata']);
                            //echo $base64ObjectData .'<br/>';
                            $colorPack =  unpack('H*', $base64ObjectData);
                            $colorHex = array_shift($colorPack);
                            //echo $colorHex;
                            //echo '<br/>';
                        }else if( $geobObject['description'] == 'Serato Analysis'){
                            echo $geobObject['description'] . '<br/>' ;
                            $seratoFeatures->setHasAnalysis(true);
                        }else if( $geobObject['description'] == 'Serato Autotags'){

                            /**
                             *  Auto tags objectdata contains: 93.32-3.8210.000 
                             *      Serato estimated BPM: 93.32
                             *      (Number of miliseconds of track??):  3.8210.000
                             */

                            echo $geobObject['description'] . '<br/>' ;
                            $seratoFeatures->setHasAutotags(true);

                        }else if( $geobObject['description'] == 'Serato Markers_'){

                            /**
                             *  Markers_ contains some vague shit. 
                             *  Use a diff between a mp3 track with cue pints, and one without, to see if this tag is different
                             *
                             *
                             */
                            // standard length ie. empty is 318 (as string)
                            //if(strlen($geobObject['objectdata'])>318){
                                echo $geobObject['description'] . ':' . iconv_strlen ($geobObject['objectdata']) . '<br/>' ;
                                echo 'do a thing: <br/>';
                                //var_dump(trim($geobObject['objectdata']));
                                echo $geobObject['datalength'].'<br/>';
                                echo base64_decode($geobObject['objectdata']).'<br/>';
                                $seratoFeatures->setHasMarkers1(true);
                            //}
                        }else if( $geobObject['description'] == 'Serato Markers2'){
                            echo $geobObject['description'].'<br/>';
                            //var_dump($geobObject);

                            // unset markers: 
                            //      AQFDT0xPUgAAAAAEAP///0JQTUxPQ0sAAAAAAQAA

                            // markers set in Crookers - No Security:
                            //      AQFDT0xPUgAAAAAEAP///0NVRQAAAAANAAEAAI0IAMyIAAAAAENVRQAAAAANAAIAAPF5AAAA zAAAAEJQTUxPQ0sAAAAAAQAA


                            // Following seems to be the data header: AQFDT0xPUgAAAAAEAP///0
                            // Following seems to be delimiting info: NVRQ JQTU
                            // Following seems to be the data footer: JQTUxPQ0sAAAAAAQAA

                            // disected:
                            // header     [AQFDT0xPUgAAAAAEAP///0]
                            // content    [NVRQAAAAANAAEAAI0IAMyIAAAAAENVRQAAAAANAAIAAPF5AAAA zAAAAE]
                            // footer     [JQTUxPQ0sAAAAAAQAA] 

                            // content disected
                            //                  [NVRQ][AAAAANAAEAAI0IAMyIAAAAAE]
                            //                  [NVRQ][AAAAANAAIAAPF5AAAA zAAAAE]
                            // further comparison
                            //                  [AAAAANAA][EAAI0IAMyIA][AAAAE]
                            //                  [AAAAANAA][IAAPF5AAAA z][AAAAE]
                            //                  [EAAI0IAMyIA]
                            //                    [IAAPF5AAAA z]

                            /** 
                             *  Marker data
                             *  CUE1: 
                             *  CUE2: 00:36.1 =  36100 ms // 36104
                             *  CUE3: 01:01.8 =  61800 ms
                             *  CUE4: 01:16.2 =  76200 ms
                             *  CUE5: 00:00.1 =    100 ms
                             *  CUE6: 01:55.4 = 115400 ms
                             *  CUE7: 01:35.6 =  95600 ms
                             *  CUE8: 02:25.9 = 145900 ms
                             */ 

                            //echo $geobObject['objectdata'].'<br/>';
                            
                            
                            // base 64 object decoding
                            $base64ObjectData = base64_decode($geobObject['objectdata']);
                            //echo $base64ObjectData.'<br/>';
                            //echo 'cue further decode: '.base64_decode(base64_decode($geobObject['objectdata'])).'<br/>';
                            //echo 'NVRQAAAAANAA: '.base64_decode('NVRQAAAAANAA').'<br/>';

                            // retrieving track color assigned in serato 
                            $color = substr($base64ObjectData, 13, 3); //       'P///0N';
                            $colorPack =  unpack('H*', $color);
                            $colorHex = array_shift($colorPack);
                            echo 'colorHex:'.$colorHex.'<br/>';



                            // find the cuepoints
                            // first variable part of cue parts is the cue number, second bit is the cue location in (ms)

                            // strip off the color an  bpm lock parts of the serato data

                            $cuepointsStr = substr($base64ObjectData, 16, -14);
                            
                            $cuepointsArr = explode('CUE', $cuepointsStr);
                           // echo 'cue in base64:'.base64_encode('CUE').'<br/>';
                            

                            $cuepoints = array();
			            	echo 'found '.count($cuepointsArr).' cuepoints in file.<br/>';
                            foreach($cuepointsArr as $key => $val){
                                if($val!=''){
                                    $order = base64_decode(substr(base64_encode($val),0 )); //       'P///0N';
                                    $orderPack =  unpack('H*', $order);
                                    $orderHex = array_shift($orderPack);
                                    $orderVal = intval(substr($orderHex, 13,1));

                                    $color = base64_decode(substr(base64_encode($val),16,6 ));
                                    $colorPack =  unpack('H*', $color);
                                    $colorVal = array_shift($colorPack);

                                    $positionVal = hexdec(substr($orderHex, 14,8));

					                $stored = false;
                                    if(count($seratoFeatures->getSeratoCuepoints()) > 0){
				                    	echo 'file already has '.count($seratoFeatures->getSeratoCuePoints()).', checking if the found cuepoints are new<br/>';
                                        foreach($seratoFeatures->getSeratoCuepoints() as $dbCuepoint){
                                            if($dbCuepoint->getOrder()==$orderVal){
                                                echo 'Cuepoint '.$orderVal.' found. Updating it.<br/>';
                                                $stored = true;
                                                $dbCuepoint->setColor('#'.$colorVal);
                                                $dbCuepoint->setPosition($positionVal);
                                            }
                                        }
                                    }

                                    if(!$stored){
                                        echo 'cuepoint '.$orderVal.' was not found in track. Storing it as a new cuepoint<br/>';
                                        $seratoCuepoint = new \AppSupply\WarakinBundle\Entity\SeratoCuepoint();
                                        $seratoCuepoint->setOrder($orderVal);
                                        $seratoCuepoint->setColor('#'.$colorVal);
                                        $seratoCuepoint->setPosition($positionVal);
                                        $seratoFeatures->addSeratoCuepoint($seratoCuepoint);
                                    }
                                    //echo 'val:'; var_dump($val);echo '<br/>';
                                    //echo(base64_encode($val));echo '<br/>';]

                                }
                            }
                            $cuepoints = $seratoFeatures->getSeratoCuepoints();
                            
                            if(count($cuepoints)>0){
                                //echo '<br/><pre>';
                                //var_dump($seratoFeatures->getSeratoCuepoints());echo '<br/>';
                                //echo '</pre><br/>';
                                //echo $geobObject['description'] . ':' . iconv_strlen ($geobObject['objectdata']) .'<br/>' ;
                                $seratoFeatures->setHasMarkers2(true);
                            }

                        }else if( $geobObject['description'] == 'Serato BeatGrid'){
                            
                            echo $geobObject['description'] . '<br/>' ;
                            $seratoFeatures->setHasBeatgrid(true);

                        }else if( $geobObject['description'] == 'Serato Offsets_'){

                            /**
                             *  Serato offsets might contain the splits of track:
                             *     ie. the mappings of different beat tempo regions based on manual bpm markers
                             */

                            echo $geobObject['description'] . '<br/>' ;
                            $seratoFeatures->setHasOffsets(true);

                        }else if( $geobObject['description'] == 'SeratoGain'){
                            echo $geobObject['description'] . '<br/>' ;
                            $seratoFeatures->setHasGain(true);
                        }
                    } 
                }
		        $warakinTrack->setSeratoFeatures($seratoFeatures);
                return $warakinTrack; 
            }  
        }
        return false;
    }

    function extractwithEssentia($track){
        
    }



	public function analyzeTagsMd5(){

        $limit = 50;
        $offset = 0;

        if(array_key_exists('offset', $_GET)){
            $offset = $_GET['offset'];
        }

		$files = $this->em
			->getRepository('AppSupplyWarakinBundle:Tags')
			//->findBy(array(), array('name'=>'asc'));
            ->createQueryBuilder('t')
			->where('t.deleted = 0')
            ->andWhere('t.md5 is null')
            //->andWhere('a.spotifyId is null')
            ->setFirstResult( $offset ) //231
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        if(count($files)<1){die('nothing left to do');}

        echo '<html><body><ol start="'.($offset+1).'">';

		foreach($files as $index => $tag){

            echo '<li>Generating MD5 for track: '.$tag->getArtist()." - ". $tag->getTitle().'<br/>';

            $succes = true;
            try {
                $tag->setMd5(md5_file($tag->geturl()));
            } catch (\Exception $e) {
                //print_r($e);
                $tag->setMd5($e->getMessage());
                echo "error: ".$e->getMessage();
                $succes = false;
            }
            if ($succes){

                $this->em->persist($tag);
                $this->em->flush();
            }
            echo '<br/><br/></li>';
            
            //die("<br/>\nwe done here");
        }

        //echo "</ol><script>window['location'].href='?offset=".($offset+$limit)."'</script>";
        echo "</ol> <a href='?offset=".($offset+$limit)."'>Do more</a>";
    
        die("</body></html>");
		
	}

}
