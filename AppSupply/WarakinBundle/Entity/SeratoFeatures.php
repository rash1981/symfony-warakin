<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Exclude;

/**
 * AppSupply\WarakinBundle\Entity\SeratoFeatures
 *
 * @ORM\Table(name="serato_features")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SeratoFeatures
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string $musicKey
     *
     * @ORM\Column(name="music_key", type="string", nullable=true)
     */
    
    private $musicKey;
    
    /**
     * @var integer $hasOverview
     *
     * @ORM\Column(name="has_overview", type="integer", nullable=true)
     */
    
    private $hasOverview;
    
    /**
     * @var integer $hasAutotags
     *
     * @ORM\Column(name="has_autotags", type="integer", nullable=true)
     */
    
    private $hasAutotags;
    
    /**
     * @var integer $hasOffsets
     *
     * @ORM\Column(name="has_offsets", type="integer", nullable=true)
     */
    
    private $hasOffsets;
    
    /**
     * @var integer $hasMarkers1
     *
     * @ORM\Column(name="has_markers1", type="integer", nullable=true)
     */
    
    private $hasMarkers1;

    /**
     * @var integer $hasMarkers2
     *
     * @ORM\Column(name="has_markers2", type="integer", nullable=true)
     */
    
    private $hasMarkers2;

    /**
     * @var integer $hasAnalysis
     *
     * @ORM\Column(name="has_analysis", type="integer", nullable=true)
     */
    
    private $hasAnalysis;

    /**
     * @var integer $hasBeatgrid
     *
     * @ORM\Column(name="has_beatgrid", type="integer", nullable=true)
     */
    
    private $hasBeatgrid;
    

    /**
     * @ORM\OneToOne(targetEntity="Tags", inversedBy="seratoFeatures")
     * @ORM\JoinColumn(name="tags_id", referencedColumnName="id" )
     */
    private $tag;

    /**
     *
     * @Type("ArrayCollection<SeratoCuepoints>")
     * @ORM\OneToMany(targetEntity="SeratoCuepoint", mappedBy="seratoFeatures", cascade={"persist"})
     */
    private $seratoCuepoints;


    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime")
     */
    protected $created;


    /**
     * @var datetime $updated
     * 
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param \AppSupply\WarakinBundle\Entity\Tags $tag
     * @return SeratoFeatures
     */
    public function setTag(\AppSupply\WarakinBundle\Entity\Tags $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \AppSupply\WarakinBundle\Entity\Tags 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set hasCuepoints
     *
     * @param integer $hasCuepoints
     * @return SeratoFeatures
     */
    public function setHasCuepoints($hasCuepoints)
    {
        $this->hasCuepoints = $hasCuepoints;

        return $this;
    }

    /**
     * Get hasCuepoints
     *
     * @return integer 
     */
    public function getHasCuepoints()
    {
        return $this->hasCuepoints;
    }

    /**
     * Set musicKey
     *
     * @param string $musicKey
     * @return SeratoFeatures
     */
    public function setMusicKey($musicKey)
    {
        $this->musicKey = $musicKey;

        return $this;
    }

    /**
     * Get musicKey
     *
     * @return string
     */
    public function getMusicKey()
    {
        return $this->musicKey;
    }

    /**
     * Set hasOverview
     *
     * @param integer $hasOverview
     * @return SeratoFeatures
     */
    public function setHasOverview($hasOverview)
    {
        $this->hasOverview = $hasOverview;

        return $this;
    }

    /**
     * Get hasOverview
     *
     * @return integer 
     */
    public function getHasOverview()
    {
        return $this->hasOverview;
    }

    /**
     * Set hasAutotags
     *
     * @param integer $hasAutotags
     * @return SeratoFeatures
     */
    public function setHasAutotags($hasAutotags)
    {
        $this->hasAutotags = $hasAutotags;

        return $this;
    }

    /**
     * Get hasAutotags
     *
     * @return integer 
     */
    public function getHasAutotags()
    {
        return $this->hasAutotags;
    }

    /**
     * Set hasOffsets
     *
     * @param integer $hasOffsets
     * @return SeratoFeatures
     */
    public function setHasOffsets($hasOffsets)
    {
        $this->hasOffsets = $hasOffsets;

        return $this;
    }

    /**
     * Get hasOffsets
     *
     * @return integer 
     */
    public function getHasOffsets()
    {
        return $this->hasOffsets;
    }

    /**
     * Set hasMarkers1
     *
     * @param integer $hasMarkers1
     * @return SeratoFeatures
     */
    public function setHasMarkers1($hasMarkers1)
    {
        $this->hasMarkers1 = $hasMarkers1;

        return $this;
    }

    /**
     * Get hasMarkers1
     *
     * @return integer 
     */
    public function getHasMarkers1()
    {
        return $this->hasMarkers1;
    }

    /**
     * Set hasMarkers2
     *
     * @param integer $hasMarkers2
     * @return SeratoFeatures
     */
    public function setHasMarkers2($hasMarkers2)
    {
        $this->hasMarkers2 = $hasMarkers2;

        return $this;
    }

    /**
     * Get hasMarkers2
     *
     * @return integer 
     */
    public function getHasMarkers2()
    {
        return $this->hasMarkers2;
    }

    /**
     * Set hasAnalysis
     *
     * @param integer $hasAnalysis
     * @return SeratoFeatures
     */
    public function setHasAnalysis($hasAnalysis)
    {
        $this->hasAnalysis = $hasAnalysis;

        return $this;
    }

    /**
     * Get hasAnalysis
     *
     * @return integer 
     */
    public function getHasAnalysis()
    {
        return $this->hasAnalysis;
    }

    /**
     * Set hasBeatgrid
     *
     * @param integer $hasBeatgrid
     * @return SeratoFeatures
     */
    public function setHasBeatgrid($hasBeatgrid)
    {
        $this->hasBeatgrid = $hasBeatgrid;

        return $this;
    }

    /**
     * Get hasBeatgrid
     *
     * @return integer 
     */
    public function getHasBeatgrid()
    {
        return $this->hasBeatgrid;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seratoCuepoints = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add seratoCuepoints
     *
     * @param \AppSupply\WarakinBundle\Entity\SeratoCuepoint $seratoCuepoints
     * @return SeratoFeatures
     */
    public function addSeratoCuepoint(\AppSupply\WarakinBundle\Entity\SeratoCuepoint $seratoCuepoints)
    {
        $seratoCuepoints = $seratoCuepoints->setSeratoFeatures($this);
        $this->seratoCuepoints[] = $seratoCuepoints;

        return $this;
    }

    /**
     * Remove seratoCuepoints
     *
     * @param \AppSupply\WarakinBundle\Entity\SeratoCuepoint $seratoCuepoints
     */
    public function removeSeratoCuepoint(\AppSupply\WarakinBundle\Entity\SeratoCuepoint $seratoCuepoints)
    {
        $this->seratoCuepoints->removeElement($seratoCuepoints);
    }

    /**
     * Get seratoCuepoints
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSeratoCuepoints()
    {
        return $this->seratoCuepoints;
    }

    /**
    * Gets triggered only on insert

    * @ORM\PrePersist
    */
   public function onPrePersist()
   {
        $this->created = new \DateTime("now");
        $this->updated = new \DateTime("now");
   }

   /**
    * Gets triggered every time on update

    * @ORM\PreUpdate
    */
   public function onPreUpdate()
   {
        if(is_null($this->updated)){
            $this->updated = new \DateTime("now");
        }
   }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return SeratoFeatures
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return SeratoFeatures
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
