<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Exclude;

/**
 * AppSupply\WarakinBundle\Entity\PlaylistTrack
 *
 * @ORM\Table(name="playlist_track")
 * @ORM\Entity
 */
class PlaylistTrack
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    
    /**
     * @var integer $order
     *
     * @ORM\Column(name="ordernum", type="integer", nullable=true)
     */
    
    private $order;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="Playlists", inversedBy="playlistTracks", cascade={"persist"})
     * @ORM\JoinColumn(name="playlist_id", referencedColumnName="id")
     */
    private $playlist;


    /**
     * @ORM\ManyToOne(targetEntity="Tags", inversedBy="playlistTracks", cascade={"persist"})
     * @ORM\JoinColumn(name="track_id", referencedColumnName="id")
     */
    private $track;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return PlaylistTrack
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set playlist
     *
     * @param \AppSupply\WarakinBundle\Entity\Playlists $playlist
     * @return PlaylistTrack
     */
    public function setPlaylist(\AppSupply\WarakinBundle\Entity\Playlists $playlist = null)
    {
        $this->playlist = $playlist;

        return $this;
    }

    /**
     * Get playlist
     *
     * @return \AppSupply\WarakinBundle\Entity\Playlists 
     */
    public function getPlaylist()
    {
        return $this->playlist;
    }

    /**
     * Set track
     *
     * @param \AppSupply\WarakinBundle\Entity\Tags $track
     * @return PlaylistTrack
     */
    public function setTrack(\AppSupply\WarakinBundle\Entity\Tags $track = null)
    {
        $this->track = $track;

        return $this;
    }

    /**
     * Get track
     *
     * @return \AppSupply\WarakinBundle\Entity\Tags 
     */
    public function getTrack()
    {
        return $this->track;
    }
}
