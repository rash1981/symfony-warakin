<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Type;

/**
 * AppSupply\WarakinBundle\Entity\Album
 *
 * @ORM\Table(name="album")
 * @ORM\Entity
 */
class Album
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @Type("ArrayCollection<Tags>")
     * @ORM\OneToMany(targetEntity="Tags", mappedBy="album")
     * @ORM\OrderBy({"track" = "ASC", "url" = "ASC"})
     */
    protected $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Album
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add tags
     *
     * @param AppSupply\WarakinBundle\Entity\Tags $tags
     * @return Album
     */
    public function addTag(\AppSupply\WarakinBundle\Entity\Tags $tags)
    {
        $this->tags[] = $tags;
    
        return $this;
    }

    /**
     * Remove tags
     *
     * @param AppSupply\WarakinBundle\Entity\Tags $tags
     */
    public function removeTag(\AppSupply\WarakinBundle\Entity\Tags $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }

    public function getShort($coverPath=null, $rootUri='')
    {
        if($coverPath==null){
            //$coverPath='/app_dev.php/data/albums/'.$this->getId().'/cover';
            $coverPath=$rootUri.'data/albums/'.$this->getId().'/cover';
        }
        return array(
                    "id"=>$this->getId(),
                    "name"=>$this->getName(),
                    'dataUrl'=>$rootUri.'data/albums/'.$this->getId(),
                    'coverUrl'=>$coverPath,
                    'year'=>$this->getYear()
                );
    }

    public function getYear()
    {
        $year = false;
        foreach($this->tags as $key=>$track){
            if($track->getYear()){
                $year = $track->getYear()->getName();
                break;
            }
        }
        return $year;
    }
    

    public function getArtist()
    {

        $tracks  = $this->getTags();
        $artists  = array();
        // echo 'new find artist for album loop: '."\n";
        //var_dump($tracks);


        foreach($tracks as $track)
        {
            $match = false;
            $trackArtist = $track->getArtist();
            // echo 'new track loop: ';
            // var_dump($trackArtist);

            if($trackArtist)
            {
                $trackArtistId = $trackArtist->getId();
                foreach($artists as $artistIndex=>$artist)
                {
                    if($artist->getId() == $trackArtistId)
                    {
                        $match=true;
                    }
                }
                //var_dump($match);
                if($match===false){
                    if($trackArtistId)
                    {
                        //var_dump($trackArtist->getName());
                        $artists[] = $trackArtist;
                    }
                }
            }
        }
        
        //var_dump($artists);

        if(count($artists) == 1){
            $artist = $artists[0];
        }else{
            $artist = new Artist();
            $artist->setName('Various Artists');
        }
        //}
        return $artist;
    }


    function __toString(){
        return $this->getName();
    }
}
