<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * AppSupply\WarakinBundle\Entity\LastFmTrackInfo
 *
 * @ORM\Table(name="last_fm_track_info")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class LastFmTrackInfo
{
    /**
     * @var integer $id
     *
     * @Exclude
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Tags", inversedBy="lastFmTrackInfo")
     * @ORM\JoinColumn(name="track_id", referencedColumnName="id" )
     */
    protected $track;

    /**
     * @var float $listeners
     *
     * @ORM\Column(name="listeners", type="float", nullable=true)
     */
    private $listeners;

    /**
     * @var integer $playcount
     *
     * @ORM\Column(name="playcount", type="integer", nullable=true)
     */
    private $playcount;


    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime")
     */
    protected $created;


    /**
     * @var datetime $updated
     * 
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated;

    /**
     * Constructor
     */
    public function __construct()
    {

    }

    /**
    * Gets triggered only on insert

    * @ORM\PrePersist
    */
    public function onPrePersist()
    {
        $this->created = new \DateTime("now");
        $this->updated = new \DateTime("now");
    }

    /**
    * Gets triggered every time on update

    * @ORM\PreUpdate
    */
    public function onPreUpdate()
    {
        $this->updated = new \DateTime("now");
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set listeners
     *
     * @param float $listeners
     * @return LastFmTrackInfo
     */
    public function setListeners($listeners)
    {
        $this->listeners = $listeners;

        return $this;
    }

    /**
     * Get listeners
     *
     * @return float
     */
    public function getListeners()
    {
        return $this->listeners;
    }

    /**
     * Set playcount
     *
     * @param integer $playcount
     * @return LastFmTrackInfo
     */
    public function setPlaycount($playcount)
    {
        $this->playcount = $playcount;

        return $this;
    }

    /**
     * Get playcount
     *
     * @return integer
     */
    public function getPlaycount()
    {
        return $this->playcount;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     * @return LastFmTrackInfo
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return LastFmTrackInfo
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set track
     *
     * @param \AppSupply\WarakinBundle\Entity\Tags $track
     * @return LastFmTrackInfo
     */
    public function setTrack(\AppSupply\WarakinBundle\Entity\Tags $track = null)
    {
        $this->track = $track;

        return $this;
    }

    /**
     * Get track
     *
     * @return \AppSupply\WarakinBundle\Entity\Tags 
     */
    public function getTrack()
    {
        return $this->track;
    }
}
