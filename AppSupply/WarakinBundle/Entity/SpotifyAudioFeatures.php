<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
/*
    ["danceability"]=>      float(0.355)
    ["energy"]=>            float(0.703)
    ["key"]=>               int(7)
    ["loudness"]=>          float(-5.893)
    ["mode"]=>              int(1)
    ["speechiness"]=>       float(0.26)
    ["acousticness"]=>      float(0.0349)
    ["instrumentalness"]=>  int(0)
    ["liveness"]=>          float(0.271)
    ["valence"]=>           float(0.502)
    ["tempo"]=>             float(66.734)
    ["type"]=>              string(14) "audio_features"
    
    spotify_track_id
    ["id"]=>                string(22) "3CjGlv5nmX2maNybqwCmS6"
    
    ["uri"]=>               string(36) "spotify:track:3CjGlv5nmX2maNybqwCmS6"
    ["track_href"]=>        string(56) "https://api.spotify.com/v1/tracks/3CjGlv5nmX2maNybqwCmS6"
    ["analysis_url"]=>      string(64) "https://api.spotify.com/v1/audio-analysis/3CjGlv5nmX2maNybqwCmS6"
    ["duration_ms"]=>       int(216123)
    ["time_signature"]=>    int(4)
 */

/**
 * AppSupply\WarakinBundle\Entity\SpotifyAudioFeatures
 *
 * @ORM\Table(name="spotify_audio_features")
 * @ORM\Entity
 */
class SpotifyAudioFeatures
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    

    /**
     * @var float $danceability
     *
     * @ORM\Column(name="danceability", type="float", nullable=true)
     */
    
    private $danceability;
    
    
    
    /**
     * @var float $energy
     *
     * @ORM\Column(name="energy", type="float", nullable=true)
     */
    
    private $energy;
    
    
    
    /**
     * @var integer $musicKey
     *
     * @ORM\Column(name="music_key", type="integer", nullable=true)
     */
    
    private $musicKey;
    
    
    
    /**
     * @var float $loudness
     *
     * @ORM\Column(name="loudness", type="float", nullable=false)
     */
    
    private $loudness;
    
    
    
    /**
     * @var integer $modality
     *
     * @ORM\Column(name="modality", type="integer", nullable=true)
     */
    
    private $modality;
    
    
    
    /**
     * @var float $speechiness
     *
     * @ORM\Column(name="speechiness", type="float", nullable=true)
     */
    
    private $speechiness;
    
    
    
    /**
     * @var float $acousticness
     *
     * @ORM\Column(name="acousticness", type="float", nullable=true)
     */
    
    private $acousticness;
    
    
    
    /**
     * @var integer $instrumentalness
     *
     * @ORM\Column(name="instrumentalness", type="integer", nullable=true)
     */
    
    private $instrumentalness;
    
    
    
    /**
     * @var float $liveness
     *
     * @ORM\Column(name="liveness", type="float", nullable=true)
     */
    
    private $liveness;
    
    
    
    /**
     * @var float $valence
     *
     * @ORM\Column(name="valence", type="float", nullable=true)
     */
    
    private $valence;
    
    
    
    /**
     * @var float $tempo
     *
     * @ORM\Column(name="tempo", type="float", nullable=false)
     */
    
    private $tempo;
    
    
    
    /**
     * @var string $contentType
     *
     * @ORM\Column(name="content_type", type="text", nullable=false)
     */
    
    private $contentType;
    
    
    
    /**
     * @var string $spotify_track_id
     *
     * @ORM\Column(name="spotify_track_id", type="text", nullable=false)
     */
    
    private $spotify_track_id;
    
    
    
    /**
     * @var string $uri
     *
     * @ORM\Column(name="uri", type="text", nullable=false)
     */
    
    private $uri;
    
    
    
    /**
     * @var string $track_href
     *
     * @ORM\Column(name="track_href", type="text", nullable=false)
     */
    
    private $track_href;
    
    
    
    /**
     * @var string $analysis_url
     *
     * @ORM\Column(name="analysis_url", type="text", nullable=false)
     */
    
    private $analysis_url;
    
    
    
    /**
     * @var integer $duration_ms
     *
     * @ORM\Column(name="duration_ms", type="integer", nullable=false)
     */
    
    private $duration_ms;
    
    
    
    /**
     * @var integer $time_signature
     *
     * @ORM\Column(name="time_signature", type="integer", nullable=true)
     */
    
    private $time_signature;

    /**
     * @ORM\OneToOne(targetEntity="Tags", inversedBy="spotifyAudioFeatures")
     * @ORM\JoinColumn(name="tags_id", referencedColumnName="id" )
     */
    protected $tag;
    
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set danceability
     *
     * @param float $danceability
     * @return SpotifyAudioFeatures
     */
    public function setDanceability($danceability)
    {
        $this->danceability = $danceability;

        return $this;
    }

    /**
     * Get danceability
     *
     * @return float 
     */
    public function getDanceability()
    {
        return $this->danceability;
    }

    /**
     * Set energy
     *
     * @param float $energy
     * @return SpotifyAudioFeatures
     */
    public function setEnergy($energy)
    {
        $this->energy = $energy;

        return $this;
    }

    /**
     * Get energy
     *
     * @return float 
     */
    public function getEnergy()
    {
        return $this->energy;
    }

    /**
     * Set musicKey
     *
     * @param integer $musicKkey
     * @return SpotifyAudioFeatures
     */
    public function setMusicKey($musicKey)
    {
        $this->musicKey = $musicKey;

        return $this;
    }

    /**
     * Get musicKey
     *
     * @return integer 
     */
    public function getMusicKey()
    {
        return $this->musicKey;
    }

    /**
     * Set loudness
     *
     * @param float $loudness
     * @return SpotifyAudioFeatures
     */
    public function setLoudness($loudness)
    {
        $this->loudness = $loudness;

        return $this;
    }

    /**
     * Get loudness
     *
     * @return float 
     */
    public function getLoudness()
    {
        return $this->loudness;
    }

    /**
     * Set modality
     *
     * @param integer $modality
     * @return SpotifyAudioFeatures
     */
    public function setModality($modality)
    {
        $this->modality = $modality;

        return $this;
    }

    /**
     * Get modality
     *
     * @return integer 
     */
    public function getModality()
    {
        return $this->modality;
    }

    /**
     * Set speechiness
     *
     * @param float $speechiness
     * @return SpotifyAudioFeatures
     */
    public function setSpeechiness($speechiness)
    {
        $this->speechiness = $speechiness;

        return $this;
    }

    /**
     * Get speechiness
     *
     * @return float 
     */
    public function getSpeechiness()
    {
        return $this->speechiness;
    }

    /**
     * Set acousticness
     *
     * @param float $acousticness
     * @return SpotifyAudioFeatures
     */
    public function setAcousticness($acousticness)
    {
        $this->acousticness = $acousticness;

        return $this;
    }

    /**
     * Get acousticness
     *
     * @return float 
     */
    public function getAcousticness()
    {
        return $this->acousticness;
    }

    /**
     * Set instrumentalness
     *
     * @param integer $instrumentalness
     * @return SpotifyAudioFeatures
     */
    public function setInstrumentalness($instrumentalness)
    {
        $this->instrumentalness = $instrumentalness;

        return $this;
    }

    /**
     * Get instrumentalness
     *
     * @return integer 
     */
    public function getInstrumentalness()
    {
        return $this->instrumentalness;
    }

    /**
     * Set liveness
     *
     * @param float $liveness
     * @return SpotifyAudioFeatures
     */
    public function setLiveness($liveness)
    {
        $this->liveness = $liveness;

        return $this;
    }

    /**
     * Get liveness
     *
     * @return float 
     */
    public function getLiveness()
    {
        return $this->liveness;
    }

    /**
     * Set valence
     *
     * @param float $valence
     * @return SpotifyAudioFeatures
     */
    public function setValence($valence)
    {
        $this->valence = $valence;

        return $this;
    }

    /**
     * Get valence
     *
     * @return float 
     */
    public function getValence()
    {
        return $this->valence;
    }

    /**
     * Set tempo
     *
     * @param float $tempo
     * @return SpotifyAudioFeatures
     */
    public function setTempo($tempo)
    {
        $this->tempo = $tempo;

        return $this;
    }

    /**
     * Get tempo
     *
     * @return float 
     */
    public function getTempo()
    {
        return $this->tempo;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return SpotifyAudioFeatures
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * Set spotify_track_id
     *
     * @param string $spotifyTrackId
     * @return SpotifyAudioFeatures
     */
    public function setSpotifyTrackId($spotifyTrackId)
    {
        $this->spotify_track_id = $spotifyTrackId;

        return $this;
    }

    /**
     * Get spotify_track_id
     *
     * @return string 
     */
    public function getSpotifyTrackId()
    {
        return $this->spotify_track_id;
    }

    /**
     * Set uri
     *
     * @param string $uri
     * @return SpotifyAudioFeatures
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string 
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set track_href
     *
     * @param string $trackHref
     * @return SpotifyAudioFeatures
     */
    public function setTrackHref($trackHref)
    {
        $this->track_href = $trackHref;

        return $this;
    }

    /**
     * Get track_href
     *
     * @return string 
     */
    public function getTrackHref()
    {
        return $this->track_href;
    }

    /**
     * Set analysis_url
     *
     * @param string $analysisUrl
     * @return SpotifyAudioFeatures
     */
    public function setAnalysisUrl($analysisUrl)
    {
        $this->analysis_url = $analysisUrl;

        return $this;
    }

    /**
     * Get analysis_url
     *
     * @return string 
     */
    public function getAnalysisUrl()
    {
        return $this->analysis_url;
    }

    /**
     * Set duration_ms
     *
     * @param integer $durationMs
     * @return SpotifyAudioFeatures
     */
    public function setDurationMs($durationMs)
    {
        $this->duration_ms = $durationMs;

        return $this;
    }

    /**
     * Get duration_ms
     *
     * @return integer 
     */
    public function getDurationMs()
    {
        return $this->duration_ms;
    }

    /**
     * Set time_signature
     *
     * @param integer $timeSignature
     * @return SpotifyAudioFeatures
     */
    public function setTimeSignature($timeSignature)
    {
        $this->time_signature = $timeSignature;

        return $this;
    }

    /**
     * Get time_signature
     *
     * @return integer 
     */
    public function getTimeSignature()
    {
        return $this->time_signature;
    }

    /**
     * Set tag
     *
     * @param \AppSupply\WarakinBundle\Entity\Tags $tag
     * @return SpotifyAudioFeatures
     */
    public function setTag(\AppSupply\WarakinBundle\Entity\Tags $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \AppSupply\WarakinBundle\Entity\Tags 
     */
    public function getTag()
    {
        return $this->tag;
    }
}
