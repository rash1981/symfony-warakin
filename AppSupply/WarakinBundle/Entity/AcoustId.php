<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * AppSupply\WarakinBundle\Entity\AcoustId
 *
 * @ORM\Table(name="acoustid")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class AcoustId
{
    /**
     * @var integer $id
     *
     * @Exclude
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var datetime $updated
     * 
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated;

    /**
     * @var string $acoustid
     * @Exclude
     * @ORM\Column(name="acoustid", type="string", length=255, nullable=true)
     */
    private $acoustid;


    /**
     * @var integer $score
     *
     * @ORM\Column(name="score", type="integer", nullable=true)
     */
    private $score;

    /**
     * @var integer $year
     *
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    private $year;

    /**
     * @var string $recording_mbid
     * @Exclude
     * @ORM\Column(name="recording_mbid", type="string", length=255, nullable=true)
     */
    private $recording_mbid;

    /**
     * @var string $recording_title
     * @Exclude
     * @ORM\Column(name="recording_title", type="string", length=255, nullable=true)
     */
    private $recording_title;

    /**
     * @var string $artist_mbid
     * @Exclude
     * @ORM\Column(name="artist_mbid", type="string", length=255, nullable=true)
     */
    private $artist_mbid;

    /**
     * @var string $artist_name
     * @Exclude
     * @ORM\Column(name="artist_name", type="string", length=255, nullable=true)
     */
    private $artist_name;

    /**
     * @var string $release_mbid
     * @Exclude
     * @ORM\Column(name="release_mbid", type="string", length=255, nullable=true)
     */
    private $release_mbid;

    /**
     * @var string $release_name
     * @Exclude
     * @ORM\Column(name="release_name", type="string", length=255, nullable=true)
     */
    private $release_name;

    /**
     * @var integer $duration
     *
     * @ORM\Column(name="duration", type="integer", nullable=false)
     */
    private $duration;

    /**
     * @var string $identity
     * @Exclude
     * @ORM\Column(name="identity", type="text", nullable=false)
     */
    private $identity;

    /**
     * @ORM\OneToOne(targetEntity="Tags", inversedBy="acoustId")
     * @ORM\JoinColumn(name="tags_id", referencedColumnName="id" )
     */
    protected $tag;

    /**
     * @var string $apiResult
     * @Exclude
     * @ORM\Column(name="api_result", type="text", nullable=false)
     */
    private $apiResult;


    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identity
     *
     * @param string $identity
     * @return AcoustId
     */
    public function setIdentity($identity)
    {
        $this->identity = $identity;

        return $this;
    }

    /**
     * Get identity
     *
     * @return string
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return AcoustId
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Add tag
     *
     * @param \AppSupply\WarakinBundle\Entity\Tags $tag
     * @return AcoustId
     */
    public function setTags(\AppSupply\WarakinBundle\Entity\Tags $tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \AppSupply\WarakinBundle\Entity\Tags $tag
     */
    public function getTags()
    {
        return $this->tag;
    }

    /**
    * Gets triggered only on insert

    * @ORM\PrePersist
    */
    public function onPrePersist()
    {
        $this->created = new \DateTime("now");
        $this->updated = new \DateTime("now");
    }

    /**
        * Gets triggered every time on update

        * @ORM\PreUpdate
        */
    public function onPreUpdate()
    {
        $this->updated = new \DateTime("now");
    }
    
    /**
     * Set created
     *
     * @param \DateTime $created
     * @return AcoustId
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return AcoustId
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set tag
     *
     * @param \AppSupply\WarakinBundle\Entity\Tags $tag
     * @return AcoustId
     */
    public function setTag(\AppSupply\WarakinBundle\Entity\Tags $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \AppSupply\WarakinBundle\Entity\Tags 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set acoustid
     *
     * @param string $acoustid
     * @return AcoustId
     */
    public function setAcoustid($acoustid)
    {
        $this->acoustid = $acoustid;

        return $this;
    }

    /**
     * Get acoustid
     *
     * @return string 
     */
    public function getAcoustid()
    {
        return $this->acoustid;
    }

    /**
     * Set recording_mbid
     *
     * @param string $recordingMbid
     * @return AcoustId
     */
    public function setRecordingMbid($recordingMbid)
    {
        $this->recording_mbid = $recordingMbid;

        return $this;
    }

    /**
     * Get recording_mbid
     *
     * @return string 
     */
    public function getRecordingMbid()
    {
        return $this->recording_mbid;
    }

    /**
     * Set recording_title
     *
     * @param string $recordingTitle
     * @return AcoustId
     */
    public function setRecordingTitle($recordingTitle)
    {
        $this->recording_title = $recordingTitle;

        return $this;
    }

    /**
     * Get recording_title
     *
     * @return string 
     */
    public function getRecordingTitle()
    {
        return $this->recording_title;
    }

    /**
     * Set artist_mbid
     *
     * @param string $artistMbid
     * @return AcoustId
     */
    public function setArtistMbid($artistMbid)
    {
        $this->artist_mbid = $artistMbid;

        return $this;
    }

    /**
     * Get artist_mbid
     *
     * @return string 
     */
    public function getArtistMbid()
    {
        return $this->artist_mbid;
    }

    /**
     * Set artist_name
     *
     * @param string $artistName
     * @return AcoustId
     */
    public function setArtistName($artistName)
    {
        $this->artist_name = $artistName;

        return $this;
    }

    /**
     * Get artist_name
     *
     * @return string 
     */
    public function getArtistName()
    {
        return $this->artist_name;
    }

    /**
     * Set release_mbid
     *
     * @param string $releaseMbid
     * @return AcoustId
     */
    public function setReleaseMbid($releaseMbid)
    {
        $this->release_mbid = $releaseMbid;

        return $this;
    }

    /**
     * Get release_mbid
     *
     * @return string 
     */
    public function getReleaseMbid()
    {
        return $this->release_mbid;
    }

    /**
     * Set release_name
     *
     * @param string $releaseName
     * @return AcoustId
     */
    public function setReleaseName($releaseName)
    {
        $this->release_name = $releaseName;

        return $this;
    }

    /**
     * Get release_name
     *
     * @return string 
     */
    public function getReleaseName()
    {
        return $this->release_name;
    }

    /**
     * Set score
     *
     * @param integer $score
     * @return AcoustId
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return integer 
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return AcoustId
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }




    /**
     * Set apiResult
     *
     * @param string $apiResult
     * @return AcoustId
     */
    public function setApiResult($apiResult)
    {
        $this->apiResult = $apiResult;

        return $this;
    }

    /**
     * Get apiResult
     *
     * @return string
     */
    public function getApiResult()
    {
        return $this->apiResult;
    }
}
