<?php

namespace AppSupply\WarakinBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Type;

/**
 * AppSupply\WarakinBundle\Entity\PlayEvent
 *
 * @ORM\Table(name="play_event")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class PlayEvent
{
   /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;
    

    /**
     * @ORM\ManyToOne(targetEntity="Tags", inversedBy="playEvents", cascade={"persist"})
     * @ORM\JoinColumn(name="track_id", referencedColumnName="id")
     */

    protected $track;

    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @var string $remote
     *
     * @ORM\Column(name="remote", type="string", length=255, nullable=true)
     */

    private $remote;

    /**
     * @var string $userAgent
     *
     * @ORM\Column(name="user_agent", type="string", length=255, nullable=true)
     */

    private $userAgent;

    /**
    * Gets triggered only on insert

    * @ORM\PrePersist
    */
    public function onPrePersist()
    {
        $this->created = new \DateTime("now");
        $this->remote = $_SERVER["REMOTE_ADDR"];
        $this->userAgent = $_SERVER["HTTP_USER_AGENT"];
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return PlayEvent
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set track
     *
     * @param \AppSupply\WarakinBundle\Entity\Tags $track
     * @return PlaylistTrack
     */
    public function setTrack(\AppSupply\WarakinBundle\Entity\Tags $track = null)
    {
        $this->track = $track;

        return $this;
    }

    /**
     * Get track
     *
     * @return \AppSupply\WarakinBundle\Entity\Tags 
     */
    public function getTrack()
    {
        return $this->track;
    }



    /**
     * Set created
     *
     * @param \DateTime $created
     * @return PlayEvent
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }


    /**
     * Set remote
     *
     * @param string $remote
     * @return Tags
     */
    public function setRemote($remote)
    {
        $this->remote = $remote;

        return $this;
    }

    /**
     * Get remote
     *
     * @return string
     */
    public function getRemote()
    {
        return $this->remote;
    }

    /**
     * Set userAgent
     *
     * @param string $userAgent
     * @return Tags
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * Get userAgent
     *
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }


}

