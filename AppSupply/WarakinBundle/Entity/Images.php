<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppSupply\WarakinBundle\Entity\Images
 *
 * @ORM\Table(name="images")
 * @ORM\Entity
 */
class Images
{
   /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="text", nullable=false)
     */
    private $path;

    /**
     * @var integer $deviceid
     *
     * @ORM\Column(name="deviceid", type="integer", nullable=true)
     */
    private $deviceid;

    /**
     * @var string $artist
     *
     * @ORM\Column(name="artist", type="string", length=255, nullable=true)
     */
    private $artist;

    /**
     * @var string $album
     *
     * @ORM\Column(name="album", type="string", length=255, nullable=true)
     */
    private $album;



    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set deviceid
     *
     * @param integer $deviceid
     * @return Images
     */
    public function setDeviceid($deviceid)
    {
        $this->deviceid = $deviceid;
    
        return $this;
    }

    /**
     * Get deviceid
     *
     * @return integer 
     */
    public function getDeviceid()
    {
        return $this->deviceid;
    }

    /**
     * Set artist
     *
     * @param string $artist
     * @return Images
     */
    public function setArtist($artist)
    {
        $this->artist = $artist;
    
        return $this;
    }

    /**
     * Get artist
     *
     * @return string 
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * Set album
     *
     * @param string $album
     * @return Images
     */
    public function setAlbum($album)
    {
        $this->album = $album;
    
        return $this;
    }

    /**
     * Get album
     *
     * @return string 
     */
    public function getAlbum()
    {
        return $this->album;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Images
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }
}
