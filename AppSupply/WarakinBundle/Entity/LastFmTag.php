<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * AppSupply\WarakinBundle\Entity\LastFmTag
 *
 * @ORM\Table(name="last_fm_tag")
 * @ORM\Entity
 */
class LastFmTag
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer $type
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     * 
     * classification sql:
     * 
     * SELECT name, type, count(t.id) as tag_count
     * FROM last_fm_tag as lft
     * LEFT JOIN tracks_last_fm_tags as tlft
     * ON lft.id = tlft.last_fm_tag_id 
     * LEFT JOIN tags as t 
     * ON tlft.tracks_id = t.id
     * WHERE type is null and active = 1 
     * GROUP BY lft.id
     * order by tag_count desc
     * 
     *  1 = genre / style
     *  2 = language / country / geo 
     *  3 = mood
     *  4 = timeperiod
     *  5 = muscial characteristis, like instrument or type of vocal
     *  6 = artist
     *  7 = label / producer / publisher
     *  8 = festival / event
     *  9 = crew & collectives
     * 10 = top X
     * 11 = rating
     * 12 = bpm
     * 13 = theme
     * 14 = release type
     * 15 = collection related (new, old, discovery, etc)
     * 16 = type of song; ballad, sing along, intro, outtro etc.
     * 17 = duration
     * 18 = listening situation; driving, workout, etc
     * 19 = cover & mixes
     * 
     */
    private $type;

    /**
     * @var integer $active
     *
     * @ORM\Column(name="active", type="integer", nullable=true)
     * 
     * 
     * 0 = inactive ( too little tracks)
     * 
     */
    private $active;

    /**
     * @var \Doctrine\Common\Collections\Collection|Tags[]
     *
     * @ORM\ManyToMany(targetEntity="Tags", mappedBy="lastFmTags")
     */
    protected $tracks;

    public function __construct()
    {
        $this->tracks = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return LastFmTag
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return LastFmTag
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return LastFmTag
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    
    /**
     * @param Tags $track
     */
    public function addTrack(Tags $track)
    {
        if ($this->tracks->contains($track)) {
            return;
        }
        $this->tracks->add($track);
        $track->addLastFmTag($this);
    }

    /**
     * @param Tags $track
     */
    public function removeTrack(Tags $track)
    {
        if (!$this->tracks->contains($track)) {
            return;
        }
        $this->tracks->removeElement($track);
        $track->removeLastFmTag($this);
    }
    
    /**
     */
    public function getTracks()
    {
        return $this->tracks;
    }
}
