<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Type;

/**
 * AppSupply\WarakinBundle\Entity\Tags
 *
 * @ORM\Table(name="tags")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Tags
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $url
     * @Exclude
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var string $title
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @Type("AppSupply\WarakinBundle\Entity\Album")
     * @ORM\ManyToOne(targetEntity="Album", inversedBy="tags", cascade={"detach"})
     * @ORM\JoinColumn(name="album", referencedColumnName="id")
     */
    protected $album;

    /**
     * @Type("AppSupply\WarakinBundle\Entity\Artist")
     * @ORM\ManyToOne(targetEntity="Artist", inversedBy="tags", cascade={"detach"})
     * @ORM\JoinColumn(name="artist", referencedColumnName="id")
     */
    protected $artist;

    /**
     * @Type("string")
     * @ORM\ManyToOne(targetEntity="Genre", inversedBy="tags", cascade={"detach"})
     * @ORM\JoinColumn(name="genre", referencedColumnName="id")
     */
    private $genre;


    /**
     * @var string $dir
     *
     * @ORM\Column(name="dir", type="blob", nullable=true)
     */
    private $dir;

    /**
     * @var integer $createdate
     * @Exclude
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @var integer $modifydate
     * @Exclude
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @var integer $composer
     * @Exclude
     * @ORM\Column(name="composer", type="integer", nullable=true)
     */
    private $composer;

    /**
     * @var string $year
     * @ORM\ManyToOne(targetEntity="Year", inversedBy="tags", cascade={"detach"})
     * @ORM\JoinColumn(name="year", referencedColumnName="id")
     */
    private $year;

    /**
     * @var string $comment
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @var float $track
     *
     * @ORM\Column(name="track", type="decimal", nullable=true)
     */
    private $track;

    /**
     * @var integer $discnumber
     *
     * @ORM\Column(name="discnumber", type="integer", nullable=true)
     */
    private $discnumber;

    /**
     * @var integer $bitrate
     *
     * @ORM\Column(name="bitrate", type="integer", nullable=true)
     */
    private $bitrate;

    /**
     * @var integer $length
     *
     * @ORM\Column(name="length", type="integer", nullable=true)
     */
    private $length;

    /**
     * @var integer $samplerate
     *
     * @ORM\Column(name="samplerate", type="integer", nullable=true)
     */
    private $samplerate;

    /**
     * @var integer $filesize
     *
     * @ORM\Column(name="filesize", type="integer", nullable=true)
     */
    private $filesize;

    /**
     * @var integer $filetype
     *
     * @ORM\Column(name="filetype", type="string", length=5, nullable=true)
     */
    private $filetype;


    /**
     * @var boolean $sampler
     * @Exclude
     * @ORM\Column(name="sampler", type="boolean", nullable=true)
     */
    private $sampler;

    /**
     * @var float $bpm
     *
     * @ORM\Column(name="bpm", type="float", nullable=true)
     */
    private $bpm;


    /**
     * @var integer $energy
     *
     * @ORM\Column(name="energy", type="integer", nullable=true)
     */
    private $energy;

    /**
     * @var integer $mood
     *
     * @ORM\Column(name="mood", type="integer", nullable=true)
     */
    private $mood;


    /**
     * @var integer $deviceid
     * @Exclude
     * @ORM\Column(name="deviceid", type="integer", nullable=true)
     */
    private $deviceid;

    /**
     * @var boolean $deleted
     * @ORM\Column(name="deleted", type="boolean", nullable=true)
     */
    private $deleted;


    /**
     * @ORM\OneToOne(targetEntity="Statistics", mappedBy="tag", cascade={"persist"})
     */
    private $statistics;

    /**
     * @Exclude
     * @ORM\OneToOne(targetEntity="AcoustId", mappedBy="tag", cascade={"persist"})
     */
    private $acoustId;


    /**
     * @ORM\OneToOne(targetEntity="LastFmTrackInfo", mappedBy="track", cascade={"persist"})
     */
    private $lastFmTrackInfo;


    /**
     * @Exclude
     * @ORM\OneToOne(targetEntity="SpotifyAudioFeatures", mappedBy="tag", cascade={"persist"})
     */
    private $spotifyAudioFeatures;

    /**
     * @ORM\OneToOne(targetEntity="SeratoFeatures", mappedBy="tag", cascade={"persist"})
     */
    private $seratoFeatures;

    /**
     * @var string $lastFmTitle
     *
     * @ORM\Column(name="last_fm_title", type="string", length=255, nullable=true)
     */
    private $lastFmTitle;

    /**
     * @var string $md5
     *
     * @ORM\Column(name="md5", type="string", length=255, nullable=true)
     */
    
    private $md5;

    /**
     * @var string $lastFmUrl
     *
     * @ORM\Column(name="last_fm_url", type="string", length=255, nullable=true)
     */
    private $lastFmUrl;

    /**
     * @var string $lastFmArtistName
     *
     * @ORM\Column(name="last_fm_artist_name", type="string", length=255, nullable=true)
     */
    private $lastFmArtistName;

    /**
     * @var string $lastFmMbid
     *
     * @ORM\Column(name="last_fm_mbid", type="string", length=255, nullable=true)
     */
    private $lastFmMbid;


    /**
     * @Type("ArrayCollection<PlaylistTrack>")
     * @ORM\OneToMany(targetEntity="PlaylistTrack", mappedBy="track")
     */

    private $playlistTracks;

     /**
     * @var \Doctrine\Common\Collections\Collection|LastFmTag[]
     *
     * @Exclude
     * @ORM\ManyToMany(targetEntity="LastFmTag", inversedBy="tracks")
     * @ORM\JoinTable(
     *  name="tracks_last_fm_tags",
     *  joinColumns={
     *      @ORM\JoinColumn(name="tracks_id", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="last_fm_tag_id", referencedColumnName="id")
     *  }
     * )
     */
    protected $lastFmTags;
    
    /**
     * @Exclude
     * @Type("ArrayCollection<PlayEvent>")
     * @ORM\OneToMany(targetEntity="PlayEvent", mappedBy="track")
     */

    private $playEvents;


     /**
     * @Exclude
     * @ORM\ManyToMany(targetEntity="Tags")
     * @ORM\JoinTable(name="related_last_fm_tracks",
     *     joinColumns={@ORM\JoinColumn(name="track_a_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="track_b_id", referencedColumnName="id")}
     * )
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $relatedLastFmTracks;


    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime")
     */
    protected $created;


    /**
     * @var datetime $updated
     * 
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated;


    public function __construct(){
        
        $this->playlists = new ArrayCollection();
        $this->lastFmTags = new ArrayCollection();
        $this->playEvents = new ArrayCollection();
        $this->playlistTracks = new ArrayCollection();
        $this->relatedLastFmTracks = new ArrayCollection();
        $this->lastFmTags = new ArrayCollection();

    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
       $this->url = $url;
       return $this;
    }


    /** @ORM\PostLoad */
    public function doPostLoad(){
        //$this->url = str_replace('./media/', '/', $this->url);
    }


    /** @ORM\PreUpdate */
    public function doPreUpdate(){
        //$this->url = './media/'.substr($this->url, 1);
    }

    /**
     * Set dir
     *
     * @param string $dir
     * @return Tags
     */
    public function setDir($dir)
    {
        $this->dir = $dir;

        return $this;
    }

    /**
     * Get dir
     *
     * @return string
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * Set createdate
     *
     * @param integer $createdate
     * @return Tags
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return integer
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set modifydate
     *
     * @param integer $modifydate
     * @return Tags
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return integer
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }


    /**
     * Set composer
     *
     * @param integer $composer
     * @return Tags
     */
    public function setComposer($composer)
    {
        $this->composer = $composer;

        return $this;
    }

    /**
     * Get composer
     *
     * @return integer
     */
    public function getComposer()
    {
        return $this->composer;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Tags
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return Tags
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Tags
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set track
     *
     * @param float $track
     * @return Tags
     */
    public function setTrack($track)
    {
        $this->track = $track;

        return $this;
    }

    /**
     * Get track
     *
     * @return float
     */
    public function getTrack()
    {
        return $this->track;
    }

    /**
     * Set discnumber
     *
     * @param integer $discnumber
     * @return Tags
     */
    public function setDiscnumber($discnumber)
    {
        $this->discnumber = $discnumber;

        return $this;
    }

    /**
     * Get discnumber
     *
     * @return integer
     */
    public function getDiscnumber()
    {
        return $this->discnumber;
    }

    /**
     * Set bitrate
     *
     * @param integer $bitrate
     * @return Tags
     */
    public function setBitrate($bitrate)
    {
        $this->bitrate = $bitrate;

        return $this;
    }

    /**
     * Get bitrate
     *
     * @return integer
     */
    public function getBitrate()
    {
        return $this->bitrate;
    }

    /**
     * Set length
     *
     * @param integer $length
     * @return Tags
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return integer
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set samplerate
     *
     * @param integer $samplerate
     * @return Tags
     */
    public function setSamplerate($samplerate)
    {
        $this->samplerate = $samplerate;

        return $this;
    }

    /**
     * Get samplerate
     *
     * @return integer
     */
    public function getSamplerate()
    {
        return $this->samplerate;
    }

    /**
     * Set filesize
     *
     * @param integer $filesize
     * @return Tags
     */
    public function setFilesize($filesize)
    {
        $this->filesize = $filesize;

        return $this;
    }

    /**
     * Get filesize
     *
     * @return integer
     */
    public function getFilesize()
    {
        return $this->filesize;
    }

    /**
     * Set filetype
     *
     * @param $string filetype
     * @return Tags
     */
    public function setFiletype($filetype)
    {
        $this->filetype = $filetype;

        return $this;
    }

    /**
     * Get filetype
     *
     * @return filetype
     */
    public function getFiletype()
    {
        return $this->filetype;
    }

    /**
     * Set sampler
     *
     * @param boolean $sampler
     * @return Tags
     */
    public function setSampler($sampler)
    {
        $this->sampler = $sampler;

        return $this;
    }

    /**
     * Get sampler
     *
     * @return boolean
     */
    public function getSampler()
    {
        return $this->sampler;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Tags
     */
    public function setdeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getdeleted()
    {
        return $this->deleted;
    }

    /**
     * Set bpm
     *
     * @param float $bpm
     * @return Tags
     */
    public function setBpm($bpm)
    {
        $this->bpm = $bpm;

        return $this;
    }

    /**
     * Get bpm
     *
     * @return float
     */
    public function getBpm()
    {
        return $this->bpm;
    }

    /**
     * Set deviceid
     *
     * @param integer $deviceid
     * @return Tags
     */
    public function setDeviceid($deviceid)
    {
        $this->deviceid = $deviceid;

        return $this;
    }

    /**
     * Get deviceid
     *
     * @return integer
     */
    public function getDeviceid()
    {
        return $this->deviceid;
    }

    
    /**
     * Set md5
     *
     * @param string $md5
     * @return Tags
     */
    public function setMd5($md5)
    {
        $this->md5 = $md5;

        return $this;
    }

    /**
     * Get md5
     *
     * @return string 
     */
    public function getMd5()
    {
        return $this->md5;
    }


    /**
     * Set album
     *
     * @param AppSupply\WarakinBundle\Entity\Album $album
     * @return Tags
     */
    public function setAlbum(\AppSupply\WarakinBundle\Entity\Album $album = null)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get album
     *
     * @return AppSupply\WarakinBundle\Entity\Album
     */
    public function getAlbum()
    {
        return $this->album;
    }


    /**
     * Set artist
     *
     * @param AppSupply\WarakinBundle\Entity\Artist $artist
     * @return Tags
     */
    public function setArtist(\AppSupply\WarakinBundle\Entity\Artist $artist = null)
    {
        $this->artist = $artist;

        return $this;
    }

    /**
     * Get artist
     *
     * @return AppSupply\WarakinBundle\Entity\Artist
     */
    public function getArtist()
    {
        return $this->artist;
    }

    /**
     * Set genre
     *
     * @param AppSupply\WarakinBundle\Entity\Genre $genre
     * @return Tags
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return AppSupply\WarakinBundle\Entity\Genre
     */
    public function getGenre()
    {
        return $this->genre;
    }


    /**
     * Set statistics
     *
     * @param \AppSupply\WarakinBundle\Entity\Statistics $statistics
     * @return Tags
     */
    public function setStatistics(\AppSupply\WarakinBundle\Entity\Statistics $statistics = null)
    {
        $statistics = $statistics->setTags($this);
        $this->statistics = $statistics;

        return $this;
    }

    /**
     * Get statistics
     *
     * @return \AppSupply\WarakinBundle\Entity\Statistics
     */
    public function getStatistics()
    {
        return $this->statistics;
    }


    /**
     * Set lastFmTrackInfo
     *
     * @param \AppSupply\WarakinBundle\Entity\LastFmTrackInfo $lastFmTrackInfo
     * @return Tags
     */
    public function setLastFmTrackInfo(\AppSupply\WarakinBundle\Entity\LastFmTrackInfo $lastFmTrackInfo = null)
    {
        $lastFmTrackInfo = $lastFmTrackInfo->setTrack($this);
        $this->lastFmTrackInfo = $lastFmTrackInfo;

        return $this;
    }

    /**
     * Get lastFmTrackInfo
     *
     * @return \AppSupply\WarakinBundle\Entity\LastFmTrackInfo
     */
    public function getLastFmTrackInfo()
    {
        return $this->lastFmTrackInfo;
    }

    /**
     * Set spotifyAudioFeatures
     *
     * @param \AppSupply\WarakinBundle\Entity\SpotifyAudioFeatures $spotifyAudioFeatures
     * @return Tags
     */
    public function setSpotifyAudioFeatures(\AppSupply\WarakinBundle\Entity\SpotifyAudioFeatures $spotifyAudioFeatures = null)
    {
        $this->spotifyAudioFeatures = $spotifyAudioFeatures;

        return $this;
    }

    /**
     * Get spotifyAudioFeatures
     *
     * @return \AppSupply\WarakinBundle\Entity\SpotifyAudioFeatures 
     */
    public function getSpotifyAudioFeatures()
    {
        return $this->spotifyAudioFeatures;
    }


    /**
     * Set lastFmTitle
     *
     * @param string $lastFmTitle
     * @return Tags
     */
    public function setLastFmTitle($lastFmTitle)
    {
        $this->lastFmTitle = $lastFmTitle;

        return $this;
    }

    /**
     * Get lastFmTitle
     *
     * @return string 
     */
    public function getLastFmTitle()
    {
        return $this->lastFmTitle;
    }

    /**
     * Set lastFmUrl
     *
     * @param string $lastFmUrl
     * @return Tags
     */
    public function setLastFmUrl($lastFmUrl)
    {
        $this->lastFmUrl = $lastFmUrl;

        return $this;
    }

    /**
     * Get lastFmUrl
     *
     * @return string 
     */
    public function getLastFmUrl()
    {
        return $this->lastFmUrl;
    }

    /**
     * Set lastFmArtistName
     *
     * @param string $lastFmArtistName
     * @return Tags
     */
    public function setLastFmArtistName($lastFmArtistName)
    {
        $this->lastFmArtistName = $lastFmArtistName;

        return $this;
    }

    /**
     * Get lastFmArtistName
     *
     * @return string 
     */
    public function getLastFmArtistName()
    {
        return $this->lastFmArtistName;
    }

    /**
     * Set lastFmMbid
     *
     * @param string $lastFmMbid
     * @return Tags
     */
    public function setLastFmMbid($lastFmMbid)
    {
        $this->lastFmMbid = $lastFmMbid;

        return $this;
    }

    /**
     * Get lastFmMbid
     *
     * @return string 
     */
    public function getLastFmMbid()
    {
        return $this->lastFmMbid;
    }

    /**
     * @return array
     */
    public function getRelatedLastFmTracks()
    {
        return $this->relatedLastFmTracks->toArray();
    }

    /**
     * @return array
     */
    public function getRelatedLastFmTracksByHarmony($camelotKey)
    {
        $keyRegister = intval(substr($camelotKey, 0, strlen($camelotKey)-1));
        $keyMajorMinor = substr($camelotKey, -1);
        $tracks = [];
        foreach($this->relatedLastFmTracks->toArray() as $index=>$track){
            if($track->getCamelotKey()){
                $trackKey = $track->getCamelotKey();
                $trackRegister = intval(substr($trackKey, 0, strlen($trackKey)-1));
                $trackMajorMinor = substr($trackKey, -1);
                if($keyRegister == $trackRegister){
                    $tracks[] = $track;
                }else if($trackRegister <= $keyRegister+2 &&  $trackRegister >= $keyRegister-2){
                    if($trackMajorMinor == $keyMajorMinor){
                        $tracks[] = $track;
                    }
                }
            }
        }
        usort($tracks, function($a, $b) {
            return $a->getCamelotKey() - $b->getCamelotKey();
        });
        return $tracks;
    }

    /**
     * @param Tags $track
     * @return void
     */
    public function addRelatedLastFmTrack(Tags $track)
    {
        if (!$this->relatedLastFmTracks->contains($track)) {
            $this->relatedLastFmTracks->add($track);
            $track->addRelatedLastFmTrack($this);
        }
    }

    /**
     * @param Tags $track
     * @return void
     */
    public function removeRelatedLastFmTrack(Tags $track)
    {
        if ($this->relatedLastFmTracks->contains($track)) {
            $this->relatedLastFmTracks->removeElement($track);
            $track->removeFriend($this);
        }
    }

    /**
     * Set seratoFeatures
     *
     * @param \AppSupply\WarakinBundle\Entity\SeratoFeatures $seratoFeatures
     * @return Tags
     */
    public function setSeratoFeatures(\AppSupply\WarakinBundle\Entity\SeratoFeatures $seratoFeatures)
    {        
        $seratoFeatures = $seratoFeatures->setTag($this);
        $this->seratoFeatures = $seratoFeatures;

        return $this;
    }

    /**
     * Get seratoFeatures
     *
     * @return \AppSupply\WarakinBundle\Entity\SeratoFeatures 
     */
    public function getSeratoFeatures()
    {
        return $this->seratoFeatures;
    }

    /**
     * Add playlistTracks
     *
     * @param \AppSupply\WarakinBundle\Entity\PlaylistTrack $playlistTracks
     * @return Tags
     */
    public function addPlaylistTrack(\AppSupply\WarakinBundle\Entity\PlaylistTrack $playlistTrack)
    {
        $this->playlistTrack->setTrack($this);
        $this->playlistTracks[] = $playlistTrack;

        return $this;
    }

    /**
     * Remove playlistTracks
     *
     * @param \AppSupply\WarakinBundle\Entity\PlaylistTrack $playlistTracks
     */
    public function removePlaylistTrack(\AppSupply\WarakinBundle\Entity\PlaylistTrack $playlistTrack)
    {
        $playlistTrack->setTrack(null);
        $this->playlistTracks->removeElement($playlistTrack);
    }
    
    /**
     * Get playlistTracks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlaylistTracks()
    {
        return $this->playlistTracks;
    }

        
    /**
     * Get MusicKey
     *
     * @return integer 
     */
    public function getMusicKey()
    {
        if($this->seratoFeatures){
            if($this->seratoFeatures->getMusicKey()){
                return $this->seratoFeatures->getMusicKey();
            }
        }
        if($this->spotifyAudioFeatures){
            if($this->spotifyAudioFeatures->getMusicKey()){
                return $this->spotifyAudioFeatures->getMusicKey();
            }
        }
        
        return null;
        
    }


    /**
     *  Get MusicKey as Camelot
     * 
     *  @return string;
     */
    function getCamelotKey(){
        $musicKey = $this->getMusicKey();
        //var_dump($musicKey);
        $camelotKey = false;
        if($musicKey){
            $keyRosetta = [
                // CAMELOT 1
                [
                    '01A',			// CAMELOT WHEEL
                    11, 			// PITCH CLASS
                    'A-Flat Minor',	// TRADITIONAL	
                    'Abm'
                ],
                [
                    '01A',			// CAMELOT WHEEL
                    11, 			// PITCH CLASS
                    'G-Sharp Minor',	// TRADITIONAL	
                    'G#m'
                ],	
                [
                    '01B',			// CAMELOT WHEEL
                    11 /* e or B */,// PITCH CLASS
                    'B Major',		// TRADITIONAL	
                    'B'
                ],
                [
                    '02A',			// CAMELOT WHEEL
                    6, 				// PITCH CLASS
                    'E-Flat Minor',	// TRADITIONAL	
                    'Ebm'
                ],
    
                // CAMELOT 2
                [
                    '02B',			// CAMELOT WHEEL
                    6, 				// PITCH CLASS
                    'F-Sharp Major',// TRADITIONAL	
                    'F#'
                ],
                [
                    '02B',			// CAMELOT WHEEL
                    6, 				// PITCH CLASS
                    'G-Flat Major',// TRADITIONAL	
                    'Gb'
                ],
                [
                    '03A',			// CAMELOT WHEEL
                    1,	 			// PITCH CLASS
                    'B-Flat Minor',	// TRADITIONAL	
                    'Bbm'
                ],
                [
                    '03B',			// CAMELOT WHEEL
                    1, 				// PITCH CLASS
                    'D-Flat Major',	// TRADITIONAL	
                    'Db'
                ],
                [	
                    '03B',			// CAMELOT WHEEL
                    1, 				// PITCH CLASS
                    'C-Sharp Major',// TRADITIONAL	
                    'C#'
                ],
    
                // CAMELOT 4
                [
                    '04A',	    		// CAMELOT WHEEL
                    8,       			// PITCH CLASS
                    'F Minor',	    	// TRADITIONAL	
                    'Fm'
                ],
                [
                    '04B',			    // CAMELOT WHEEL
                    8,   			    // PITCH CLASS
                    'A-Flat Major',	    // TRADITIONAL	
                    'Ab'
                ],
                [
                    '04B',		    	// CAMELOT WHEEL
                    8, 	        		// PITCH CLASS
                    'G-Sharp Major',	// TRADITIONAL	
                    'G#'
                ],
    
                // CAMELOT 5
                [
                    '05A',		    	// CAMELOT WHEEL
                    3, 				    // PITCH CLASS
                    'C Minor',		    // TRADITIONAL	
                    'Cm'
                ],
                [
                    '05B',			    // CAMELOT WHEEL
                    3, 				    // PITCH CLASS
                    'E-Flat Major',	    // TRADITIONAL	
                    'Eb'
                ],
                [
                    '05B',			    // CAMELOT WHEEL
                    3, 				    // PITCH CLASS
                    'D-Sharp Major',	// TRADITIONAL	
                    'D#'
                ],
    
                // CAMELOT 6
                [
                    '06A',			    // CAMELOT WHEEL
                    10, 			    // PITCH CLASS
                    'G Minor',		    // TRADITIONAL	
                    'Gm'
                ],
                [
                    '06B',			    // CAMELOT WHEEL
                    10, 			    // PITCH CLASS
                    'B-Flat Major',	    // TRADITIONAL	
                    'Bb'
                ],
                [
                    '06B',			    // CAMELOT WHEEL
                    10, 			    // PITCH CLASS
                    'A-Sharp Major',	// TRADITIONAL	
                    'A#'
                ],
    
                // CAMELOT 7
                [
                    '07A',			    // CAMELOT WHEEL
                    5, 				    // PITCH CLASS
                    'D Minor',		    // TRADITIONAL	
                    'Dm'
                ],
                [
                    '07B',			    // CAMELOT WHEEL
                    5, 				    // PITCH CLASS
                    'F Major',		    // TRADITIONAL	
                    'F'
                ],
                [
                    '08A',			    // CAMELOT WHEEL
                    0, 			        // PITCH CLASS
                    'A Minor',		    // TRADITIONAL	
                    'Am'
                ],
                [
                    '08B',			    // CAMELOT WHEEL
                    0, 				    // PITCH CLASS
                    'C Major',		    // TRADITIONAL	
                    'C'
                ],
                [
                    '09A',			    // CAMELOT WHEEL
                    7, 				    // PITCH CLASS
                    'E Minor',		    // TRADITIONAL	
                    'Em'
                ],
                [
                    '09B',			    // CAMELOT WHEEL
                    7, 				    // PITCH CLASS
                    'G Major',		    // TRADITIONAL	
                    'G'
                ],
                [
                    '10A',		    	// CAMELOT WHEEL
                    2, 			    	// PITCH CLASS
                    'B Minor',		    // TRADITIONAL	
                    'Bm'
                ],
                [
                    '10B',		    	// CAMELOT WHEEL
                    2, 				    // PITCH CLASS
                    'D Major',	    	// TRADITIONAL	
                    'D'
                ],
                [
                    '11A',			    // CAMELOT WHEEL
                    9, 				    // PITCH CLASS
                    'F-Sharp Minor',    // TRADITIONAL	
                    'F#m'
                ],
                [
                    '11B',			    // CAMELOT WHEEL
                    9, 				    // PITCH CLASS
                    'A Major',		    // TRADITIONAL	
                    'A'
                ],
                
                // CAMELOT 12
                [
                    '12A',			    // CAMELOT WHEEL
                    4, 				    // PITCH CLASS
                    'C-Sharp Minor',    // TRADITIONAL	
                    'C#m'
                ],
                [
                    '12A',			    // CAMELOT WHEEL
                    4, 				    // PITCH CLASS
                    'D-Flat Minor',	    // TRADITIONAL	
                    'Dbm'
                ],
                [
                    '12B',			    // CAMELOT WHEEL
                    4, 				    // PITCH CLASS
                    'E Major',		    // TRADITIONAL	
                    'E'
                ]
            ];
    
            $camelotKey = '('+$musicKey+')';
            //assign properties to element
           
            //if(gettype($musicKey) === 'string'){
                //console.log('tonalKey: '+tonalKey);
                foreach($keyRosetta as $keyKey => $keyVal) {
                    //console.log(keyVal);
                    if(strtolower($keyVal[3]) === str_replace(' ','', strtolower($musicKey)) ){
                        // console.log("var camelotKey: "+camelotKey);
                        // console.log("camelotKey: "+keyVal[0]);
                        $camelotKey = $keyVal[0];
                        break;
                    }
                    if($keyVal[1] == $musicKey ){
                        // console.log("var camelotKey: "+camelotKey);
                        // console.log("camelotKey: "+keyVal[0]);
                        $camelotKey = $keyVal[0];
                        break;
                    }
                }
            /*}else if(gettype($musicKey) === 'integer'){
                foreach($keyRosetta as $keyKey => $keyVal) {
                    //console.log(keyVal);
                    if($keyVal[1] == $musicKey ){
                        // console.log("var camelotKey: "+camelotKey);
                        // console.log("camelotKey: "+keyVal[0]);
                        $camelotKey = $keyVal[0];
                    }
                }
            }else{
                $camelotKey = $musicKey;
            }*/
            //console.log("Final found camelotKey: "+camelotKey);
        }
        return $camelotKey;
    }

    /**
    * Gets triggered only on insert

    * @ORM\PrePersist
    */
    public function onPrePersist()
    {
        $this->created = new \DateTime("now");
        $this->updated = new \DateTime("now");
    }

    /**
        * Gets triggered every time on update

        * @ORM\PreUpdate
        */
    public function onPreUpdate()
    {
        if(is_null($this->updated)){
            $this->updated = new \DateTime("now");
        }
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Tags
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Tags
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Add playEvents
     *
     * @param \AppSupply\WarakinBundle\Entity\PlayEvent $playEvents
     * @return Tags
     */
    public function addPlayEvent(\AppSupply\WarakinBundle\Entity\PlayEvent $playEvents)
    {
        $this->playEvents[] = $playEvents;

        return $this;
    }

    /**
     * Remove playEvents
     *
     * @param \AppSupply\WarakinBundle\Entity\PlayEvent $playEvents
     */
    public function removePlayEvent(\AppSupply\WarakinBundle\Entity\PlayEvent $playEvents)
    {
        $this->playEvents->removeElement($playEvents);
    }

    /**
     * Get playEvents
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlayEvents()
    {
        return $this->playEvents;
    }

    /**
     * Set energy
     *
     * @param integer $energy
     * @return Tags
     */
    public function setEnergy($energy)
    {
        $this->energy = $energy;

        return $this;
    }

    /**
     * Get energy
     *
     * @return integer 
     */
    public function getEnergy()
    {
        return $this->energy;
    }

    /**
     * Set mood
     *
     * @param integer $mood
     * @return Tags
     */
    public function setMood($mood)
    {
        $this->mood = $mood;

        return $this;
    }

    /**
     * Get mood
     *
     * @return integer 
     */
    public function getMood()
    {
        return $this->mood;
    }


    /**
     * @param LastFmTag $lastFmTag
     */
    public function addLastFmTag(LastFmTag $lastFmTag)
    {
        if ($this->lastFmTags->contains($lastFmTag)) {
            return;
        }
        $this->lastFmTags->add($lastFmTag);
        $lastFmTag->addTrack($this);
    }
    /**
     * @param LastFmTag $lastFmTag
     */
    public function removeLastFmTag(LastFmTag $lastFmTag)
    {
        if (!$this->lastFmTags->contains($lastFmTag)) {
            return;
        }
        $this->lastFmTags->removeElement($lastFmTag);
        $lastFmTag->removeTrack($this);
    }

    /**
     * 
     */
    public function getLastFmTags()
    {
        return $this->lastFmTags->toArray();
    }


    
    /**
     * Set acoustId
     *
     * @param \AppSupply\WarakinBundle\Entity\AcoustId $acoustId
     * @return Tags
     */
    public function setAcoustId(\AppSupply\WarakinBundle\Entity\AcoustId $acoustId = null)
    {
        $acoustId = $acoustId->setTags($this);
        $this->acoustId = $acoustId;

        return $this;
    }

    /**
     * Get acoustId
     *
     * @return \AppSupply\WarakinBundle\Entity\AcoustId
     */
    public function getAcoustId()
    {
        return $this->acoustId;
    }
    
}
