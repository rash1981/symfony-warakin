<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Type;

/**
 * AppSupply\WarakinBundle\Entity\Genre
 *
 * @ORM\Table(name="genre")
 * @ORM\Entity
 */
class Genre
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @Type("ArrayCollection<Artists>")
     */
    protected $artists;

    /**
     * @Type("ArrayCollection<Albums>")
     */
    protected $albums;

    /**
     * @Type("ArrayCollection<Tags>")
     * @ORM\OneToMany(targetEntity="Tags", mappedBy="genre")
     */
    protected $tags;

    

    /**
     * @ORM\ManyToOne(targetEntity="GenreGroup", inversedBy="genres", cascade={"persist"})
     * @ORM\JoinColumn(name="genregroup_id", referencedColumnName="id")
     */
    protected $genreGroup;

    public function __construct()
    {
        $this->artists = new ArrayCollection();
        $this->albums = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Genre
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add tags
     *
     * @param AppSupply\WarakinBundle\Entity\Tags $tags
     * @return Genre
     */
    public function addTag(\AppSupply\WarakinBundle\Entity\Tags $tags)
    {
        $this->tags[] = $tags;

        return $this;
    }

    /**
     * Remove tags
     *
     * @param AppSupply\WarakinBundle\Entity\Tags $tags
     */
    public function removeTag(\AppSupply\WarakinBundle\Entity\Tags $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    public function getShort($rootUri='')
    {
        return array(
                'id'=>$this->getId(),
                'name'=>$this->getName(),
                'dataUrl'=>$rootUri.'data/genres/'.$this->getId(),
                //'trackCount'=>count($this->getTags())
            );
    }

    public function getAlbums()
    {
        $tracks  = $this->getTags();
        $albums  = array();
        foreach($tracks as $track)
        {
            $match = false;
            if($trackAlbum = $track->getAlbum())
            {
                $trackAlbumId  = $trackAlbum->getId();
                $trackAlbumName  = $trackAlbum->getName();

                foreach($albums as $albumIndex=>$album)
                {
                    if($album->getId() == $trackAlbumId)
                    {
                        $match=true;
                    }
                }
                if(!$match){
                    if($trackAlbumName && $trackAlbumId)
                    {
                        $albums[] = $trackAlbum;

                    }
                }
            }
        }
        // //sort by alphabet
        // usort($albums, function($a, $b) {
        //      return strcasecmp($a, $b);
        // });

        $this->albums = $albums;
        return $this->albums;
    }

    public function getArtists()
    {
        /*if()
        {*/
            $tracks  = $this->getTags();
            $artists  = array();
            foreach($tracks as $track)
            {
                $match = false;
                if($trackArtist = $track->getArtist())
                {
                    $trackArtistId  = $trackArtist->getId();
                    $trackArtistName  = $trackArtist->getName();

                    foreach($artists as $artistIndex=>$artist)
                    {
                        if($artist->getId() == $trackArtistId)
                        {
                            $match=true;
                        }
                    }
                    if(!$match)
                    {
                        if($trackArtistName && $trackArtistId)
                        {
                            $artists[] = $trackArtist;
                        }
                    }
                }
            }

            //sort by alphabet
            // usort($artists, function($a, $b){
            //      return strcasecmp($a, $b);
            // });

            $this->artists = $artists;
        //}
        return $artists;
    }


    public function getSortedByFoo()
    {
      $arr = $this->arrayCollection->toArray();
      usort($arr, function($a, $b) {
        if ($a->getFoo() > $b->getFoo()) {
          return -1;
        }
      });
      return $arr;
    }


    function __toString(){
        return $this->getName();
    }

    /**
     * Set genreGroup
     *
     * @param \AppSupply\WarakinBundle\Entity\genreGroup $genreGroup
     * @return Genre
     */
    public function setGenreGroup(\AppSupply\WarakinBundle\Entity\genreGroup $genreGroup = null)
    {
        $this->genreGroup = $genreGroup;

        return $this;
    }

    /**
     * Get genreGroup
     *
     * @return \AppSupply\WarakinBundle\Entity\genreGroup 
     */
    public function getGenreGroup()
    {
        return $this->genreGroup;
    }
}
