<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * AppSupply\WarakinBundle\Entity\Statistics
 *
 * @ORM\Table(name="statistics")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Statistics
{
    /**
     * @var integer $id
     *
     * @Exclude
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $url
     * @Exclude
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var integer $createdate
     *
     * @ORM\Column(name="createdate", type="integer", nullable=true)
     */
    private $createdate;

    /**
     * @var integer $accessdate
     *
     * @ORM\Column(name="accessdate", type="integer", nullable=true)
     */
    private $accessdate;

    /**
     * @var float $percentage
     *
     * @ORM\Column(name="percentage", type="float", nullable=true)
     */
    private $percentage;

    /**
     * @var integer $rating
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $rating;

    /**
     * @var integer $playcounter
     *
     * @ORM\Column(name="playcounter", type="integer", nullable=true)
     */
    private $playcounter;
    
    /**
     * @var boolean $deleted
     * @Exclude
     * @ORM\Column(name="deleted", type="boolean", nullable=true)
     */
    private $deleted;

    /**
     * @ORM\OneToOne(targetEntity="Tags", inversedBy="statistics")
     * @ORM\JoinColumn(name="tags_id", referencedColumnName="id" )
     */
    protected $tag;


    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime")
     */
    protected $created;


    /**
     * @var datetime $updated
     * 
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Statistics
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set createdate
     *
     * @param integer $createdate
     * @return Statistics
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return integer
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set accessdate
     *
     * @param integer $accessdate
     * @return Statistics
     */
    public function setAccessdate($accessdate)
    {
        $this->accessdate = $accessdate;

        return $this;
    }

    /**
     * Get accessdate
     *
     * @return integer
     */
    public function getAccessdate()
    {
        return $this->accessdate;
    }

    /**
     * Set percentage
     *
     * @param float $percentage
     * @return Statistics
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return float
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return Statistics
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set playcounter
     *
     * @param integer $playcounter
     * @return Statistics
     */
    public function setPlaycounter($playcounter)
    {
        $this->playcounter = $playcounter;

        return $this;
    }

    /**
     * Get playcounter
     *
     * @return integer
     */
    public function getPlaycounter()
    {
        return $this->playcounter;
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Statistics
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Add tag
     *
     * @param \AppSupply\WarakinBundle\Entity\Tags $tag
     * @return Statistics
     */
    public function setTags(\AppSupply\WarakinBundle\Entity\Tags $tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \AppSupply\WarakinBundle\Entity\Tags $tag
     */
    public function getTags()
    {
        return $this->tag;
    }

    /**
    * Gets triggered only on insert

    * @ORM\PrePersist
    */
    public function onPrePersist()
    {
        $this->created = new \DateTime("now");
        $this->updated = new \DateTime("now");
    }

    /**
        * Gets triggered every time on update

        * @ORM\PreUpdate
        */
    public function onPreUpdate()
    {
        $this->updated = new \DateTime("now");
    }
    
    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Statistics
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Statistics
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set tag
     *
     * @param \AppSupply\WarakinBundle\Entity\Tags $tag
     * @return Statistics
     */
    public function setTag(\AppSupply\WarakinBundle\Entity\Tags $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \AppSupply\WarakinBundle\Entity\Tags 
     */
    public function getTag()
    {
        return $this->tag;
    }
}
