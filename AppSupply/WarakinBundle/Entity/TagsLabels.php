<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppSupply\WarakinBundle\Entity\TagsLabels
 *
 * @ORM\Table(name="tags_labels")
 * @ORM\Entity
 */
class TagsLabels
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var integer $labelid
     *
     * @ORM\Column(name="labelid", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $labelid;




    /**
     * Set url
     *
     * @param string $url
     * @return TagsLabels
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set labelid
     *
     * @param integer $labelid
     * @return TagsLabels
     */
    public function setLabelid($labelid)
    {
        $this->labelid = $labelid;

        return $this;
    }

    /**
     * Get labelid
     *
     * @return integer
     */
    public function getLabelid()
    {
        return $this->labelid;
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return TagsLabels
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
