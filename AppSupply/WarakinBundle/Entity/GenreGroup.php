<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Type;

/**
 * AppSupply\WarakinBundle\Entity\GenreGroup
 *
 * @ORM\Table(name="genreGroup")
 * @ORM\Entity
 */
class GenreGroup
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @Type("ArrayCollection<Genre>")
     * @ORM\OneToMany(targetEntity="Genre", mappedBy="genreGroup")
     */
    protected $genres;

    public function __construct()
    {
        $this->genres = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GenreGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add genre
     *
     * @param AppSupply\WarakinBundle\Entity\Genre $genre
     * @return GenreGroup
     */
    public function addGenre(\AppSupply\WarakinBundle\Entity\Genre $genre)
    {
        $this->genres[] = $genre;

        return $this;
    }

    /**
     * Remove genre
     *
     * @param AppSupply\WarakinBundle\Entity\Genre $genre
     */
    public function removeGenre(\AppSupply\WarakinBundle\Entity\Genre $genre)
    {
        $this->genres->removeElement($genre);
    }

    /**
     * Get genre
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getGenres()
    {
        return $this->genres;
    }


    function __toString(){
        return $this->getName();
    }
}
