<?php

namespace AppSupply\WarakinBundle\Entity;
 
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Type;

/**
 * AppSupply\WarakinBundle\Entity\Playlists
 *
 * @ORM\Table(name="playlists")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Playlists
{
   /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;


    /**
     * @var string $type
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;
    

    /**
     * @Exclude
     * @Type("ArrayCollection<PlaylistTrack>")
     * @ORM\OneToMany(targetEntity="PlaylistTrack", mappedBy="playlist", cascade={"persist"})
     * @ORM\OrderBy({"order" = "ASC"})
     */

    protected $playlistTracks;


    /**
     *  @var array $playlistGenres
     * 
     *  @Type("array")
     */

    private $playlistGenres;


    /**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime")
     */
    protected $created;


    /**
     * @var datetime $updated
     * 
     * @ORM\Column(type="datetime", nullable = true)
     */
    protected $updated;


    public function __construct()
    {
        //echo 'blaaat';
        $this->playlistTracks = new ArrayCollection();
    }


    /** 
     * @ORM\PostLoad 
     * */
    public function doPostLoad()
    {
        //$this->setPlaylistGenres();// = [];
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Playlists
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     *  Get MetaStats(avg)
     *
     */

    public function getMetaStats($property = 'tempo'){
        $stats = array(
                    'danceability',
                    'energy',
                    'musicKey',
                    'loudness',
                    'modality',
                    'speechiness',
                    'acousticness',
                    'instrumentalness',
                    'liveness',
                    'valence',
                    'tempo'
                );
        $metaStats = array();

        foreach($stats as $index=>$stat){
            $metaStats[$stat] = $this->getMetaStat($stat);
        }

        return $metaStats;
    }


    /**
     *  Get Meta Stat (avg)
     *
     */

    public function getMetaStat($stat = 'tempo'){

        // determine danceability avg etc.
        $tags = $this->getPlaylistTracks();

        $tempoAr = array();
        foreach($tags as $index=>$tag){
            $saf = $tag->getTrack()->getSpotifyAudioFeatures();
            if($saf){
                $fetchFunction = 'get'.ucfirst($stat);
                $tempoAr[] = $saf->$fetchFunction();
            }
        }
        if(count($tempoAr) > 0){
        $tempoArClean = $this->remove_outliers($tempoAr);
            if(count($tempoArClean) > 0){
                //echo $this->name.' : '.$stat.' : '.count($tempoArClean).' < '.count($tempoAr).' : '.min($tempoArClean).':'.max($tempoArClean).'<br/>'."\n"; 
                return (array_sum($tempoArClean)/count($tempoArClean));
            }
        }
        
        return false;
    }

	/*
		Utility Functions
	*/


	function sd_square($x, $mean) {
		return pow($x - $mean, 2);
	} 

	function remove_outliers($dataset, $magnitude = 1) {

		$count = count($dataset);
		$mean = array_sum($dataset) / $count; // Calculate the mean
		$deviation = sqrt(array_sum(array_map(array($this, "sd_square"), $dataset, array_fill(0, $count, $mean))) / $count) * $magnitude; // Calculate standard deviation and times by magnitude

		return array_filter($dataset, function($x) use ($mean, $deviation) { return ($x <= $mean + $deviation && $x >= $mean - $deviation); }); // Return filtered array of values that lie within $mean +- $deviation.
	}



    /**
     * Add playlistTracks
     *
     * @param \AppSupply\WarakinBundle\Entity\PlaylistTrack $playlistTracks
     * @return Playlists
     */
    public function addPlaylistTrack(\AppSupply\WarakinBundle\Entity\PlaylistTrack $playlistTrack)
    {
        $playlistTrack->setPlaylist($this);
        $this->playlistTracks[] = $playlistTrack;

        return $this;
    }

    /**
     * Remove playlistTracks
     *
     * @param \AppSupply\WarakinBundle\Entity\PlaylistTrack $playlistTracks
     */
    public function removePlaylistTrack(\AppSupply\WarakinBundle\Entity\PlaylistTrack $playlistTrack)
    {
        $playlistTrack->setPlaylist(null);
        $playlistTrack->getTrack()->removePlaylistTrack($playlistTrack);
        $playlistTrack->setTrack(null);
        $this->playlistTracks->removeElement($playlistTrack);
    }

    /**
     * Get playlistTracks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    
    public function getPlaylistTracks()
    {
        return $this->playlistTracks;
    }

    /**
     * Set playlistGenres
     * 
     * @return array $playlistGenres
     */

    public function setPlaylistGenres(){

        $playlistTracks = $this->getPlaylistTracks();

        $playlistGenres = [];
        
        foreach($playlistTracks as $index=>$playlistTrack){
            $genre = $playlistTrack->getTrack()->getGenre();
            if($genre){
                if(array_key_exists($genre->getId(), $playlistGenres)){
                    $playlistGenres[$genre->getId()]['count'] += 1;

                }else{
                    $playlistGenres[$genre->getId()] = ["name"=>$genre->getName(), "count"=>1];
                }
            }
        }

        usort($playlistGenres, function($a, $b){
            return $b['count'] - $a['count'];
        });

        $this->playlistGenres = $playlistGenres;

        return $this->playlistGenres;
        
    }

    /**
     * Get playlistGenres
     * 
     * @return array $playlistGenres
     */

    public function getPlaylistGenres(){

        return $this->playlistGenres;
        
    }

    public function clearPlaylistTracks()
    {
        $this->getPlaylistTracks()->clear();
    }

     /**
     * Gets triggered only on insert

     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->created = new \DateTime("now");
        $this->updated = new \DateTime("now");
    }

    /**
     * Gets triggered every time on update

     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updated = new \DateTime("now");
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Playlists
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Playlists
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Playlists
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

}
