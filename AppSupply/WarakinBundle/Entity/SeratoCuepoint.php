<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Exclude;

/**
 * AppSupply\WarakinBundle\Entity\SeratoCuepoint
 *
 * @ORM\Table(name="serato_cuepoint")
 * @ORM\Entity
 */
class SeratoCuepoint
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var integer $order
     *
     * @ORM\Column(name="ordernum", type="integer", nullable=true)
     */
    
    private $order;
    
    /**
     * @var integer $position
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    
    private $position;
    
    /**
     * @var integer $color
     *
     * @ORM\Column(name="color", type="string", nullable=true)
     */
    
    private $color;
    
    /**
     * @Type("string")
     * @ORM\ManyToOne(targetEntity="SeratoFeatures", inversedBy="seratoCuepoints", cascade={"persist"})
     * @ORM\JoinColumn(name="serato_features_id", referencedColumnName="id")
     * @Exclude
     */
    private $seratoFeatures;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return SeratoCuepoint
     */
    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return SeratoCuepoint
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set color
     *
     * @param stringr $color
     * @return SeratoCuepoint
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set seratoFeatures
     *
     * @param \AppSupply\WarakinBundle\Entity\SeratoFeatures $seratoFeatures
     * @return SeratoCuepoint
     */
    public function setSeratoFeatures(\AppSupply\WarakinBundle\Entity\SeratoFeatures $seratoFeatures = null)
    {
        $this->seratoFeatures = $seratoFeatures;

        return $this;
    }

    /**
     * Get seratoFeatures
     *
     * @return \AppSupply\WarakinBundle\Entity\SeratoFeatures 
     */
    public function getSeratoFeatures()
    {
        return $this->seratoFeatures;
    }
}
