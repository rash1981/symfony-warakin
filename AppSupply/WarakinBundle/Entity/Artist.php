<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Exclude;

/**
 * AppSupply\WarakinBundle\Entity\Artist
 *
 * @ORM\Table(name="artist")
 * @ORM\Entity
 */
class Artist
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @Type("ArrayCollection<Albums>")
     */
    protected $albums;

    /**
     *
     * @Type("ArrayCollection<Tags>")
     * @ORM\OneToMany(targetEntity="Tags", mappedBy="artist")
     * @ORM\OrderBy({"createdate" = "DESC", "track" = "ASC"})
     * @Exclude
     */
    protected $tags;

    /**
     * @var string $lastFmMbid
     *
     * @ORM\Column(name="last_fm_mbid", type="string", length=255, nullable=true)
     */
    private $lastFmMbid;

    /**
     * @var string $lastFmUrl
     *
     * @ORM\Column(name="last_fm_url", type="string", length=255, nullable=true)
     */
    private $lastFmUrl;

    /**
     * @var string $lastFmName
     *
     * @ORM\Column(name="last_fm_name", type="string", length=255, nullable=true)
     */
    private $lastFmName;

     /**
     * @Exclude
     * @ORM\ManyToMany(targetEntity="Artist")
     * @ORM\OrderBy({"name" = "ASC"})
     * @ORM\JoinTable(name="related_last_fm_artists",
     *     joinColumns={@ORM\JoinColumn(name="artist_a_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="artist_b_id", referencedColumnName="id")}
     * )
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $relatedLastFmArtists;

    /**
     * @var string $spotifyId
     *
     * @ORM\Column(name="spotify_id", type="string", length=255, nullable=true)
     */
    private $spotifyId;

    /**
     * @var string $spotifyName
     *
     * @ORM\Column(name="spotify_name", type="string", length=255, nullable=true)
     */
    private $spotifyName;

     /**
     * @Exclude
     * @ORM\ManyToMany(targetEntity="Artist")
     * @ORM\JoinTable(name="related_spotify_artists",
     *     joinColumns={@ORM\JoinColumn(name="artist_a_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="artist_b_id", referencedColumnName="id")}
     * )
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    private $relatedSpotifyArtists;




    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->albums = new ArrayCollection();
        $this->relatedLastFmArtists = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Artist
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add tags
     *
     * @param AppSupply\WarakinBundle\Entity\Tags $tags
     * @return Artist
     */
    public function addTag(\AppSupply\WarakinBundle\Entity\Tags $tags)
    {
        $this->tags[] = $tags;
    
        return $this;
    }

    /**
     * Remove tags
     *
     * @param AppSupply\WarakinBundle\Entity\Tags $tags
     */
    public function removeTag(\AppSupply\WarakinBundle\Entity\Tags $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Get tags
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getTags()
    {
        return $this->tags;
    }

    public function getAlbums()
    {
        $tracks  = $this->getTags();
        $albums  = array();
        foreach($tracks as $track)
        {
            $match = false;
            if($trackAlbum = $track->getAlbum())
            {
                $trackAlbumId  = $trackAlbum->getId();
                $trackAlbumName  = $trackAlbum->getName();

                foreach($albums as $albumIndex=>$album)
                {
                    if($album->getId() == $trackAlbumId)
                    {
                        $match=true;
                    }
                }
                if(!$match){
                    if($trackAlbumName && $trackAlbumId)
                    {
                        $albums[] = $trackAlbum;                                    
                    }
                }
            }
        }
        $this->albums = $albums;
        return $this->albums;
    }

    public function getShort($rootUri=''){
        return array(
                'id'=>$this->getId(),
                'name'=>$this->getName(),
                'dataUrl'=>$rootUri.'data/artists/'.$this->getId(),
                'songCount'=>count($this->getTags()),
            );
    }

    function __toString(){
        return "".$this->getName();
    }

    /**
     * Set spotifyId
     *
     * @param string $spotifyId
     * @return Artist
     */
    public function setSpotifyId($spotifyId)
    {
        $this->spotifyId = $spotifyId;

        return $this;
    }

    /**
     * Get spotifyId
     *
     * @return string 
     */
    public function getSpotifyId()
    {
        return $this->spotifyId;
    }

    /**
     * Set lastFmMbid
     *
     * @param string $lastFmMbid
     * @return Artist
     */
    public function setLastFmMbid($lastFmMbid)
    {
        $this->lastFmMbid = $lastFmMbid;

        return $this;
    }

    /**
     * Get lastFmMbid
     *
     * @return string 
     */
    public function getLastFmMbid()
    {
        return $this->lastFmMbid;
    }

    /**
     * Set lastFmUrl
     *
     * @param string $lastFmUrl
     * @return Artist
     */
    public function setLastFmUrl($lastFmUrl)
    {
        $this->lastFmUrl = $lastFmUrl;

        return $this;
    }

    /**
     * Get lastFmUrl
     *
     * @return string 
     */
    public function getLastFmUrl()
    {
        return $this->lastFmUrl;
    }

    /**
     * Set lastFmName
     *
     * @param string $lastFmName
     * @return Artist
     */
    public function setLastFmName($lastFmName)
    {
        $this->lastFmName = $lastFmName;

        return $this;
    }

    /**
     * Get lastFmName
     *
     * @return string 
     */
    public function getLastFmName()
    {
        return $this->lastFmName;
    }

    /**
     * @return array
     */
    public function getRelatedLastFmArtists()
    {
        return $this->relatedLastFmArtists;
        //return $this->relatedLastFmArtists->toArray();
    }

    /**
     * @param  Artist $artist
     * @return void
     */
    public function addRelatedLastFmArtist(Artist $artist)
    {
        if (!$this->relatedLastFmArtists->contains($artist)) {
            $this->relatedLastFmArtists->add($artist);
            $artist->addRelatedLastFmArtist($this);
        }
    }

    /**
     * @param  Artist $artist
     * @return void
     */
    public function removeRelatedLastFmArtist(Artist $artist)
    {
        if ($this->relatedLastFmArtists->contains($artist)) {
            $this->relatedLastFmArtists->removeElement($artist);
            $artist->removeFriend($this);
        }
    }

    /**
     * Set spotifyName
     *
     * @param string $spotifyName
     * @return Artist
     */
    public function setSpotifyName($spotifyName)
    {
        $this->spotifyName = $spotifyName;

        return $this;
    }

    /**
     * Get spotifyName
     *
     * @return string 
     */
    public function getSpotifyName()
    {
        return $this->spotifyName;
    }

    /**
     * @return array
     */
    public function getRelatedSpotifyArtists()
    {
        return $this->relatedSpotifyArtists->toArray();
    }

    /**
     * @param  Artist $artist
     * @return void
     */
    public function addRelatedSpotifyArtist(Artist $artist)
    {
        if (!$this->relatedSpotifyArtists->contains($artist)) {
            $this->relatedSpotifyArtists->add($artist);
            $artist->addRelatedSpotifyArtist($this);
        }
    }

    /**
     * @param  Artist $artist
     * @return void
     */
    public function removeRelatedSpotifyArtist(Artist $artist)
    {
        if ($this->relatedSpotifyArtists->contains($artist)) {
            $this->relatedSpotifyArtists->removeElement($artist);
            $artist->removeFriend($this);
        }
    }

}
