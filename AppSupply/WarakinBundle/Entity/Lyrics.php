<?php

namespace AppSupply\WarakinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AppSupply\WarakinBundle\Entity\Lyrics
 *
 * @ORM\Table(name="lyrics")
 * @ORM\Entity
 */
class Lyrics
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string $sourceUrl
     *
     * @ORM\Column(name="source_url", type="string", length=255, nullable=false)
     */
    private $sourceUrl;

    /**
     * @var string $lyrics
     *
     * @ORM\Column(name="lyrics", type="text", nullable=true)
     */
    private $lyrics;    

    /**
     * @ORM\OneToOne(targetEntity="Tags", inversedBy="lyrics")
     * @ORM\JoinColumn(name="track_id", referencedColumnName="id" )
     */
    protected $track;
    
    /**
     * Get sourceUrl
     *
     * @return string 
     */
    public function getSourceUrl()
    {
        return $this->sourceUrl;
    }

    /**
     * Set lyrics
     *
     * @param string $lyrics
     * @return Lyrics
     */
    public function setLyrics($lyrics)
    {
        $this->lyrics = $lyrics;
    
        return $this;
    }

    /**
     * Get lyrics
     *
     * @return string 
     */
    public function getLyrics()
    {
        return $this->lyrics;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sourceUrl
     *
     * @param string $sourceUrl
     * @return Lyrics
     */
    public function setUrl($sourceUrl)
    {
        $this->sourceUrl = $sourceUrl;

        return $this;
    }
    
    /**
     * Set track
     *
     * @param \AppSupply\WarakinBundle\Entity\Tags $track
     * @return LastFmTrackInfo
     */
    public function setTrack(\AppSupply\WarakinBundle\Entity\Tags $track = null)
    {
        $this->track = $track;

        return $this;
    }

    /**
     * Get track
     *
     * @return \AppSupply\WarakinBundle\Entity\Tags 
     */
    public function getTrack()
    {
        return $this->track;
    }

    /**
     * Set sourceUrl
     *
     * @param string $sourceUrl
     * @return Lyrics
     */
    public function setSourceUrl($sourceUrl)
    {
        $this->sourceUrl = $sourceUrl;

        return $this;
    }
}
